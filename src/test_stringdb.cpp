//#include <algorithm>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <errno.h>


//#include "StringInfo.h"
#include "StringDB.h"

#include <iostream>

int main(int argc, char *argv[]) {
  StringDB sdb = StringDB::get_db();
  if (sdb.size() <= 1) {
    const char *strings[] = {"ett", "två", "tre", "fyra", "åttioåtta", "etthundrasju"};
    for (unsigned int i=0; i < sizeof(strings)/sizeof(const char *); ++i)
      sdb.add(strings[i]);
  }

  StringDB::iterator p = sdb.begin();
  while (p != sdb.end()) {
    std::cerr << "Str: " << *p << " at StringNumber " << p.string_number() << std::endl;
    ++p;
  }
}
