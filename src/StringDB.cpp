#include "StringDB.h"
#include "Index.h"
#include "StringInfo.h"
#include "StringSort.h"

#include <iostream>
#include <exception>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

StringDB *StringDB::the_db = 0;

void StringDB::get_db_size() {
  // Find the end of the DB, i.e. the last non-zero position.
  if (curr_size <= 1)
    return;

  unsigned long str_min = 1, str_no = curr_size - 1;

  if ( !*(stringdb_addr+str_min*StrLength) ) {
    curr_size = 1;
    return;
  }
  if (*(stringdb_addr+str_no*StrLength) or *(stringdb_addr+str_no*StrLength-1)) {
    return;
  }

  while ( true ) {
    unsigned long pos = (str_min+str_no) / 2;
    if ( *(stringdb_addr+pos*StrLength) or *(stringdb_addr+pos*StrLength-1) ) {
      if ( str_min == pos )
	break;
      str_min = pos;
    } else {
      str_no = pos;
    }
  }

  curr_size = str_no;
}

StringDB::StringDB() {
  stringdb_fd = open(stringdb_filename, O_RDWR | O_APPEND | O_CREAT, 0666);
  if (stringdb_fd < 0) {
    throw BadIndex(std::string("Cannot open ") + stringdb_filename);
  }
  struct stat st;
  if (fstat(stringdb_fd, &st) < 0) {
    throw BadIndex(std::string("Cannot stat ") + stringdb_filename);
  }
  stringdb_filesize = st.st_size;
  curr_size = stringdb_filesize / StrLength;
  stringdb_addr = 0;
  stringdb_maplen = 0;
  curr_map_size = 0;
  if (stringdb_filesize == 0) {
    // New file, store "\0\0\0\0\0\0\0\0" (nul bytes, length StrLength)
    add("");
  } else {
    remap_dbfile();
    get_db_size();
  }
  atexit(&clean_up);
}

StringDB::~StringDB() {
  if (stringdb_addr) {
    if (munmap(stringdb_addr, stringdb_maplen) < 0) {
      std::cerr << "cannot unmap " << stringdb_filename << std::endl;
    }
  }
  stringdb_addr = 0;
  stringdb_maplen = 0;
  if (close(stringdb_fd) < 0) {
      std::cerr << "cannot close " << stringdb_filename << std::endl;
  }
}

// Will return 0 on failure
unsigned int StringDB::add(const char *str) {
  unsigned int len = strlen(str);
  if (!len and curr_size) {
    // Empty string only at position 0:
    return 0;
  }

  if (len >= MAX_STRING_LENGTH)
    return 0;

  // Length of word record in the string DB file must be a multiple of StrLength:
  unsigned int no_units = len/StrLength+1;

  if (curr_size + no_units > curr_map_size) {
    // Overflow, we'll have to allocate more space.
    // Allocate 10% of current size, but at least 1 MB.
    char nullstr[4096];
    memset((char *)nullstr, 0, sizeof(nullstr));
    size_t space = curr_size*StrLength;
    size_t to_write = space/10;
    if (to_write < 1048576)
      to_write = 1048576;
    space += to_write;
    while (true) {
      off_t fpos = lseek(stringdb_fd, 0, SEEK_END);
      if (fpos < 0)
	return 0;
      stringdb_filesize = fpos;
      if (stringdb_filesize >= space)
	break;
      if (write(stringdb_fd, nullstr, sizeof(nullstr)) < 0)
	return 0;
    }
    remap_dbfile();
  }
  unsigned int pos = curr_size;
  //std::cerr << "Writing " << str << " at pos " << pos << std::endl;
  strcpy(stringdb_addr+pos*StrLength, str);
  curr_size += no_units;
  cache[str] = pos;
  return pos;
}

void StringDB::remap_dbfile() {
  if (!stringdb_filesize or stringdb_filesize == stringdb_maplen)
    return;

  if (stringdb_addr) {
    void *new_map = 
      mremap(stringdb_addr, stringdb_maplen, stringdb_filesize, MREMAP_MAYMOVE);
    if (new_map != MAP_FAILED) {
      if (new_map == stringdb_addr) {
	//std::cerr << "remapped, no move\n";
      } else {
	//std::cerr << "remapped and moved\n";
	stringdb_addr = (char *) new_map;
      }
      stringdb_maplen = stringdb_filesize;
      curr_map_size = stringdb_maplen/StrLength;
      return;
    }
    std::cerr << "couldn't remap StringDB; will unmap first\n";
    if (munmap(stringdb_addr, stringdb_maplen) < 0) {
      throw BadIndex(std::string("Cannot unmap ") + stringdb_filename);
    }
  }
  stringdb_maplen = stringdb_filesize;
  stringdb_addr = (char *)
    mmap(NULL, stringdb_maplen, PROT_READ|PROT_WRITE, MAP_SHARED, stringdb_fd, 0);
  if (stringdb_addr == MAP_FAILED) {
    std::cerr << "Cannot map " << stringdb_filename << ": " << strerror(errno);
    stringdb_addr = 0;
    curr_map_size = 0;
    stringdb_maplen = 0;
    throw BadIndex(std::string("Cannot map ") + stringdb_filename);
  }
  curr_map_size = stringdb_maplen/StrLength;
}

unsigned int StringDB::find(std::string str) {
  SMap::iterator p = cache.find(str);
  if (p != cache.end())
    return p->second;
  static StringSort<LexiCmp> *alpha = StringSort<LexiCmp>::get_db("index/lexisort.txt");
  return alpha->find(str.c_str());
}

StringDB *StringDB::iterator::sdb = 0;
