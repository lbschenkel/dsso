#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>

#include <iostream>
#include <fstream>
#include <vector>

#include "defs.h"
#include "csiphash.h"

#include "LineIndex.h"


const size_t max_cache_size = 50000000;

class Hbuf {
public:
  Hbuf(int filedes) : fd(filedes) {
    lseek(fd, 0, SEEK_SET);
    data = (size_t *) calloc(bmax, sizeof(size_t));
    if (!data)
      throw BadIndex(std::string("cannot allocate memory"));
    load();
  }
  ~Hbuf() {
    if (data) {
      delete data;
      data = (size_t *) 0;
    }
  }
  size_t curr() {
    if (bpos < 0 or bpos > bmax)
      throw BadIndex(std::string("bad index"));
    return data[bpos];
  }
  bool next() {
    if (++bpos < bsize)
      return true;
    return load();
  }
private:
  unsigned int bsize, bpos;
  size_t *data;
  const unsigned int bmax = 65536;
  int fd;
  Hbuf(const Hbuf &old);
  bool load() {
    ssize_t n = read(fd, data, bmax*sizeof(size_t));
    bpos = 0;
    if (!n) {
      bsize = 0;
      bpos = 0;
      data[0] = 0;
      return false;
    }	
    if (n<0)
      throw BadIndex(std::string("cannot read existing hash values: ") + strerror(errno));
    bsize = n/sizeof(size_t);
    return true;
  }
};

namespace {
  int is_lhash_file(const struct dirent *entry) {
    if (strncmp(entry->d_name, "lhash", strlen("lhash")) != 0)
      return 0;
    size_t len = strlen(entry->d_name);
    if ( strcmp(entry->d_name + len -strlen(".bin"), ".bin") != 0)
      return 0;
    return 1;
  }
}

LineIndex::LineIndex(std::string dirname)
  : index_dirname(dirname) {
  int dfd = open(dirname.c_str(), O_DIRECTORY | O_RDONLY);
  if (dfd < 0) {
    throw BadIndex("cannot open line index directory");
  }
  struct dirent **namelist;
  int n = scandirat(dfd, ".", &namelist, is_lhash_file, alphasort);
  if (n < 0)
    throw BadIndex("cannot read line index directory");
  else {
    while (n--) {
      int fd = openat(dfd, namelist[n]->d_name, O_RDONLY);
      if (fd < 0) {
	std::cerr << "cannot open file " << namelist[n]->d_name << ", skipping" << std::endl;
      } else {
	try {
	  LineIndexFile *lif = new LineIndexFile(fd, namelist[n]->d_name);
	  //std::cerr << "Inserting " << namelist[n]->d_name << " at " << lif << std::endl;
	  ifile.insert(lif);
	} catch(BadIndex err) {
	  std::cerr << "skipping bad file " << namelist[n]->d_name
		    << ": " << err.what() << std::endl;
	}
      }
      free(namelist[n]);
    }
    free(namelist);
  }
}

LineIndex::~LineIndex() {
  clear_cache(false);
}

void LineIndex::clear_cache(bool load_new_file) {
  if (!cache.size())
    return;

  char ftemplate[index_dirname.size() + strlen("/lhashXXXXXX.bin") + 1];
  strcpy(ftemplate, (index_dirname + "/lhashXXXXXX" + ".bin").c_str());
  int fd = mkstemps(ftemplate, strlen(".bin") );

  //std::cerr << "Saving " << cache.size() << " records to " << ftemplate << std::endl;

  // The contents of cache will be saved to a file. In order to keep the number
  // of files down, we'll try to merge in some existing files, unless those files
  // are much larger than what we would otherwise write.
  // maxsize - the largest acceptable total size.
  size_t curr_size = cache.size();
  size_t maxsize = curr_size*3;
  if (maxsize < 1e6)
    maxsize = 1e6;
  std::vector<Hbuf *> vfile;
  FileSet::const_iterator file_it = ifile.begin();
  for (;file_it!=ifile.end() and (curr_size+=(*file_it)->size())<maxsize; ++file_it) {
    vfile.push_back(new Hbuf((*file_it)->fd_no()));
  }
  //std::cerr << "CMP: " << 
  if (vfile.size() == 1 and cache.size() > (*ifile.begin())->size()+1) {
    // Don't merge if it's just a singe file of the same size as the current one;
    // merge it later instead.        
    delete vfile[0];
    vfile.clear();
    --file_it;
  } 

  //std::cerr << "NOTE! " << vfile.size() << " files will be merged." << std::endl;
  
  const size_t bsize = 8192;
  size_t wbuf[bsize];
  size_t bpos = 0;
  std::set<size_t>::const_iterator it=cache.begin();
  while (vfile.size()) {
    if (bpos == bsize) {
      if (write(fd, wbuf, bpos*sizeof(size_t)) != bpos*sizeof(size_t)) {
	perror("cannot write line index file");
	//exit(2);
      }
      bpos = 0;
    }
    size_t val = vfile[0]->curr();
    unsigned int lindex = 0;

    for (unsigned int i=1; i<vfile.size(); ++i) {
      if (vfile[i]->curr() < val) {
	val = vfile[i]->curr();
	lindex = i;
      }
    }
    //std::cerr << "Have <" << *it << " " << val << "> ";
    if (it != cache.end() and *it < val) {
      //std::cerr << "store     val " << *it << std::endl;
      wbuf[bpos++] = *it++;
    } else {
      wbuf[bpos++] = val;
      //std::cerr << "store file val " << val << std::endl;
      if (!vfile[lindex]->next()) {
	delete vfile[lindex];
	if (lindex+1 < vfile.size()) {
	  vfile[lindex] = vfile[vfile.size()-1];
	}
	vfile.pop_back();
      }
    }
  }

  for (FileSet::const_iterator it = ifile.begin(); it!=file_it; ++it) {    
    if (unlink( (index_dirname + "/" + (*it)->name()).c_str() ) < 0) {
      perror("cannot delete line index file");
    }
    delete *it;
  }
  
  ifile.erase(ifile.begin(), file_it);

  if (bpos or it != cache.end())
    for (; ; ++it) {
      if (bpos == bsize or it == cache.end()) {
	if (write(fd, wbuf, bpos*sizeof(size_t)) != bpos*sizeof(size_t)) {
	  perror("cannot write line index file");
	  //exit(2);
	}
	if (it == cache.end())
	  break;
      bpos = 0;
      }
      //std::cerr << "store val " << *it << std::endl;
      wbuf[bpos++] = *it;
    }
  cache.clear();
  if (load_new_file) {
    if (fdatasync(fd) < 0)
      perror("cannot flush line index file");
    LineIndexFile *lif = new LineIndexFile(fd, ftemplate+index_dirname.size()+1);
    ifile.insert(lif);
  } else {
    if (close(fd) < 0)
      perror("cannot close line index file");
  }
}

bool LineIndex::add_line(std::string line) {
  static const char global_key[16] = {0,1,2,3,4,5,6,7,8,9,0xa,0xb,0xc,0xd,0xe,0xf};
  size_t hashval = siphash24(line.c_str(), line.size(), global_key);
  //std::cerr << "Hashval: " << hashval << std::endl;
  if (cache.find(hashval) != cache.end())
    return false;
  for (FileSet::const_iterator it = ifile.begin(); it!=ifile.end(); ++it) {
    //std::cerr << "CHECK: " << (*it)->name() << " at " << *it << std::endl;
    if ( (*it)->find_key(hashval) )
      return false;
  }
  cache.insert(hashval);
  if (cache.size() >= max_cache_size)
    clear_cache();
  //std::cerr << "Hashval: " << hashval << " is new" << std::endl;
  return true;
}

size_t LineIndexFile::read_pos(off_t pos) {
  //std::cerr << "Read at pos " << pos << " fd=" << fd << std::endl;
  off_t pos1 = lseek(fd, pos*sizeof(size_t), SEEK_SET);
  if (pos1 < 0) {
    perror("lseek");
    throw BadIndex("cannot seek in line index file");
  }
  //std::cerr << "Seek to pos " << pos1 << std::endl;
  size_t val;
  if (read(fd, &val, sizeof(val)) != sizeof(val)) {
    perror("read at pos");
    throw BadIndex("cannot read from line index file");
  }
  //std::cerr << "Got: " << val << std::endl;
  return val;
}

void LineIndexFile::read_len(void *buf, off_t pos, size_t no_words) {
  off_t pos1 = lseek(fd, pos*sizeof(size_t), SEEK_SET);
  if (pos1 < 0) {
    perror("lseek");
    throw BadIndex("cannot seek in line index file");
  }
  size_t len = sizeof(size_t)*no_words;
  if (read(fd, buf, len) != len) {
    throw BadIndex("cannot read from line index file");
    perror("read at pos");
  }
}

LineIndexFile::LineIndexFile(int the_fd, std::string the_filename)
  : fd(the_fd),
    filename(the_filename)
{
  struct stat st;
  if (fstat(fd, &st) < 0) {
    perror("stat");
    throw BadIndex("cannot stat line index file");
  }
  off_t file_size = st.st_size;
  if (file_size%sizeof(size_t)) {
    perror("file size");
    throw BadIndex("bad size of line index file");
  }
  if (!file_size) {
    throw BadIndex("empty line index file");
  }
  _max_pos = file_size/sizeof(size_t)-1;
  //std::cerr << "FD: " << fd << " mpos=" << _max_pos << " File: " << filename << std::endl;
  _min_val = read_pos(0), _max_val = read_pos(_max_pos);
}

bool LineIndexFile::find_key(size_t key) {
  //std::cerr << "Checking " << filename << std::endl;
  off_t min_pos = 0, max_pos = _max_pos;
  size_t min_val(_min_val), max_val(_max_val);

  //std::cerr << "Min: " << min_pos << " <" << min_val << "> ";
  //std::cerr << "Max: " << max_pos << " <" << max_val << "> " << std::endl;
  if (min_val >= key or key >= max_val)
    return (min_val == key or max_val == key);
  while (true) {
    double dst = (max_pos - min_pos);
    if (dst < 200)
      break;
    double h = max_val-min_val;
    double me = key-min_val;
    double approx_loc = me / h;
    if (approx_loc < 0.01) {
      approx_loc = 0.01;
    } else if (approx_loc > 0.99) {
      approx_loc = 0.99;
    }
    off_t pos = approx_loc * dst;
    if (pos < 100) {
      pos = 100;
    }
    pos += min_pos;
    if (pos > max_pos-100) {
      pos = max_pos - 100;
    }
    size_t val = read_pos(pos);
    if (val < key) {
      min_pos = pos;
      min_val = val;
    } else if (val > key) {
      max_pos = pos;
      max_val = val;
    } else {
      return true;
    }
    //std::cerr << "Min: " << min_pos << " <" << min_val << "> ";
    //std::cerr << "Max: " << max_pos << " <" << max_val << "> " << std::endl;
  }
  off_t dst = max_pos - min_pos;
  //std::cerr << "Reading to memory: " << dst << " words.\n";
  size_t buf[dst];
  read_len((void *)buf, min_pos, dst);
  size_t *offset = buf;
  while (true) {
    off_t pos = dst / 2;
    //std::cerr << "Dist=" << dst << " Min=" << offset[0]
    //      << " Max=" << offset[dst-1] << std::endl;
    //std::cerr << "Checking " << offset[pos] << " at pos " << pos << std::endl;
    if (offset[pos] > key) {
      dst = pos;
    } else if (offset[pos] < key) {
      offset += pos;
      dst -= pos;
    } else {
      return true;
    }
    if (pos == 0)
      break;
  }
  return false;
}
