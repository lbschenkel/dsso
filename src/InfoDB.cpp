template <class C, class F>
void InfoDB<C,F>::remap_infofile() {
  size_t to_add = sdb.size()/10;
  if (to_add < 100000)
    to_add = 100000;
  size_t min_size = sdb.size() + to_add;

  if (curr_map_size >= min_size)
    return;
  struct stat st;
  if (fstat(stringinfo_fd, &st) < 0) {
    std::cerr << "Cannot stat " << stringinfo_filename << " (fd= " << stringinfo_fd << "): " << strerror(errno);
    throw BadIndex(std::string("Cannot stat ") + stringinfo_filename);
  }

  size_t new_file_size = min_size * sizeof(C);
  size_t curr_file_size = st.st_size;
  if (curr_file_size < new_file_size) {
    // File too small, pad with zeros.
    char nullstr[4096];
    memset((char *)nullstr, 0, sizeof(nullstr));
    while (curr_file_size < new_file_size) {
      off_t fpos = lseek(stringinfo_fd, 0, SEEK_END);
      if (fpos < 0)
	throw BadIndex(std::string("Cannot seek in ") + stringinfo_filename);
      curr_file_size = fpos;
      if (write(stringinfo_fd, nullstr, sizeof(nullstr)) < 0)
	throw BadIndex(std::string("Cannot write to ") + stringinfo_filename);
    }
  }

  if (stringinfo_addr) {
    void *new_map =
      mremap(stringinfo_addr, stringinfo_maplen, new_file_size, MREMAP_MAYMOVE);
    if (new_map != MAP_FAILED) {
      if (new_map == stringinfo_addr) {
	//std::cerr << "remapped, no move\n";
      } else {
	//std::cerr << "remapped and moved\n";
	stringinfo_addr = (C *)new_map;
      }
      stringinfo_maplen = new_file_size;
      curr_map_size = min_size;
      return;
    }
    std::cerr << "couldn't remap InfoDB; will unmap first\n";
    if (munmap(stringinfo_addr, stringinfo_maplen) < 0) {
      std::cerr << "Cannot unmap " << stringinfo_filename << ": " << strerror(errno);
      throw BadIndex(std::string("Cannot unmap ") + stringinfo_filename);
    }
  }

  //std::cerr << "MMAP: " << new_file_size << " CL=" << curr_largest_position << std::endl;
  stringinfo_addr =
    (C *) mmap(NULL, new_file_size, PROT_READ|PROT_WRITE, MAP_SHARED, stringinfo_fd, 0);

  if (stringinfo_addr == MAP_FAILED) {
    stringinfo_addr = 0;
    curr_map_size = 0;
    stringinfo_maplen = 0;
    throw BadIndex(std::string("Cannot mmap ") + stringinfo_filename);
  }
  stringinfo_maplen = new_file_size;
  curr_map_size = min_size;
}

template <class C, class F>
void InfoDB<C,F>::update() {
  //std::cerr << "CL=" << curr_largest_position << ", CMS=" << curr_map_size
  //   << ", SSIZE=" << sdb.size() << std::endl;
  if (curr_map_size < sdb.size())
    remap_infofile();

  //std::cerr << "CL=" << curr_largest_position << ", CMS=" << curr_map_size
  //	    << ", SSIZE=" << sdb.size() << std::endl;

  StringDB::iterator p(curr_largest_position);
  for (++p; p < sdb.size(); ++p) {
    curr_largest_position = p.string_number();
    stringinfo_addr[curr_largest_position] = func(p);
  }
  //std::cerr << "CL=" << curr_largest_position << ", CMS=" << curr_map_size
  //	    << ", SSIZE=" << sdb.size() << std::endl;
}

template <class C, class F>
InfoDB<C,F>::InfoDB(const char *filename)
  : stringinfo_filename(filename),
    sdb(StringDB::get_db()) {
  std::string cfg_file = filename;
  cfg_file += ".txt";
  std::ifstream cfgfd(cfg_file.c_str());
  if (cfgfd) {
    cfgfd >> curr_largest_position;
    if (!cfgfd)
      throw BadIndex("Cannot read from file " + cfg_file);
  } else {
    curr_largest_position = 0;
  }
  stringinfo_fd = open(filename, O_RDWR | O_CREAT | O_APPEND, 0664);
  if (stringinfo_fd < 0) {
    std::cerr << "Cannot open " << stringinfo_filename << ": " << strerror(errno);
    throw BadIndex(std::string("Cannot open ") + stringinfo_filename);
  }

  stringinfo_addr = 0;
  stringinfo_maplen = 0;
  curr_map_size = 0;
  remap_infofile();
  atexit(&clean_up);
}

template <class C, class F>
InfoDB<C,F>::~InfoDB() {
  if (stringinfo_addr)
    munmap(stringinfo_addr, stringinfo_maplen);
  close(stringinfo_fd);
  std::string cfg_file = stringinfo_filename;
  cfg_file += ".txt";
  std::ofstream cfgfd(cfg_file.c_str());
  cfgfd << curr_largest_position;
  cfgfd.close();
  /*
  if (!cfgfd)
    throw BadIndex("Cannot write to file " + cfg_file);
  */
}
