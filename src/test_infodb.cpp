#include "StringDB.h"

#include <iostream>
#include <fstream>
#include <exception>

#include "StringDB.h"
#include "InfoDB.h"

class Slen {
public:
  unsigned char operator()(StringDB::iterator p) {
    std::cerr << "DBG: *p = <" << *p << "> Len=" << strlen(*p) << std::endl;
    return strlen(*p);
  }
};

int main(int argc, char *argv[]) {
  StringDB &sdb = StringDB::get_db();
  StringDB::iterator p = sdb.begin();

  InfoDB<unsigned char, Slen> ldb = InfoDB<unsigned char, Slen>::get_db("index/testinfodb.bin");
  ldb.update();
  p = sdb.end();
  do {
    --p;
    unsigned int length = ldb.get_info(p.string_number());    
    std::cerr << "Str: " << *p << "  SN=" << p.string_number() << " len=" << length << std::endl;
  } while (p != sdb.begin());

  exit(0);
}
