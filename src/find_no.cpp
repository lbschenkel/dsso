#include <stdlib.h>

#include <iostream>
#include <fstream>
#include <string>

#include "LineIndex.h"


/*
Läs alla filer till objekt.
Ska ha min,max sparat.

*/

int main(int argc, char *argv[]) {
  if (argc < 3)
    exit(1);
  unsigned long no;
  LineIndex li(argv[1]);
  std::ifstream fd(argv[2]);
  std::string line;
  while ( getline(fd, line) ) {
    bool res = li.add_line(line);
    if (res)
      std::cout << line << "\n";
    //if (!res)
    //std::cerr << "Dupe: " << line << std::endl;
  }
}
