#include <iostream>
#include <exception>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <utility>
#include "CorpusTypes.h"
#include "Index.h"

const char *const index_filename = "index/index.bin";
const char *const meta_filename = "index/meta.bin";


void Index::dealloc(unsigned long size, CPointer pos) {
  if (size < 4)
    throw  BadIndex("Cannot deallocate area of size < 4");
  free_blocks[size].push_back(pos);
  //std::cerr << "DEALLOC " << size << " at " << pos << std::endl;
}

CPointer Index::alloc(unsigned long size) {
  if (size < 4)
    throw  BadIndex("Cannot allocate area of size < 4");
  CPointer pos;
  if (free_blocks[size].size()) {
    pos = free_blocks[size].back();
    free_blocks[size].pop_back();
    //std::cerr << "REALLOC " << size << " at " << pos << std::endl;
  } else {
    pos = index_size;
    index_size += size;
  }
  return pos;
}

Index::~Index() {
  save_meta();
  close(index_fd);
  close(dest_fd);
}

void Index::clear_meta() {
  index_pos = 0;
  index_size = 0;
  unsigned long asize = 4;
  while (asize) {
    free_blocks[asize] = std::vector<CPointer>();
    asize <<= 1;
    //std::cerr << "asize = " << asize << std::endl;
  }
}

Index::Index() {
  //std::cerr << "Index::Index()" << std::endl;
  load_meta();
  index_fd = open(index_filename, O_RDWR | O_CREAT, 0664);
  if (index_fd < 0) {
    throw BadIndex(std::string("Cannot open ") + meta_filename);
  }
  dest_fd = open(index_filename, O_RDWR);
  if (dest_fd < 0) {
    throw BadIndex(std::string("Cannot open ") + index_filename);
  }
  //std::cerr << "Index::Index() end" << std::endl;
}

void Index::copy_idx(CPointer oldpos, unsigned int count, CPointer newpos) {
  const unsigned int bsize = 65536;
  char buf[bsize];
  CPointer bcount = count * sizeof(CPointer);
  if (lseek(index_fd, oldpos * sizeof(CPointer), SEEK_SET) < 0) {
    throw BadIndex(std::string("Cannot seek ") + index_filename);
  }
  if (lseek(dest_fd, newpos * sizeof(CPointer), SEEK_SET) < 0) {
    throw BadIndex(std::string("Cannot seek ") + index_filename);
  }
  while (bcount) {
    int read_bytes = read(index_fd, buf, (bcount > bsize) ? bsize : bcount);
    if (read_bytes <= 0) {
      std::cerr << "Cannot update index: " << strerror(errno) << " (bcount=" << bcount << ")" << std::endl;
      std::cerr << "IFD: " << lseek(index_fd, 0, SEEK_CUR) << " OLDPOS=" << oldpos << " Count=" << count << std::endl;
      exit(1);
    }
    bcount -= read_bytes;
    while (read_bytes > 0) {
      int n = write(dest_fd, buf, read_bytes);
      if (n <= 0) {
	std::cerr << "Cannot update index: " << strerror(errno) << std::endl;
	exit(1);
      }
      read_bytes -= n;
    }
  }
}

void Index::save_meta() {
  //std::cerr << "Index::save_meta() begin" << std::endl;
  int meta_fd = open(meta_filename, O_WRONLY | O_CREAT | O_TRUNC, 0664);
  if (meta_fd < 0) {
    std::cerr << "Cannot open " << meta_filename << ": " << strerror(errno);
    throw BadIndex(std::string("Cannot open ") + meta_filename);
  }
  //std::cerr << "Opened " << meta_filename << std::endl;
  lseek(meta_fd, 0, SEEK_SET);
  const int bsize = 512;
  CPointer buf[bsize];
  int bpos = 0;
  buf[bpos++] = index_pos;
  buf[bpos++] = index_size;
  //std::cerr << "Stored index pos / size " << index_pos << " / " << index_size << std::endl;
  unsigned int curr_idx = 0;
  BlockMap::const_iterator bmi = free_blocks.begin();
  while (true) {
    if (write(meta_fd, buf, bpos * sizeof(CPointer)) < (int) (bpos * sizeof(CPointer))) {
      std::cerr << "Cannot write to " << meta_filename << ": " << strerror(errno);
      throw BadIndex(std::string("Cannot write to ") + meta_filename);
    }
    bpos = 0;
    if (bmi == free_blocks.end())
      break;
    const std::vector<CPointer> &fb(bmi->second);
    if (curr_idx == 0) {
      buf[bpos++] = bmi->first;
      buf[bpos++] = fb.size();
      //std::cerr << "Free blocks size " << bmi->first << ": " << fb.size()  << " ( ";
    }
    while (curr_idx < fb.size() and bpos < bsize) {
      //std::cerr << fb[curr_idx] << " ";
      buf[bpos++] = fb[curr_idx++];
    }
    //std::cerr << ")" << std::endl;
    if (curr_idx == fb.size()) {
      curr_idx = 0;
      ++bmi;
    }
  }
  //std::cerr << "Closed " << meta_filename << std::endl;
  close(meta_fd);
  //std::cerr << "Index::save_meta() end" << std::endl;
}

void Index::load_meta() {
  //std::cerr << "Index::load_meta() begin" << std::endl;
  clear_meta();
  int meta_fd = open(meta_filename, O_RDONLY, 0664);
  if (meta_fd < 0) {
    //std::cerr << "no meta" << std::endl;
    return;
  }
  const int bsize = 512;
  CPointer buf[bsize];
  unsigned int rlen = read(meta_fd, buf, 2 * sizeof(CPointer));
  if (rlen < 2 * sizeof(CPointer)) {
    std::cerr << "bad meta" << std::endl;
    return;
  }
  index_pos = buf[0];
  index_size = buf[1];
  while (true) {
    rlen = read(meta_fd, buf, 2 * sizeof(CPointer));
    if (rlen < 2 * sizeof(CPointer)) {
      break;
    }
    unsigned int asize = buf[0];
    unsigned int no_left = buf[1];
    //std::cerr << "Free blocks size " << asize << ": " << no_left << " ( ";
    while (no_left) {
      unsigned int bpos = 0;
      unsigned int to_read = (no_left > bsize) ? bsize :  no_left;
      rlen = read(meta_fd, buf, to_read * sizeof(CPointer)) / sizeof(CPointer);
      //std::cerr << "Read " << rlen << " of " << no_left << " words\n";
      if (!rlen)
	break; // Shouldn't happen!
      while (bpos < rlen) {
	//std::cerr << buf[bpos] << " ";
	free_blocks[asize].push_back(buf[bpos++]);
	--no_left;
      }
    }
    //std::cerr << ")" << std::endl;
  }
  //std::cerr << "Index::load_meta() end" << std::endl;
}


unsigned int Index::find_union(CPointer pos1, unsigned int n1, CPointer pos2, unsigned int n2, CPointer *buf, unsigned int max_hits, unsigned int from_no) {
  if (lseek(index_fd, pos1 * sizeof(CPointer), SEEK_SET) < 0) {
    std::cerr << "Cannot seek index_fd: " << strerror(errno) << std::endl;
    exit(1);
  }
  if (lseek(dest_fd, pos2 * sizeof(CPointer), SEEK_SET) < 0) {
    std::cerr << "Cannot seek: " << strerror(errno) << std::endl;
    exit(1);
  }

  const unsigned int bsize = 8000;
  CPointer b1[bsize], b2[bsize];

  int b1pos=0, b2pos=0, b1size=0, b2size=0;
  unsigned int no_hits=0;

  while ((n1||(b1pos<b1size)) && (n2||(b2pos<b2size))) {
    std::cerr << n1 << " / " << n2 << " ; " << b1pos << " / " << b2pos << " ; " << b1size << " / " << b2size << std::endl;
    if (n1 and b1pos == b1size) {
      b1size = read(index_fd, b1, ((n1 > bsize) ? bsize : n1) * sizeof(CPointer)) / sizeof(CPointer);
      if (b1size <= 0)
	break;
      b1pos = 0;
      n1 -= b1size;
    }
    if (n2 and b2pos == b2size) {
      b2size = read(dest_fd, b2, ((n2 > bsize) ? bsize : n2) * sizeof(CPointer)) / sizeof(CPointer);
      if (b2size <= 0)
	break;
      b2pos = 0;
      n2 -= b2size;
    }
    while (b1pos < b1size and b2pos < b2size) {
      if (b1[b1pos] == b2[b2pos]) {
	if (from_no > 0)
	  --from_no;
	else {
	  buf[no_hits++] = b1[b1pos];
	  if (no_hits == max_hits)
	    goto DONE;
	}
	++b1pos;
	++b2pos;
      } else if (b1[b1pos] < b2[b2pos]) {
	++b1pos;
      } else {
	++b2pos;
      }
    }
  }
 DONE:
  return no_hits;
}

