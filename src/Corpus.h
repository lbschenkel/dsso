#ifndef __CORPUS_H__
#define __CORPUS_H__

#include "CorpusTypes.h"
#include "StringInfo.h"
#include "Index.h"
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include "StringDB.h"
#include "InfoDB.h"

class Corpus {
public:
  Corpus();
  ~Corpus();
  bool update_index() {
    //do_update_index();
    return incremental_update(2000000);
  }
  void save() {

  }
  std::string line_at(CPointer pos) {
    corpus_fd.seekg(pos, corpus_fd.beg);
    std::string line;
    getline(corpus_fd, line);
    return line;
  }
  CPointer line_search(CPointer pos, std::string word);
  unsigned int search(const char *word, CPointer *buf, unsigned int max_hits, unsigned int from_no = 0);
  unsigned int search(const std::string &word, CPointer *buf, unsigned int max_hits, unsigned int from_no = 0) {
    return search(word.c_str(), buf, max_hits, from_no);
  }
  unsigned int search2(const std::string &word1, const std::string &word2, CPointer *buf, unsigned int max_hits, unsigned int from_no = 0);
  bool is_up_to_date();
 private:
  void get_corpus_size();
  void do_update_index();
  bool incremental_update(CPointer max_increment = 1000000);
  Index idx;
  StringDB &string_db;
  void set_idx(CPointer pos, CPointer value) {
    //std::cerr << "Index " << pos << " = " << value << std::endl;
    idx.set_val(pos, value);
  }
  std::ifstream corpus_fd;
  CPointer corpus_size;
  InfoDB<StringInfo, EmptyStringInfo> &_infodb;
};


#endif
