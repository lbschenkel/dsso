#include <string.h>
#include <iostream>
#include <fstream>
#include "HunInfoDB.h"
#include "StringInfo.h"
#include "StringSort.h"
#include "InfoDB.h"
#include <glib.h>
#include <locale>

int main() {
  StringDB &sdb = StringDB::get_db();
  HunInfoDB *huninfodb = new HunInfoDB();
  std::map<std::string, unsigned char> ext2cat;
  ext2cat["se"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["com"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["nu"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["org"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["net"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["dk"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["no"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["jp"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["fi"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["eu"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["it"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["tv"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["uk"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["exe"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["txt"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["text"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["jpg"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["jpeg"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["html"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["htm"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["dll"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["php"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["bin"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["cpp"] = SPELL_CAT_IGNORE_FORGET;
  ext2cat["png"] = SPELL_CAT_IGNORE_FORGET;
  //for (StringDB::iterator it = sdb.end(); it!= sdb.end(); ++it) {
  for (StringDB::iterator it = sdb.begin(); it!= sdb.end(); ++it) {
    Glib::ustring word(*it);
    unsigned char cat = 0;
    int last_dot = -1, pos = 0, dotcount = 0;
    for (Glib::ustring::const_iterator cp=word.begin(); cp!=word.end(); ++cp) {
      if (*cp >= 1024) {
	cat = SPELL_CAT_IGNORE_FORGET;
	break;
      }
      if (*cp == '.') {
	last_dot = pos;
	++dotcount;
      }
      ++pos;
    }
    if (cat) {
	huninfodb->set_spell_cat(it, cat);
	continue;
    }
    if (last_dot > 0 and pos - last_dot > 1) {
      Glib::ustring ext = word.substr(last_dot+1);
      std::map<std::string, unsigned char>::const_iterator p = ext2cat.find(ext);
      if (p != ext2cat.end()) {
	huninfodb->set_spell_cat(it, p->second);
	continue;
      }
    }
  }

  for (StringDB::iterator it = sdb.begin(); it!= sdb.end(); ++it) {
    if (unsigned char cat = huninfodb->get_spell_cat(it)) {
      std::cerr << *it << " " << (int) cat << std::endl;
    }
  }

  delete huninfodb;
}
