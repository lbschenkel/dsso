#include <iostream>
#include <string>

#include "DictDB.h"

int main(int argc, char *argv[]) {
  DictDB dssodb("test_dsso_db.txt", "test_dsso_db.bin", "test_dsso_db.journal", "test_dsso_db_word.txt");
  std::set<unsigned int> res = dssodb.search_wf("Aaltos");
  std::cerr << "N.o. results: " << res.size() << std::endl;
  if (res.size()) {
    SyU *olle = dssodb.get_syu(*res.begin());
    const std::vector<WF> &wflist = olle->wfs();
    std::cerr << "OriG: " << olle->save() << std::endl;
    std::cerr << "SyU: " << olle->gc();
    for (unsigned int i = 0; i < wflist.size(); ++i) {
      std::cerr << " / " << wflist[i].word();
    }
    std::cerr << std::endl;
  }
  {
    SyU *syu = dssodb.get_syu(1);
    std::cerr << "SyU: " << syu->show_long() << std::endl;
  }

  for (DictDB::iterator syu = dssodb.begin(); syu != dssodb.end(); ++syu) {
    std::cerr << (*syu)->show_long() << std::endl;
  }

  /*
  SyU *syu = dssodb.create_syu("ska1", 1);
  syu->add_wf(1, 3, "ska1s", "ovanlig");
  syu->add_wf(2, 4, "ska1et");
  syu->add_wf(3, 5, "ska1ets");
  syu = dssodb.save(syu);
  std::cerr << "New:  " << syu->save() << std::endl;
  */

  /*
  olle->set_wf(1, "Olles", "redundant");
  olle->set_wf(0, "OllE", "");
  olle->set_wf(1, "Olles", "");
  std::cerr << "Modified: " << (olle->is_modified() ? "yes" : "no") << std::endl;
  std::cerr << "New:  " << olle->save() << std::endl;
  */
  res = dssodb.search_wf("ska1et");
  if (res.size()) {
    unsigned int hundid = *res.begin();
    SyU *hund(dssodb.get_syu(hundid));
    std::vector<WF> wflist = hund->wfs();
    //std::cerr << "Orig: " << hund.save() << std::endl;
    std::cerr << "ORD: " << hund->gc();
    for (unsigned int i = 0; i < wflist.size(); ++i) {
      std::cerr << " / " << wflist[i].word();
    }
    std::cerr << std::endl;
    exit(0);
    //hund->set_wf(1, "Hunds", "redundant");
    //hund->set_wf(0, "Hund", "");
    //hund->set_wf(1, "Hunds", "");
    hund->add_wf(8, 2, "hound", "skoj");
    hund->set_wf(3, "");
    std::cerr << "Modified: " << (hund->is_modified() ? "yes" : "no") << std::endl;
    //std::cerr << "New:  " << hund->save() << std::endl;
    dssodb.save(hund);
  }
 
}
