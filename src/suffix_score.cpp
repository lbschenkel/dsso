#include <string.h>
#include <iostream>
#include <fstream>
#include "HunInfoDB.h"

int main(int argc, char *argv[]) {

  HunInfoDB *huninfodb = new HunInfoDB();

  if (argc != 2) {
    std::cerr << "usage: " << argv[0] << " filename" << std::endl;
    exit(1);
  }
  std::ifstream words(argv[1]);
  
  for (std::string word; words >> word; ) {
    std::cout << word << " " << huninfodb->valid_suffix_score(word.c_str(), true) << std::endl;
  }
  delete huninfodb;
}
