#ifndef __INDEX_H__
#define __INDEX_H__

/*

Index:

The Index is a storage area for CPointer values. It is structured into segments.
The length of each segment is at least 4 and is a power of two, i.e. a segment 
can contain 4,8,16,32,64,128,... CPointer values.

At each position "pos" in the Index, a value "val" may be stored at position "pos" using the method
  set_val(pos, val)

*/

#include "CorpusTypes.h"

// The key is the size of a storage area (i.e. one of 4, 8, 16, ...),
// the value is a vector of positions of free areas of the given size
// in the Index:
typedef std::map<unsigned long, std::vector<CPointer> > BlockMap;

class Index {
public:
  Index();
  ~Index();
  void load_hits( CPointer pos, CPointer *buffer, unsigned int no_hits ) {
    lseek(index_fd, pos * sizeof(CPointer), SEEK_SET);
    read(index_fd, buffer, no_hits * sizeof(CPointer));
  }
  void set_val(CPointer pos, CPointer val) {
    lseek(index_fd, pos * sizeof(CPointer), SEEK_SET);
    write(index_fd, &val, sizeof(CPointer));
  }
  unsigned int find_union(CPointer pos1, unsigned int n1, CPointer pos2, unsigned int n2, CPointer *buf, unsigned int max_hits, unsigned int from_no);
  CPointer alloc(unsigned long size);
  void copy_idx(CPointer oldpos, unsigned int count, CPointer newpos);
  void dealloc(unsigned long size, CPointer pos);
  void set_pos(CPointer pos) {
    index_pos = pos;
    save_meta();
  }
  CPointer curr_pos() {
    return index_pos;
  }
private:
  int index_fd, dest_fd;
  CPointer index_pos;
  CPointer index_size;
  BlockMap free_blocks;
  void clear_meta();
  void save_meta();
  void load_meta();
};


#endif
