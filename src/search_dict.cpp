#include <iostream>
#include <string>

#include "DictDB.h"

int main(int argc, char *argv[]) {
  if (argc != 6) {
    std::cerr << "Usage: " << argv[0] << " dict word" << std::endl;
  }
  //DictDB dssodb("test_dsso_db.txt", "test_dsso_db.bin", "test_dsso_db.journal", "test_dsso_db_wf.txt");
  DictDB dict(argv[1], argv[2], argv[3], argv[4]);
  std::set<unsigned int> res = dict.search_wf(argv[5]);
  std::vector<unsigned int> res1(res.begin(), res.end());
  std::sort(res1.begin(), res1.end());
  for (std::vector<unsigned int>::const_iterator it = res1.begin();
       it!=res1.end(); ++it) {
    SyU *syu = dict.get_syu(*it);
    std::cout << syu->show_long() << std::endl;
    /*
    const std::vector<WF> &wflist = syu->wfs();
    std::cout << "OriG: " << syu->save() << std::endl;
    std::cout << "SyU: " << syu->gc();
    for (unsigned int i = 0; i < wflist.size(); ++i) {
      std::cout << " / " << wflist[i].word();
    }
    std::cout << std::endl;
    */
  }
  /*

  {
    SyU *syu = dict.get_syu(1);
    std::cout << "SyU: " << syu->show_long() << std::endl;
  }

  for (DictDB::iterator syu = dict.begin(); syu != dict.end(); ++syu) {
    std::cout << (*syu)->show_long() << std::endl;
  }

  SyU *syu = dict.create_syu("ska1", 1);
  syu->add_wf(1, 3, "ska1s", "ovanlig");
  syu->add_wf(2, 4, "ska1et");
  syu->add_wf(3, 5, "ska1ets");
  syu = dict.save(syu);
  std::cout << "New:  " << syu->save() << std::endl;
  */

  /*
  olle->set_wf(1, "Olles", "redundant");
  olle->set_wf(0, "OllE", "");
  olle->set_wf(1, "Olles", "");
  std::cout << "Modified: " << (olle->is_modified() ? "yes" : "no") << std::endl;
  std::cout << "New:  " << olle->save() << std::endl;
  res = dict.search_wf("ska1et");
  if (res.size()) {
    unsigned int hundid = *res.begin();
    SyU *hund(dict.get_syu(hundid));
    std::vector<WF> wflist = hund->wfs();
    //std::cout << "Orig: " << hund.save() << std::endl;
    std::cout << "ORD: " << hund->gc();
    for (unsigned int i = 0; i < wflist.size(); ++i) {
      std::cout << " / " << wflist[i].word();
    }
    std::cout << std::endl;
    exit(0);
    //hund->set_wf(1, "Hunds", "redundant");
    //hund->set_wf(0, "Hund", "");
    //hund->set_wf(1, "Hunds", "");
    hund->add_wf(8, 2, "hound", "skoj");
    hund->set_wf(3, "");
    std::cout << "Modified: " << (hund->is_modified() ? "yes" : "no") << std::endl;
    //std::cout << "New:  " << hund->save() << std::endl;
    dict.save(hund);
  }
  */
 
}
