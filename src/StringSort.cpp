template <typename C>
unsigned int StringSort<C>::find(const char *str) {
  unsigned int lastpos = map_len / sizeof(unsigned int), firstpos = 0;
  //std::cerr << "Find " << str << "; FP/LP: " << firstpos << "/" << lastpos << std::endl;
  while (true) {
    if (lastpos == firstpos)
      return 0;
    unsigned int pos = (firstpos+lastpos) / 2;
    int res = cmp.cmp(str, map_addr[pos]);
    //std::cerr << "pos=" << pos << " StrNumber=" << map_addr[pos] << ", str: " << sdb[map_addr[pos]] << ", res=" << res << std::endl;

    //std::cerr << "FP/LP: " << firstpos << "/" << lastpos << std::endl;
    if (!res)
      return map_addr[pos];
    if (res < 0) {
      if (firstpos == pos)
	return 0;
      firstpos = pos;
    } else {
      lastpos = pos;
    }
  }
  return 0;
}

template <typename C>
typename StringSort<C>::iterator StringSort<C>::find_first(const char *str) {
unsigned int lastpos = map_len / sizeof(unsigned int), firstpos = 0;
//std::cerr << "Find " << str << "; FP/LP: " << firstpos << "/" << lastpos << std::endl;
    while (true) {
      unsigned int pos = (firstpos+lastpos) / 2;
      if (pos == firstpos)
	return iterator(map_addr+lastpos);
      int res = cmp.cmp(str, map_addr[pos]);

      //std::cerr << "pos=" << pos << " StrNumber=" << map_addr[pos] << ", str: " << sdb[map_addr[pos]] << ", res=" << res << std::endl;

      //std::cerr << "FP/LP: " << firstpos << "/" << lastpos << std::endl;
      if (!res)
	return iterator(map_addr+pos);
      if (res < 0) {
	firstpos = pos;
      } else {
	lastpos = pos;
      }
    }
    return iterator(map_addr);
  }

template <typename C>
void StringSort<C>::do_mmap() {
  if (map_fd < 0) {
    map_fd = open(filename.c_str(), O_RDWR | O_CREAT, 0664);
    map_addr = 0;
    map_len = 0;
  }
  struct stat st;
  if (fstat(map_fd, &st) < 0) {
    throw BadIndex("Cannot stat file " + filename);
  }
  if (map_addr and (size_t)st.st_size == map_len)
    return; // Already mapped
  if (st.st_size > 0) {
    if (map_addr) {
      map_addr = (unsigned int *)
	mremap(map_addr, map_len, st.st_size, MREMAP_MAYMOVE);
    } else {
      map_addr = (unsigned int *)
	mmap(NULL, st.st_size, PROT_READ|PROT_WRITE, MAP_SHARED, map_fd, 0);
    }
  } else {
    map_addr = 0;
  }
  map_len = st.st_size;
  //std::cerr << "Mapped " << filename << " at " << map_addr << ", len=" << map_len << std::endl;
}

template <typename C>
void StringSort<C>::do_unmap() {
  if (map_fd < 0 or map_addr == 0)
    return;
  if (munmap(map_addr, map_len) < 0) {
      throw BadIndex("Cannot unmap file " + filename);
  }
  if (close(map_fd) < 0) {
    throw BadIndex("Cannot close file " + filename);
  }
  map_fd = -1;
}

template <typename C>
StringSort<C> *StringSort<C>::the_db = 0;

/*
   Create new mapping, write new IDs at end of new mapping, sort all new IDs,
   merge old (sorted) IDs with new IDs; release old mapping.
*/
template <typename C>
bool StringSort<C>::add_missing_string_ids() {
  StringDB::iterator it(max_id), the_end(sdb->end());
  if (it == the_end)
    return false;
  //std::cerr << "addr=" << map_addr << ", len=" << map_len << ", fd=" << map_fd << ", pos=" << lseek(map_fd, 0, SEEK_CUR) << "\n";
  //std::cerr << "Update " << filename << " - MAX=" << max_id << ", " << "END " << the_end.string_number() << "\n";
  size_t nsize = 0;
  for (StringDB::iterator p=it; p!=the_end; ++p)
    if (cmp.filter(*p))
      ++nsize;
  nsize = map_len+sizeof(unsigned int)*nsize;
  std::string tmpfilename = filename + ".tmp";
  int tfd = open(tmpfilename.c_str(), O_RDWR | O_CREAT, 0664);
  if (ftruncate(tfd, nsize) < 0)
    throw BadIndex("Cannot resize file " + tmpfilename);
  unsigned int *tmp_addr = (unsigned int *)
    mmap(NULL, nsize, PROT_READ|PROT_WRITE, MAP_SHARED, tfd, 0);
  if (tmp_addr == MAP_FAILED)
    throw BadIndex("Cannot mmap file " + tmpfilename);
  unsigned int *new_ids = tmp_addr + map_len/sizeof(unsigned int);
  unsigned int *new_end;
  //std::cerr << "Old len = " << map_len << ", new len=" << nsize << ", new addr="
  //	    << tmp_addr << ", new_ids=" << new_ids << std::endl;
  //std::cerr << "SNO=" << it.string_number() << std::endl;

  for (new_end=new_ids; it != the_end; ++it)
    if (cmp.filter(*it))
      *new_end++ = it.string_number();
  max_id = it.string_number();
  std::sort(iterator(new_ids), iterator(new_end), cmp);

  if (map_len) {
    unsigned int *old_ids = map_addr, *old_end = old_ids+map_len/sizeof(unsigned int);
    unsigned int *pos = tmp_addr;
    long factor = map_len / (nsize-map_len);
    if (factor < 10) {
      while (old_ids < old_end and new_ids < new_end) {
	*pos++ = cmp(*old_ids, *new_ids) ? *old_ids++ : *new_ids++;
      }
    } else {
      // Only added a small amount of IDs. Since the cmp method may be slow,
      // try to lower the number of cmp evaluations.
      while (old_ids < old_end and new_ids < new_end) {
	long n = factor, lower = 0;
	while ( (old_ids+n < old_end) and cmp( *(old_ids+n), *new_ids ) ) {
	  lower = n;
	  n *= 2;
	}
	if (old_ids+n >= old_end) {
	  n = old_end-old_ids-1;
	  if ( cmp( *(old_ids+n), *new_ids ) ) {
	    break;
	  }
	}
	// We know cmp( *(old_ids+upper), *new_ids ) is false.
	// If lower > 0, we know cmp( *(old_ids+lower), *new_ids ) is true,
	// but it may be that lower = 0 and cmp( *(old_ids+0), *new_ids ) is false.
	long upper = n;
	while (true) {
	  n = (lower+upper)/2;
	  if (n == lower) {
	    break;
	  }
	  if ( cmp( *(old_ids+n), *new_ids ) )
	    lower = n;
	  else
	    upper = n;
	}
	if (lower == 0) {
	  if ( cmp(*old_ids, *new_ids) ) {
	    *pos++ = *old_ids++;
	    *pos++ = *new_ids++;
	  } else {
	    *pos++ = *new_ids++;
	  }
	} else {
	  ++lower;
	  size_t len = lower * sizeof(unsigned int);
	  memmove(pos, old_ids, len);
	  pos += lower;
	  old_ids += lower;
	  *pos++ = *new_ids++;
	}
      }
    }

    while (old_ids < old_end)
      *pos++ = *old_ids++;
  }
  do_unmap();
  map_fd = tfd;
  map_addr = tmp_addr;
  map_len = nsize;
  if (rename(tmpfilename.c_str(), filename.c_str()) < 0)
    throw BadIndex("Cannot rename file " + tmpfilename);
  std::ofstream cfgfd(config_filename.c_str());
  cfgfd << filename << ' ' << max_id;
  cfgfd.close();
  if (!cfgfd) {
    throw BadIndex("Cannot write to file " + config_filename);
  }
  modified = true;
  return true;
}


template <typename C>
StringDB *StringSort<C>::sdb = 0;

template <typename C>
StringSort<C>::StringSort(const std::string &cfg_filename)
: config_filename(cfg_filename), cmp(C()) {
  map_fd = -1;
  map_addr = 0;
  map_len = 0;
  std::ifstream cfgfd(config_filename.c_str());
  if (cfgfd) {
    cfgfd >> filename >> max_id;
    if (!cfgfd)
      throw BadIndex("Cannot read from file " + config_filename);
  } else {
    unsigned int l = config_filename.length();
    if (config_filename.substr(l-4) == ".txt")
      filename = config_filename.substr(0, l-4) + ".bin";
    else
      filename = config_filename + ".bin";
    max_id = 1;
  }
  do_mmap();
  refresh();
  atexit(&clean_up);
}


/*
template <typename C>
bool StringSort<C>::add_missing_string_ids() {
  StringDB::iterator it(max_id), the_end(sdb->end());
  if (it == the_end)
    return false;
  //std::cerr << "addr=" << map_addr << ", len=" << map_len << ", fd=" << map_fd << ", pos=" << lseek(map_fd, 0, SEEK_CUR) << "\n";
  //std::cerr << "Update " << filename << " - MAX=" << max_id << ", " << "END " << the_end.string_number() << "\n";
  lseek(map_fd, 0, SEEK_END);
  do {
    unsigned int buf = it.string_number();
    //std::cerr << "Add stringNumber " << buf << std::endl;
    if (write(map_fd, &buf, sizeof(buf)) != sizeof(buf)) {
      throw BadIndex("Cannot write to file " + filename);
    }
  } while (++it != the_end);
  max_id = it.string_number();
  do_mmap();
  //std::cerr << "addr=" << map_addr << ", " << "len=" << map_len << "fd=" << map_fd << "\n";
  std::sort(begin(), end(), cmp);
  std::ofstream cfgfd(config_filename.c_str());
  cfgfd << filename << ' ' << max_id;
  cfgfd.close();
  if (!cfgfd) {
    throw BadIndex("Cannot write to file " + config_filename);
  }
  modified = true;
  return true;
}
*/
