#include <iostream>
#include <fstream>
#include <functional>
#include <string>
#include <utility>
#include <vector>
#include <map>
#include <exception>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "csiphash.h"

class BadScanner : public std::exception {
public:
  BadScanner(std::string explain) : err(explain) {
  }
  virtual const char* what() const throw() {
    return err.c_str();
  }
private:
  std::string err;
};

class Hbuf {
public:
  Hbuf(int filedes) : fd(filedes) {
    data = (size_t *) calloc(bmax, sizeof(size_t));
    if (!data)
      throw BadScanner(std::string("cannot allocate memory"));
    load();
  }
  ~Hbuf() {
    if (data) {
      delete data;
      data = (size_t *) 0;
      close(fd);
    }
  }
  size_t curr() {
    if (bpos < 0 or bpos > bmax)
      throw BadScanner(std::string("bad index"));
    return data[bpos];
  }
  bool next() {
    if (++bpos < bsize)
      return true;
    return load();
  }
private:
  unsigned int bsize, bpos;
  size_t *data;
  const unsigned int bmax = 65536;
  int fd;
  Hbuf(const Hbuf &old);
  bool load() {
    ssize_t n = read(fd, data, bmax*sizeof(size_t));
    bpos = 0;
    if (!n) {
      bsize = 0;
      bpos = 0;
      data[0] = 0;
      return false;
    }	
    if (n<0)
      throw BadScanner(std::string("cannot read existing hash values: ") + strerror(errno));
    bsize = n/sizeof(size_t);
    return true;
  }
};

class CorpusScanner {
public:
  CorpusScanner(const std::string filename, unsigned int no_lines);
  CorpusScanner();
  void scan();
  void merge();
  void rescan(const char *filename_txt);
  void open_all_files(const char *dirname_val, const char *output_file = 0);
private:
  size_t *value;
  std::ifstream corpus_fd;
  unsigned int line_count;
  unsigned int count = 0;
  std::map<size_t, bool> stash;
  std::vector<Hbuf *> vfile;
  int output_fd;
};

CorpusScanner::CorpusScanner() :
  value(0), line_count(0), count(0)
{
}

// Merge all existing files, save to new file
void CorpusScanner::merge() {
  
  while (vfile.size()) {
    size_t val = vfile[0]->curr();
    unsigned int lindex = 0;

    for (unsigned int i=1; i<vfile.size(); ++i) {
      if (vfile[i]->curr() < val) {
	val = vfile[i]->curr();
	lindex = i;
      }
    }
    ssize_t n = write(output_fd, (const char *)&val, sizeof(val));
    if (n != sizeof(val))
      throw BadScanner("cannot write to output file");
    if (!vfile[lindex]->next()) {
      delete vfile[lindex];
      if (lindex+1 < vfile.size()) {
	vfile[lindex] = vfile[vfile.size()-1];
      }
      vfile.pop_back();
    }
  }
}

CorpusScanner::CorpusScanner(const std::string filename, unsigned int no_lines) :
  value( (size_t *) calloc(no_lines, sizeof(size_t)) ),
  corpus_fd(filename),
  line_count(no_lines),
  count(0)
{
  if (!value)
    throw BadScanner("cannot allocate memory");
  if (!corpus_fd)
    throw BadScanner(std::string("cannot open file ") + filename);
}

int less_than(const void *a, const void *b) {
  if (*(size_t *) a > *(size_t *)b)
    return 1;
  if (*(size_t *) a < *(size_t *)b)
    return -1;
  return 0;
}

const char key[16] = {0,1,2,3,4,5,6,7,8,9,0xa,0xb,0xc,0xd,0xe,0xf};

void CorpusScanner::scan() {
  std::string line;
  size_t *vpos = value;
  while ( getline(corpus_fd, line) and count < line_count) {
    vpos[count++] = siphash24(line.c_str(), line.size(), key);
  }
  std::cerr << "Read " << count << " lines; ";
  if (corpus_fd.eof())
    std::cerr << "no more lines.";
  else if (count == line_count)
    std::cerr << "reach maximal number of lines.";
  else
    std::cerr << "file error.";
  //std::cerr << " Now sorting..." << std::endl;
  line_count = count;
  qsort(value, line_count, sizeof(size_t), less_than);
  //std::cerr << " Now sorted." << std::endl;
}

void CorpusScanner::open_all_files(const char *dirname_val, const char *output_file) {
  char filename_val[100];

  // Open all existing hash value files:
  while (true) {
    sprintf(filename_val, "%s/lhash%03lu.bin", dirname_val, vfile.size());
    int nfd = open(filename_val, O_RDONLY, 0666);
    if (nfd < 0) {
      if (errno == ENOENT)
	break;
      throw BadScanner(std::string("cannot open file ") + filename_val + ": " + strerror(errno));
    }
    vfile.push_back(new Hbuf(nfd));
  }  
  output_fd = open(output_file ? output_file : filename_val, O_WRONLY | O_EXCL | O_CREAT, 0666);
  if (output_fd < 0) {
    throw BadScanner(std::string("cannot create file ") + (output_file ? output_file : filename_val) + ": " + strerror(errno));
  }
}

void CorpusScanner::rescan(const char *filename_txt) {

  if (!line_count)
    return;

  size_t *next(value), *last(value+line_count);
  unsigned int save_pos = 0;

  while (true) {
    for (unsigned long i=0; i<vfile.size(); ) {
      while (vfile[i]->curr() < *next) {
	if (!vfile[i]->next()) {
	  delete vfile[i];
	  if (i+1 == vfile.size()) {
	    vfile.pop_back();
	    goto NEXT_VFILE;
	  }
	  vfile[i] = vfile[vfile.size()-1];
	  vfile.pop_back();
	}
      }
      if (vfile[i]->curr() == *next) {
	stash[*next++] = false;
	++save_pos;
	while (next < last and *(next-1) == *next) {
	  ++save_pos;
	  ++next;
	}
	if (next == last)
	  goto ScanDone;
	continue;
      }
    NEXT_VFILE:
      ++i;
    }
    if (++next == last)
      goto  ScanDone;
    if ( *(next-1) == *next) {
      stash[*next] = true;
      do {
	++next;
	++save_pos;
	if (next == last)
	  goto ScanDone;
      } while (*(next-1) == *next);
    } 
    if (save_pos)
      *(next-save_pos) = *next;
  }
 ScanDone:

  // Now save points after last saved hash value.

  std::cerr << "Found " << stash.size() << " repeated lines." << std::endl;

  /*
  if (!stash.size()) {
    std::cerr << "No rewrite needed." << std::endl;
    return;
  }
  */
  size_t to_save = (line_count - save_pos)*sizeof(size_t);
  const char *pos = (const char *) value;
  size_t to_write = 65536;
  while (to_save) {
    if (to_write > to_save)
      to_write = to_save;
    ssize_t n = write(output_fd, pos, to_write);
    if (n < 0) {
      //std::cerr << "Fel: " << strerror(errno) << std::endl;
      throw BadScanner("cannot write to output file");
    }
    to_save -= n;
    pos += n;
  }
  delete value;
  close(output_fd);

  std::ofstream outfile(filename_txt);
  if (!outfile) {
    throw BadScanner(std::string("cannot open file ") + filename_txt);
  }

  corpus_fd.clear();
  corpus_fd.seekg(0, corpus_fd.beg);

  count = 0;

  std::string line;
  while ( getline(corpus_fd, line) and count < line_count) {
    size_t hval = siphash24(line.c_str(), line.size(), key);
    if (stash.find(hval) == stash.end()) {
      outfile << line << std::endl;
    } else if (stash[hval]) {
      outfile << line << std::endl;
      stash[hval] = false;
    }
  }
  outfile.close();
}

int main(int argc, char *argv[]) {
  if (argc == 2) {
    CorpusScanner cs;
    cs.open_all_files(argv[1]);
    cs.merge();
  } else if (argc == 5) {
    CorpusScanner cs(argv[1], atoi(argv[2]));
    cs.open_all_files(argv[4]);
    cs.scan();
    cs.rescan(argv[3]);
  } else {
    std::cerr << "Usage: " << argv[0] << " [ infile linecount outfile ] indexdir" << std::endl;
    exit(1);
  }
  exit(0);
}
