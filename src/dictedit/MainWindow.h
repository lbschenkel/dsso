#ifndef __MAINWONDOW_H_
#define __MAINWONDOW_H_

#include <gtkmm/notebook.h>
#include <gtkmm/window.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/texttagtable.h>

#include "MenuBar.h"
#include "Worker.h"

class Tab;

class MainWindow : public Gtk::Window
{

public:
  MainWindow();
  virtual ~MainWindow();
  void new_search_tab(std::string word = "");
  void new_spell_tab(std::string word = "");
  void new_edit_tab(std::string word = "", std::string info = "");
  void get_new_tab(const char *title, Tab *tab,
		   std::string word = "");
  void cb_dump_old_format();
  void cb_build_hunspell();
  void cb_build_fi_hunspell();
  void cb_dump_spell_categories();
  void cb_clear();
  void cb_close_tab();
  void cb_quit();
  void cb_spell();
  void cb_reload_devel_dictionary();
  void cb_reload_base_dictionary();
  void cb_reload_master_dictionary();
  void goto_last_tab();
  void cb_search(const Glib::ustring search_str);
  void cb_search_start(const Glib::ustring search_str);
  void cb_search_middle(const Glib::ustring search_str);
  void cb_search_end(const Glib::ustring search_str);
  void cb_search_re(const Glib::ustring search_str);
  void cb_search_clear();
  void cb_set_spell_cat(unsigned char cat);
  void set_status_text(std::string msg) {
    statusbar.set_text(msg);
  }
  void set_jobmsg_text(std::string msg) {
    jobmsg.set_text(msg);
  }
  void set_search_string(std::string str) {
    menubar->set_search_string(str);
  }
  bool check_for_work();
  void notify() {
    dispatcher.emit();
  }
protected:
  bool on_delete_event(GdkEventAny *event);
  void cb_switch_tab(Gtk::Widget *page, guint page_num);
  Gtk::Box main_area;
  Gtk::Notebook main_notebook;
  MenuBar *menubar;
  Gtk::Box statusarea;
  Gtk::Label statusbar;
  Gtk::Label jobmsg;
  Glib::RefPtr<Gtk::TextTagTable> tagTable;
private:
  void worker_notification_cb();
  void add_tags();
  std::vector<Tab *> tabs;
  unsigned int _current_tab;
  Glib::Dispatcher dispatcher;
  Worker the_worker;
  Glib::Threads::Thread *worker_thread;
};

#endif
