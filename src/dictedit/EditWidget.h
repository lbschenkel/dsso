#ifndef __EDITWIDGET_H_
#define __EDITWIDGET_H_

#include <gtkmm/window.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/eventbox.h>
#include <gtkmm/textview.h>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/grid.h>
#include <gtkmm/entry.h>

class SyU;
class Tab;

class EditWidget : public Gtk::ScrolledWindow
{

public:
  EditWidget(Tab *parent_tab);
  virtual ~EditWidget();
  virtual void cb_clear();
  virtual void cb_spell();
  virtual void cb_search(const Glib::ustring search_str);
  void open_syu(unsigned int syuid, unsigned int tab_id);
  void open_syu(SyU *a_syu, unsigned int tab_id);
  void new_syu(std::string word, unsigned int gc, unsigned int tab_id);
  void cb_wf_entry(unsigned int row);
  void cb_wf_modify(unsigned int row);
  void cb_new_wf(unsigned int icno);
  void cb_attr_modify(const char *attr, Gtk::Entry *entry);
  void cb_save_syu();
  void cb_analyse();
  bool cb_suffix_menu(GdkEventButton* event);
  bool cb_prefix_menu(GdkEventButton* event);
  bool cb_spellFlags_menu(GdkEventButton* event);
  void cb_set_spellFlags(std::string sflags);
  bool cb_tags_menu(GdkEventButton* event);
  bool cb_pcomp_menu(GdkEventButton* event);
  bool cb_mcomp_menu(GdkEventButton* event);
  bool cb_bcomp_menu(GdkEventButton* event);
  void cb_set_tags(std::string sflags);
  void cb_set_prefix(unsigned int id);
  void cb_set_suffix(unsigned int id);
  bool cb_wf_category_menu(GdkEventButton* event, unsigned int rnum);
  void cb_set_wf_category(unsigned int rnum, unsigned char cat);
  void cb_set_comp(std::string pref, unsigned int row);
  void cb_remove_widget(Gtk::Widget &wid);
  void cb_apply_pattern(std::string pname);
  void refresh_wfs();
  SyU *curr_open_syu() {
    return syu;
  }
  SyU *curr_analyse_syu() {
    return analyse_syu;
  }
 protected:
  Gtk::Box main_vbox;
  Gtk::Grid *edit_grid;
  Gtk::Button *analysis_btn;
  Gtk::Button *savebtn;
  Gtk::Box pattern_hbox;
  Gtk::Menu suffixMenu;
  Gtk::EventBox suffix_box;
  Gtk::Menu prefixMenu;
  Gtk::EventBox prefix_box;

  Gtk::Menu wfCatMenu;

  Gtk::Menu tagsMenu;
  Gtk::EventBox tagsBox;
  Gtk::Label tagsLabel;

  Gtk::Menu spellFlagsMenu;
  Gtk::EventBox spellFlagsBox;
  Gtk::Label spellFlagsLabel;

  Gtk::Menu pCompMenu;
  Gtk::EventBox pCompBox;
  Gtk::Label pCompLabel;

  Gtk::Menu mCompMenu;
  Gtk::EventBox mCompBox;
  Gtk::Label mCompLabel;

  Gtk::Menu bCompMenu;
  Gtk::EventBox bCompBox;
  Gtk::Label bCompLabel;
  
  Gtk::Box compound_vbox;
  Gtk::Box other_syus_vbox;
  Gtk::Box corpus_vbox;
  Gtk::Label suffix_lbl, prefix_lbl, suffix_prompt, prefix_prompt;
 private:
  void check_modified();
  void show_other_syu(unsigned int syuid);
  void open_current_syu();
  unsigned int next_row_number;
  unsigned int wfno;
  SyU *syu;
  SyU *analyse_syu;
  Tab *parent;
  void add_edit_grid_row(unsigned int icno,
			 std::string word = "",
			 std::string tag = "");
  std::map<unsigned int,unsigned int> row2wfno;
  std::set<unsigned int> current_string_numbers;
  std::set<CPointer> current_line_numbers;
  std::set<unsigned int> current_other_syuids;
};

#endif
