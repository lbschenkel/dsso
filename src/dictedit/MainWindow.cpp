#include <sys/types.h>
#include <sys/wait.h>
#include <iostream>

#include <gtkmm/textview.h>
#include <gtkmm.h>

#include "MainWindow.h"
#include "SpellTab.h"
#include "SearchTab.h"
#include "EditTab.h"

MainWindow::MainWindow()
  : main_area(Gtk::ORIENTATION_VERTICAL),
    menubar(Gtk::manage(new MenuBar(this))),
    statusarea(Gtk::ORIENTATION_HORIZONTAL),
    statusbar("", Gtk::ALIGN_START),
    jobmsg("", Gtk::ALIGN_START),
    tagTable(Gtk::TextTagTable::create()),
    dispatcher(),
    the_worker(),
    worker_thread(0),
    _current_tab(0)
{
  set_icon_name("fonts");

  main_notebook.signal_switch_page().
    connect(sigc::mem_fun(*this,
			  &MainWindow::cb_switch_tab) );
  main_area.pack_start(*menubar, Gtk::PACK_SHRINK);
  main_area.pack_start(main_notebook);
  main_area.pack_start(statusarea, Gtk::PACK_SHRINK);
  statusarea.pack_start(statusbar);
  statusarea.pack_start(jobmsg, Gtk::PACK_SHRINK);
  main_notebook.show();
  main_area.show();
  menubar->show();
  statusbar.show();
  jobmsg.show();
  statusarea.show();
  add(main_area);
  add_tags();

  dispatcher.connect(sigc::mem_fun(*this, &MainWindow::worker_notification_cb));

  sigc::slot<bool> cfw = sigc::mem_fun(*this,
				       &MainWindow::check_for_work);
  Glib::signal_timeout().connect(cfw, 2000);

  //show_all_children();
}

void MainWindow::worker_notification_cb() {
  double fraction_done;
  Glib::ustring message;
  if (worker_thread && the_worker.stopped())
  {
    worker_thread->join();
    worker_thread = 0;
  }
  the_worker.get_info(&fraction_done, &message);
  set_jobmsg_text(message);
}

MainWindow::~MainWindow()
{
}

bool MainWindow::check_for_work() {
  if (Dict::corpus) {
    if (Dict::corpus->update_index()) {
      if (Dict::corpus->is_up_to_date()) {
	set_status_text("Updating indices, this may take a few minutes. Please wait...");
	Dict::alpha->refresh();
	Dict::beta->refresh();
	Dict::freq->refresh();
	Dict::huninfodb->refresh();
	set_status_text("Updated indices.");
      }
    }
  }
  return true;
}

void MainWindow::cb_close_tab() {
  if (tabs.size() < 2)
    return;
  tabs[_current_tab]->theMenuBarBox()->hide();
  unsigned int remove = _current_tab;
  _current_tab = 1000000;
  Tab *old_tab = tabs[remove];
  tabs.erase(tabs.begin()+remove);
  main_notebook.remove_page(remove);
  delete old_tab;
  _current_tab = main_notebook.get_current_page();
  tabs[_current_tab]->theMenuBarBox()->show();
}
void MainWindow::cb_reload_devel_dictionary() {
  Dict::huninfodb->reload_devel();
}
void MainWindow::cb_reload_base_dictionary() {
  Dict::huninfodb->reload_base();
}
void MainWindow::cb_reload_master_dictionary() {
  Dict::huninfodb->reload_sv_se();
}

void MainWindow::goto_last_tab() {
  main_notebook.set_current_page(-1);
}

void MainWindow::cb_dump_old_format() {
  Dict::dict_db->dump_old_format("tmp_db.json");
}

void MainWindow::new_search_tab(std::string word) {
  get_new_tab("Search", new SearchTab(this, tagTable, word), word);
}

void MainWindow::new_spell_tab(std::string word) {
  get_new_tab("Spell", new SpellTab(this, tagTable), word);
}

void MainWindow::new_edit_tab(std::string word, std::string info) {
  if (info.find(" ") != std::string::npos)
    get_new_tab("Edit", new EditTab(this, info), word);
  else
    get_new_tab("Edit", new EditTab(this, word), word);
}

void MainWindow::get_new_tab(const char *title, Tab *tab,
			     std::string word)
{
  tabs.push_back(tab);
  main_notebook.append_page(*tab, title);
  menubar->set_search_string(word);
  menubar->add_box(tab->theMenuBarBox());
  tab->show();
  //std::cout << "Adding tab" << std::endl;
}


void MainWindow::add_tags() {
  Glib::RefPtr<Gtk::TextBuffer::Tag> tagp;

  tagp = Gtk::TextBuffer::Tag::create("spell_red");
  tagp->property_background() = "red";
  tagTable->add(tagp);

  tagp = Gtk::TextBuffer::Tag::create("spell_yellow");
  tagp->property_background() = "yellow";
  tagTable->add(tagp);

  tagp = Gtk::TextBuffer::Tag::create("spell_green");
  tagp->property_background() = "green";
  tagTable->add(tagp);

  tagp = Gtk::TextBuffer::Tag::create("spell_orange");
  tagp->property_background() = "orange";
  tagTable->add(tagp);

  tagp = Gtk::TextBuffer::Tag::create("unknown_word");
  tagp->property_weight() = PANGO_WEIGHT_BOLD;
  tagTable->add(tagp);
}

void MainWindow::cb_switch_tab(Gtk::Widget *page, guint page_num) {
  if (_current_tab > 999999)
    return;
  tabs[_current_tab]->theMenuBarBox()->hide();
  _current_tab = page_num;
  tabs[_current_tab]->theMenuBarBox()->show();
  //std::cerr << "Page no: " << _current_tab << std::endl;
}

void MainWindow::cb_build_hunspell() {
  if (!worker_thread)
    worker_thread = Glib::Threads::Thread::create(
      sigc::bind(sigc::mem_fun(the_worker, &Worker::build_hunspell), this,
		 "sv_SE.dic", "sv_FI,kontrollera,redundant,ovanlig"));
}

void MainWindow::cb_build_fi_hunspell() {
  if (!worker_thread)
    worker_thread = Glib::Threads::Thread::create
      (sigc::bind(sigc::mem_fun(the_worker, &Worker::build_hunspell), this,
		  "sv_FI.dic", "kontrollera,webb,redundant,ovanlig"));
}

void MainWindow::cb_dump_spell_categories() {
  Dict::huninfodb->dump_spell_categories();
}

void MainWindow::cb_clear() {
  tabs[_current_tab]->cb_clear();
}

void MainWindow::cb_spell() {
  tabs[_current_tab]->cb_spell();
}

void MainWindow::cb_search(const Glib::ustring search_str) {
  //std::cerr << "MainWindow::cb_search(" << search_str << ")\n";
  tabs[_current_tab]->cb_search(search_str);
}

void MainWindow::cb_search_start(const Glib::ustring search_str) {
  tabs[_current_tab]->cb_search_start(search_str);
}

void MainWindow::cb_search_middle(const Glib::ustring search_str) {
  tabs[_current_tab]->cb_search_middle(search_str);
}

void MainWindow::cb_search_end(const Glib::ustring search_str) {
  tabs[_current_tab]->cb_search_end(search_str);
}

void MainWindow::cb_search_re(const Glib::ustring search_str) {
  tabs[_current_tab]->cb_search_re(search_str);
}

void MainWindow::cb_search_clear() {
  tabs[_current_tab]->cb_search_clear();
}

void MainWindow::cb_set_spell_cat(unsigned char cat) {
  tabs[_current_tab]->cb_set_spell_cat(cat);
}

void MainWindow::cb_quit() {
  Dict::close_dicts();
  hide();
}

bool MainWindow::on_delete_event(GdkEventAny* event) {
  //std::cerr << "Goodbye" << std::endl;
  cb_quit();
  return true;
}
