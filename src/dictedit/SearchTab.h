#ifndef __SEARCHTAB_H_
#define __SEARCHTAB_H_

#include <gtkmm/texttagtable.h>
#include <gtkmm/label.h>
#include "Tab.h"
class Textarea;

class SearchTab : public Tab
{

 public:
  SearchTab(MainWindow *master, Glib::RefPtr<Gtk::TextTagTable> tags, std::string word = "");
  virtual ~SearchTab();

  virtual void cb_clear();
  virtual void cb_spell();
  virtual void cb_search(const Glib::ustring search_str);
  virtual void cb_search_start(const Glib::ustring search_str);
  virtual void cb_search_middle(const Glib::ustring search_str);
  virtual void cb_search_end(const Glib::ustring search_str);
  virtual void selected_result(Glib::ustring str, unsigned int score, Glib::ustring info);
  virtual void new_tab(std::string word, std::string info = "");
 protected:

 private:
  // Returns true if the previous search string and type was the same:
  bool set_search_type(std::string string, std::string type);
  unsigned int get_search_value(Glib::ustring val);
  unsigned int get_search_from_value();
  unsigned int get_search_to_value();
  void set_search_from_value(unsigned int v);
  void set_search_to_value(unsigned int v);
  Textarea *textarea;
  Gtk::Box *search_more_box;
  std::string search_last_type;
  Glib::ustring search_last_string;
  Gtk::Label search_word_lbl, search_to_lbl, search_no_hits_label;
  Gtk::Entry search_from_entry, search_to_entry;
  unsigned int search_last_from_value, search_last_to_value;
  Glib::RefPtr<Gtk::TextTagTable> the_tag_table;
};

#endif
