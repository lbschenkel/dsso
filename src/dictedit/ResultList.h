#ifndef __RESULTLIST_H_
#define __RESULTLIST_H_

#include <gtkmm/liststore.h>
#include <gtkmm/treeview.h>

class Tab;

class ResultList : public Gtk::TreeView {
 public:
  ResultList(Tab *tab, bool multiple_spelection = false);
  void clear() {
    _lockResult = true;
    resultlist->clear();
    _lockResult = false;
  }
  void add_result(std::string str, unsigned int score, std::string info = "") {
    Gtk::TreeRow row = *(resultlist->append());
    row[m_columns.result] = str;
    row[m_columns.score] = score;
    row[m_columns.info] = info;
  }
  void add_result_top(std::string str, unsigned int score, std::string info = "") {
    Gtk::TreeRow row = *(resultlist->prepend());
    row[m_columns.result] = str;
    row[m_columns.score] = score;
    row[m_columns.info] = info;
  }
  void cb_result_selected();
  bool on_button_press_event(GdkEventButton* event);
  void cb_mark_as(unsigned char flag);
 protected:
  struct ModelColumns : public Gtk::TreeModelColumnRecord
  {
    Gtk::TreeModelColumn<unsigned int> score;
    Gtk::TreeModelColumn<Glib::ustring> result;
    Gtk::TreeModelColumn<Glib::ustring> info;
    ModelColumns() { add(score); add(result); add(info); }
  };

  const ModelColumns m_columns;

  Glib::RefPtr<Gtk::ListStore> resultlist;
  Glib::RefPtr<Gtk::TreeSelection> refTreeSelection;

  bool cb_main_menu(GdkEventButton* event);
  Gtk::Menu result_context_menu;

 private:
  void new_search_tab();
  void new_edit_tab();
  Tab *parent;
  bool _lockResult;
};

#endif
