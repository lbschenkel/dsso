#include "dict.h"

StringDB *Dict::string_db;
DictDB *Dict::dict_db;
Corpus *Dict::corpus;
HunInfoDB *Dict::huninfodb;
InfoDB<StringInfo, EmptyStringInfo> *Dict::infodb;
StringSort<LexiCmp> *Dict::alpha;
StringSort<RLexiCmp> *Dict::beta;
StringSort<FreqCmp> *Dict::freq;
Tokenizer *Dict::tokenizer;

void initdict(std::string filename) {
  Dict::string_db = &StringDB::get_db();
  Dict::dict_db = new DictDB(filename, "index/" + filename + ".bin",
			     "index/" + filename + ".journal",
			     "index/" + filename + "_wfindex.txt", get_json_id_val);
  Dict::corpus = new Corpus();
  Dict::huninfodb = new HunInfoDB();
  Dict::tokenizer = new Tokenizer(Dict::huninfodb);
  Dict::infodb = InfoDB<StringInfo, EmptyStringInfo>::get_dbp("index/stringinfo.bin");
  Dict::alpha = StringSort<LexiCmp>::get_db("index/lexisort.txt");
  Dict::beta = StringSort<RLexiCmp>::get_db("index/rlexisort.txt");
  Dict::freq = StringSort<FreqCmp>::get_db("index/freqsort.txt");
}

void Dict::get_suggestions(const char *word, std::vector<std::string> &v) {
  huninfodb->suggest(word, v);
}

void Dict::close_dicts() {
  if (huninfodb)
    huninfodb->refresh();
  if (dict_db)
    dict_db->refresh();
  if (dict_db)
    delete dict_db;
  if (corpus)
    delete corpus;
  if (huninfodb)
    delete huninfodb;
}

unsigned int Dict::word_count(const char *word) {
  unsigned int sno = string_db->find(word);
  //std::cerr << "word <" << word << "> sno=" << sno << std::endl;
  return Dict::word_count(sno);
}

unsigned int Dict::word_count(unsigned int sno) {
  if (!sno)
    return 0;
  return infodb->word_count(sno);
}

unsigned long Dict::tot_score(SyU *syu) {
  unsigned long tot = 0;
  std::set<std::string> counted;
  const std::vector<WF> &wfs = syu->wfs();
  for (unsigned int i=0; i<wfs.size(); ++i) {
    std::string w = wfs[i].word();
    if (counted.count(w))
      continue;
    counted.insert(w);
    tot += word_count(w.c_str());
  }
  return tot;
}

std::string Dict::spellcheck(const char *word) {
  int res = Dict::huninfodb->check_word(word);
  if (res == 2)
    return "sms";
  if (res)
    return "ok";
  return "nok";
}

std::string Dict::spellcheck(unsigned int sno) {
  unsigned char spell = Dict::huninfodb->spell_status(sno);
  if (spell & SPELL_DEVEL_OK) {
    if (spell & SPELL_IS_COMPOUND)
      return "sms";
    else
      return "ok";
  }
  return "nok";
}

const char *Dict::spell_category(unsigned int sno) {
  return Dict::huninfodb->get_spell_cat_name(sno);
}

unsigned int num_value(const char *s) {
  unsigned int n=0;
  while (*s >= '0' and *s <= '9') {
    n *= 10;
    n += (*s - '0');
    ++s;
  }
  return n;
}

char *str_value(unsigned int v) {
  static char str[40];
  static char zero[2] = "0";
  if (!v)
    return zero;
  char *p = str + sizeof(str)/sizeof(char);
  *--p = 0;
  while (v) {
    *--p = (v%10 + '0');
    v /= 10;
  }
  return p;
}
