#ifndef __EDITTAB_H_
#define __EDITTAB_H_

#include <gtkmm/eventbox.h>
#include "Tab.h"
#include "dict.h"

class EditWidget;
class SyU;

class EditTab : public Tab
{

 public:
  EditTab(MainWindow *master, std::string word = "" /*, SyU *a_syu = 0 */);
  virtual ~EditTab();

  virtual void cb_clear();
  virtual void cb_spell();
  virtual void cb_search(const Glib::ustring search_str);
  virtual void cb_search_start(const Glib::ustring search_str);
  virtual void selected_result(Glib::ustring str, unsigned int score, Glib::ustring info);

  void cb_new_syu(unsigned int gc);
  void cb_paradigm(unsigned int syuid);
  bool cb_other_gc_box(GdkEventButton *event);
  bool cb_paradigm_box(GdkEventButton *event);
  virtual void new_tab(std::string word, std::string info = "");
 protected:
  EditWidget *editwidget;
  Gtk::Box *add_new_box;
  Gtk::Label add_new_label, other_gc_label, paradigm_label;
  const std::vector<std::string> &grammar_class;
  Gtk::Button new_gc0, new_gc1, new_gc2, new_gc3;
  Gtk::Menu gcMenu;
  Gtk::EventBox otherGCbox;
  Gtk::Menu paradigmMenu;
  Gtk::EventBox paradigm_box;
 private:
  std::string curr_word;
  bool info_string(Glib::ustring info);
};

#endif
