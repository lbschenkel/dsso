#ifndef __TEXTAREA_H_
#define __TEXTAREA_H_

#include <gtkmm/window.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/textview.h>

class Tab;

class Textarea : public Gtk::ScrolledWindow
{

public:
  Textarea(Glib::RefPtr<Gtk::TextTagTable> tags, Tab *parent);
  virtual ~Textarea();
  virtual void cb_clear();
  virtual void cb_spell();
  void cb_populate_popup(Gtk::Menu *menu);
  bool cb_button_press(GdkEventButton *ev);
  void cb_context_menu(std::string what);
  virtual unsigned int cb_search(const Glib::ustring search_str, unsigned int max_hits = 100, unsigned int from_no = 0);
  void cb_textsearch(const Glib::ustring search_str);
  void do_spellcheck_buffer();
protected:
  Glib::RefPtr<Gtk::TextBuffer> buffer;
  Gtk::TextView textview;
  Gtk::TextIter last_click_pos;
  Tab *my_tab;
};

#endif
