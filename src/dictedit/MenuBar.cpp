#include <iostream>

#include "MenuBar.h"
#include "MainWindow.h"
#include "dict.h"

MenuBar::MenuBar(MainWindow *parent)
  : Gtk::Box(Gtk::ORIENTATION_HORIZONTAL),
    spell_cat_lbl("CAT"),
    search_button("SEARCH"),
    search_entry(),
    search_start_button("^"),
    search_middle_button("*|*"),
    search_end_button("$"),
    search_re_button("RE"),
    search_clear_button("CLR"),
    quit_button("QUIT"),
    spell_button("SPELL"),
    clear_button("CLR"),
    _separator("     "),
    _parent(parent)
{

  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("New EditTab"));
    gcItem->signal_activate().
      connect( sigc::bind<std::string, std::string>(sigc::mem_fun(*_parent, &MainWindow::new_edit_tab), std::string(), std::string() ));
    main_menu.add(*gcItem);
  }
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("New SearchTab"));
    gcItem->signal_activate().
      connect( sigc::bind<std::string>(sigc::mem_fun(*_parent, &MainWindow::new_search_tab), std::string() ));
    main_menu.add(*gcItem);
  }
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("New SpellTab"));
    gcItem->signal_activate().
      connect( sigc::bind<std::string>(sigc::mem_fun(*_parent, &MainWindow::new_spell_tab), std::string() ));
    main_menu.add(*gcItem);
  }
  /*
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Clear"));
    gcItem->signal_activate().
      connect( sigc::mem_fun(*_parent, &MainWindow::cb_clear) );
    main_menu.add(*gcItem);
  }
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Export Dictionary"));
    gcItem->signal_activate().
      connect( sigc::mem_fun(*_parent, &MainWindow::cb_dump_old_format) );
    main_menu.add(*gcItem);
  }
  */
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Build Hunspell sv_SE"));
    gcItem->signal_activate().
      connect( sigc::mem_fun(*_parent, &MainWindow::cb_build_hunspell) );
    main_menu.add(*gcItem);
  }
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Build Hunspell sv_FI"));
    gcItem->signal_activate().
      connect( sigc::mem_fun(*_parent, &MainWindow::cb_build_fi_hunspell) );
    main_menu.add(*gcItem);
  }
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Dump Spell Categories"));
    gcItem->signal_activate().
      connect( sigc::mem_fun(*_parent, &MainWindow::cb_dump_spell_categories) );
    main_menu.add(*gcItem);
  }
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Close Current Tab"));
    gcItem->signal_activate().
      connect( sigc::mem_fun(*_parent, &MainWindow::cb_close_tab) );
    main_menu.add(*gcItem);
  }
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Reload Devel Dictionary"));
    gcItem->signal_activate().
      connect( sigc::mem_fun(*_parent, &MainWindow::cb_reload_devel_dictionary) );
    main_menu.add(*gcItem);
  }
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Reload Base Dictionary"));
    gcItem->signal_activate().
      connect( sigc::mem_fun(*_parent, &MainWindow::cb_reload_base_dictionary) );
    main_menu.add(*gcItem);
  }
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Reload Master Dictionary"));
    gcItem->signal_activate().
      connect( sigc::mem_fun(*_parent, &MainWindow::cb_reload_master_dictionary) );
    main_menu.add(*gcItem);
  }
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Quit"));
    gcItem->signal_activate().
      connect( sigc::mem_fun(*_parent, &MainWindow::cb_quit) );
    main_menu.add(*gcItem);
  }
  main_menu.show_all();

  main_menu_icon.set_from_icon_name("fonts", Gtk::IconSize(5));
  main_menu_icon.show();
  main_menu_button.add(main_menu_icon);
  pack_start(main_menu_button, Gtk::PACK_SHRINK);
  main_menu_button.signal_button_press_event().
    connect( sigc::mem_fun(*this, &MenuBar::cb_main_menu) );

  main_menu_button.show();

  {
    for (unsigned int i=0; i<16; ++i) {
      std::string menustr = Dict::huninfodb->spell_category_name[i];
      unsigned char cat = Dict::huninfodb->spell_category_number[i];
      Gtk::MenuItem *item = Gtk::manage(new Gtk::MenuItem(menustr));
      item->signal_activate().
	connect(sigc::bind<unsigned char>
		(sigc::mem_fun(*parent, &MainWindow::cb_set_spell_cat), cat));
      spell_cat_menu.add(*item);
    }
    spell_cat_menu.show_all();

    spell_cat_lbl.show();
    spell_cat_button.add(spell_cat_lbl);
    pack_start(spell_cat_button, Gtk::PACK_SHRINK);
    spell_cat_button.signal_button_press_event().
      connect( sigc::mem_fun(*this, &MenuBar::cb_spell_cat_menu) );

    spell_cat_button.show();
  }

  pack_start(search_clear_button, Gtk::PACK_SHRINK);
  search_clear_button.show();

  pack_start(search_button, Gtk::PACK_SHRINK);
  search_button.show();

  //search_entry.set_max_length(30);
  pack_start(search_entry, Gtk::PACK_SHRINK);
  search_entry.show();

  pack_start(search_start_button, Gtk::PACK_SHRINK);
  search_start_button.show();

  pack_start(search_middle_button, Gtk::PACK_SHRINK);
  search_middle_button.show();

  pack_start(search_end_button, Gtk::PACK_SHRINK);
  search_end_button.show();

  pack_start(search_re_button, Gtk::PACK_SHRINK);
  search_re_button.show();

  pack_start(clear_button, Gtk::PACK_SHRINK);
  clear_button.show();

  pack_start(_separator, Gtk::PACK_SHRINK);
  _separator.show();

  pack_end(quit_button, Gtk::PACK_SHRINK);
  //quit_button.show();

  pack_end(spell_button, Gtk::PACK_SHRINK);
  spell_button.show();

  quit_button.signal_clicked().
    connect( sigc::mem_fun(*_parent, &MainWindow::cb_quit) );
  clear_button.signal_clicked().
    connect( sigc::mem_fun(*_parent, &MainWindow::cb_clear) );
  spell_button.signal_clicked().
    connect( sigc::mem_fun(*_parent, &MainWindow::cb_spell) );

  search_button.signal_clicked().
    connect( sigc::mem_fun(*this, &MenuBar::cb_search) );
  search_entry.signal_activate().
    connect( sigc::mem_fun(*this, &MenuBar::cb_search) );
  search_start_button.signal_clicked().
    connect( sigc::mem_fun(*this, &MenuBar::cb_search_start) );
  search_middle_button.signal_clicked().
    connect( sigc::mem_fun(*this, &MenuBar::cb_search_middle) );
  search_end_button.signal_clicked().
    connect( sigc::mem_fun(*this, &MenuBar::cb_search_end) );
  search_re_button.signal_clicked().
    connect( sigc::mem_fun(*this, &MenuBar::cb_search_re) );
  search_clear_button.signal_clicked().
    connect( sigc::mem_fun(*this, &MenuBar::cb_search_clear) );
}

void MenuBar::add_box(Gtk::Box *box) {
  pack_start(*box, Gtk::PACK_SHRINK);  
}

bool MenuBar::cb_main_menu(GdkEventButton *event) {
  //std::cerr << "cb_main_menu()" << std::endl;
  if (!main_menu.get_attach_widget()) {
    main_menu.attach_to_widget(*this);
  }
  main_menu.popup(event->button, event->time);
  return true; //It has been handled.
}

bool MenuBar::cb_spell_cat_menu(GdkEventButton *event) {
  //std::cerr << "cb_spell_cat_menu()" << std::endl;
  if (!spell_cat_menu.get_attach_widget()) {
    spell_cat_menu.attach_to_widget(*this);
  }
  spell_cat_menu.popup(event->button, event->time);
  return true; //It has been handled.
}

void MenuBar::cb_search() {
  _parent->cb_search(search_entry.get_text());
}

void MenuBar::cb_search_start() {
  _parent->cb_search_start(search_entry.get_text());
}

void MenuBar::cb_search_middle() {
  _parent->cb_search_middle(search_entry.get_text());
}

void MenuBar::cb_search_end() {
  _parent->cb_search_end(search_entry.get_text());
}

void MenuBar::cb_search_re() {
  _parent->cb_search_re(search_entry.get_text());
}

void MenuBar::cb_search_clear() {
  _parent->cb_search_clear();
}

MenuBar::~MenuBar()
{
}
