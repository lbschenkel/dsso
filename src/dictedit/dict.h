#ifndef __DICT_H__
#define __DICT_H__

#include <SyuGetWordforms.h>
#include <StringDB.h>
#include <RawStringDB.h>
#include <RawStringIndex.h>
#include <DictDB.h>
#include <HunInfoDB.h>
#include <StringInfo.h>
#include <StringSort.h>
#include <InfoDB.h>
#include <Corpus.h>
#include <FreqCmp.h>
#include <Tokenizer.h>

namespace Dict {
  extern StringDB *string_db;
  extern DictDB *dict_db;
  extern Corpus *corpus;
  extern HunInfoDB *huninfodb;
  extern Tokenizer *tokenizer;
  extern InfoDB<StringInfo, EmptyStringInfo> *infodb;
  extern StringSort<LexiCmp> *alpha;
  extern StringSort<RLexiCmp> *beta;
  extern StringSort<FreqCmp> *freq;
  unsigned int word_count(const char *word);
  unsigned int word_count(unsigned int sno);
  inline unsigned int string_number(std::string word) {
    return string_db->find(word);
  }
  std::string spellcheck(unsigned int sno);
  std::string spellcheck(const char *word);
  const char *spell_category(unsigned int sno);
  unsigned long tot_score(SyU *syu);
  void close_dicts();
  void get_suggestions(const char *word, std::vector<std::string> &v);
}

unsigned int num_value(const char *s);
char *str_value(unsigned int v);

void initdict(std::string filename = "dsso_db.txt");

#endif
