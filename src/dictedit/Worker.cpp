#include "Worker.h"
#include "MainWindow.h"
#include "dict.h"
#include <sstream>

Worker::Worker() :
  mutex(),
  shall_stop(false),
  has_stopped(false),
  fraction_done(0.0),
  message()
{
}

void Worker::get_info(double *done, Glib::ustring *msg) const
{
  Glib::Threads::Mutex::Lock lock(mutex);

  if (done)
    *done = fraction_done;

  if (msg)
    *msg = message;
}

void Worker::stop_thread()
{
  Glib::Threads::Mutex::Lock lock(mutex);
  shall_stop = true;
}

bool Worker::stopped() const
{
  Glib::Threads::Mutex::Lock lock(mutex);
  return has_stopped;
}

void Worker::build_hunspell(MainWindow *caller, const char *target,
			    const char *tags)
{
  {
    Glib::Threads::Mutex::Lock lock(mutex);
    has_stopped = false;
    message = "Writing wordlists....";
  }
  caller->notify();
  Dict::dict_db->build_hunspell_wordlists("dssobuild", tags);
  {
    Glib::Threads::Mutex::Lock lock(mutex);
    message = "Building Hunspell dictionary....";
  }
  caller->notify();
  Dict::dict_db->build_hunspell_make("dssobuild", target);
  {
    Glib::Threads::Mutex::Lock lock(mutex);
    message = "Built ";
    message += target;
    has_stopped = true;
  }
  caller->notify();
}
