#include <iostream>
#include <vector>

#include "ResultList.h"
#include "Tab.h"
#include "dict.h"

ResultList::ResultList(Tab *tab, bool multiple_spelection)
  : parent(tab),
    _lockResult(false)
{
  resultlist = Gtk::ListStore::create(m_columns);

  set_model(resultlist);
  set_rules_hint();
  //set_search_column(m_columns.result.index());

  append_column("Score", m_columns.score);
  append_column("Result", m_columns.result);
  append_column("Info", m_columns.info);

  refTreeSelection = get_selection();
  refTreeSelection->signal_changed().
    connect(sigc::mem_fun(*this, &ResultList::cb_result_selected));
  if (multiple_spelection)
    refTreeSelection->set_mode(Gtk::SELECTION_MULTIPLE);
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Search ..."));
    gcItem->signal_activate().
      connect( sigc::mem_fun(*this, &ResultList::new_search_tab) );
    result_context_menu.add(*gcItem);
  }
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Edit ..."));
    gcItem->signal_activate().
      connect( sigc::mem_fun(*this, &ResultList::new_edit_tab) );
    result_context_menu.add(*gcItem);
  }
  //   "OK", "NOK", "IGNORE", "UNDECIDED"
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Mark as OK"));
    gcItem->signal_activate().
      connect( sigc::bind<unsigned char>
	       (sigc::mem_fun(*this, &ResultList::cb_mark_as), SPELL_CAT_OK) );
    result_context_menu.add(*gcItem);
  }
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Mark as NOK"));
    gcItem->signal_activate().
      connect( sigc::bind<unsigned char>
	       (sigc::mem_fun(*this, &ResultList::cb_mark_as), SPELL_CAT_NOK) );
    result_context_menu.add(*gcItem);
  }
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Mark as IGNORE"));
    gcItem->signal_activate().
      connect( sigc::bind<unsigned char>
	       (sigc::mem_fun(*this, &ResultList::cb_mark_as), SPELL_CAT_IGNORE) );
    result_context_menu.add(*gcItem);
  }
  {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem("Mark as UNDECIDED"));
    gcItem->signal_activate().
      connect( sigc::bind<unsigned char>
	       (sigc::mem_fun(*this, &ResultList::cb_mark_as), SPELL_CAT_UNDECIDED) );
    result_context_menu.add(*gcItem);
  }
  result_context_menu.show_all();
}



void ResultList::cb_result_selected() {
  if (_lockResult) {
    return;
  }
  std::vector<Gtk::TreeModel::Path> pathlist =
    refTreeSelection->get_selected_rows();

  for (std::vector<Gtk::TreeModel::Path>::iterator iter = pathlist.begin();
       iter != pathlist.end(); ++iter) {
    Gtk::TreeModel::Row row = *(resultlist->get_iter(*iter));
    //std::cerr << "Sel " << row[m_columns.result] << "\n";
    parent->selected_result(row[m_columns.result],
			    row[m_columns.score],
			    row[m_columns.info]);
  }
}
  
bool ResultList::on_button_press_event(GdkEventButton* event)
{
  
  if ((event->type == GDK_BUTTON_PRESS) && (event->button == 3)) {
    bool old = _lockResult;
    _lockResult = true;
    bool res = TreeView::on_button_press_event(event);
    _lockResult = old;
    //std::cerr << "Hej hopp" << std::endl;
    result_context_menu.popup(event->button, event->time);
    return res;
  }

  return TreeView::on_button_press_event(event);
}

void ResultList::new_search_tab() {
  std::vector<Gtk::TreeModel::Path> pathlist =
    refTreeSelection->get_selected_rows();

  for (std::vector<Gtk::TreeModel::Path>::iterator iter = pathlist.begin();
       iter != pathlist.end(); ++iter) {
    Gtk::TreeModel::Row row = *(resultlist->get_iter(*iter));
    Glib::ustring word = row[m_columns.result];
    //std::cerr << "Search " << word << "\n";
    parent->search_tab(word);
    parent->goto_last_tab();
    break; // Or should we permit opening several tabs?
  }
}

void ResultList::cb_mark_as(unsigned char flag) {
  //std::cerr << "Mark as " << (int) flag << std::endl;

  std::vector<Gtk::TreeModel::Path> pathlist =
    refTreeSelection->get_selected_rows();

  for (std::vector<Gtk::TreeModel::Path>::iterator iter = pathlist.begin();
       iter != pathlist.end(); ++iter) {
    Gtk::TreeModel::Row row = *(resultlist->get_iter(*iter));
    Glib::ustring word = row[m_columns.result];
    unsigned int sno = Dict::string_db->find(word);
    if (sno) {
      //std::cerr << "Mark " << word << " as " << (unsigned int) flag << "\n";
      Dict::huninfodb->set_spell_cat(sno, flag);
      Glib::ustring info = row[m_columns.info];
      size_t pos = info.find(" / ");
      if (pos != std::string::npos) {
	row[m_columns.info] = info.substr(0, pos) + " / " + 
	  spell_category_name_sorted[flag>>4];
      }
    }
  }
}

void ResultList::new_edit_tab() {

  std::vector<Gtk::TreeModel::Path> pathlist =
    refTreeSelection->get_selected_rows();

  for (std::vector<Gtk::TreeModel::Path>::iterator iter = pathlist.begin();
       iter != pathlist.end(); ++iter) {
    Gtk::TreeModel::Row row = *(resultlist->get_iter(*iter));
    Glib::ustring word = row[m_columns.result];
    Glib::ustring info = row[m_columns.info];
    std::cerr << "Search " << word << "\n";
    parent->edit_tab(word, info);
    parent->goto_last_tab();
    break; // Or should we permit opening several tabs?
  }
}
