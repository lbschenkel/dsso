#ifndef __TAB_H_
#define __TAB_H_

#include <gtkmm/paned.h>
#include <gtkmm/label.h>
#include <gtkmm/button.h>
#include <gtkmm/window.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/box.h>

#include "ResultList.h"

class MainWindow;

class Tab : public Gtk::Paned
{

public:
  Tab(MainWindow *master, Gtk::Widget *widget, bool multiple_spelection = false);
  virtual ~Tab();

  virtual void cb_clear() { }
  virtual void cb_spell() { }
  virtual void cb_search(const Glib::ustring search_str) { }
  virtual void cb_search_start(const Glib::ustring search_str) { }
  virtual void cb_search_middle(const Glib::ustring search_str) { }
  virtual void cb_search_end(const Glib::ustring search_str) { }
  virtual void cb_search_re(const Glib::ustring search_str) { }
  virtual void cb_set_spell_cat(unsigned char cat) {
    result_window.cb_mark_as(cat);
  }
  virtual void cb_search_clear() {
    clear_result();
  }
  virtual void selected_result(Glib::ustring str, unsigned int score, Glib::ustring info) = 0;
  void add_result(std::string str, unsigned int score = 0, std::string info = "");
  void add_result_top(std::string str, unsigned int score = 0, std::string info = "");
  void clear_result();
  Gtk::Box *theMenuBarBox() {
    return menuBarBox;
  }
  virtual void edit_tab(std::string word, std::string info = "");
  virtual void search_tab(std::string word, std::string info = "");
  virtual void add_tab(std::string title, Tab *tab, std::string word, std::string info = "");
  virtual void new_tab(std::string word, std::string info = "") = 0;
  void goto_last_tab();
protected:

  Gtk::Widget *main_tab_widget;

  Gtk::ScrolledWindow m_ScrolledWindow;
  ResultList result_window;
  unsigned int tab_id() {
    return _tab_no;
  }
  Gtk::Box *menuBarBox;
  MainWindow *mw() {
    return main_window;
  }
  
  void set_status_text(std::string msg);
  void status_show_suggestions(const char *word);
 private:
  static unsigned int _tab_serial_no;
  unsigned int _tab_no;
  MainWindow *main_window;
};

#endif
