#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <iostream>

#include "MainWindow.h"
#include <gtkmm/application.h>
#include <gtkmm/label.h>
#include "dict.h"

int main (int argc, char *argv[])
{
  std::string filename = "dsso_db.txt";
  std::string dirname = "";

  if (argc > 1) {
    struct stat st;
    if (stat(argv[1], &st) < 0) {
      std::cerr << "Cannot open " << argv[1] << ": " << strerror(errno) << std::endl;
      exit(1);
    }
    std::string path = argv[1];
    if ((st.st_mode & S_IFMT) == S_IFREG) {
      std::string::size_type pos = path.find_last_of('/');
      if (pos != std::string::npos) {
	dirname = path.substr(0, pos);
	filename = path.substr(pos+1);
      } else {
	filename = path;
      }
    } else if ((st.st_mode & S_IFMT) == S_IFDIR) {
      dirname = path;
    } else {
      std::cerr << "Cannot open " << argv[1] << ": not a regular file" << std::endl;
      exit(1);
    }
    if (dirname != "" and dirname != ".") {
      if (chdir(dirname.c_str()) == 0) {
	--argc;
	++argv;
      } else {
	std::cerr << "Cannot move to " << dirname << ": " << strerror(errno) << std::endl;
	exit(1);
      }
    }
  }
  //std::cerr << "Dirname is " << dirname << std::endl;
  //std::cerr << "Filename is " << filename << std::endl;
  setlocale(LC_ALL, "sv_SE.UTF-8");

  Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "se.dsso.dictedit");

  initdict(filename);

  MainWindow dictedit;
  dictedit.set_default_size(1400, 800);
  dictedit.new_edit_tab();
  dictedit.new_search_tab();
  dictedit.new_spell_tab();

  //Shows the window and returns when it is closed.
  return app->run(dictedit);
  Dict::close_dicts();
  std::cerr << "THE END\n";
}
