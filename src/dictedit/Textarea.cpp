#include <iostream>
#include "dict.h"
#include "Textarea.h"
#include "Tab.h"
#include "gtkmm/filechooserdialog.h"

Textarea::Textarea(Glib::RefPtr<Gtk::TextTagTable> tags, Tab *parent)
  : buffer(Gtk::TextBuffer::create(tags)),
    textview(buffer),
    my_tab(parent)
{
  add(textview);
  textview.set_wrap_mode(Gtk::WRAP_WORD);
  textview.signal_button_press_event().
    connect(sigc::mem_fun(*this, &Textarea::cb_button_press), false);
  textview.signal_populate_popup().
    connect(sigc::mem_fun(*this, &Textarea::cb_populate_popup));
  textview.show();
  show();
}

Textarea::~Textarea() {
}

void Textarea::cb_clear() {
  buffer->set_text("");
}

static const char *ctxmenu[] = { "Search", "Edit", "Open File" };

void Textarea::cb_populate_popup(Gtk::Menu *menu) {

  for (unsigned int i=0; i<sizeof(ctxmenu)/sizeof(const char *); ++i) {
    Gtk::MenuItem *menuItem = Gtk::manage(new Gtk::MenuItem(ctxmenu[i]));
    menu->add(*menuItem);
    menuItem->signal_activate().
      connect(sigc::bind<std::string>(sigc::mem_fun(*this, &Textarea::cb_context_menu), ctxmenu[i]));
    menuItem->show();
  }

}

bool Textarea::cb_button_press(GdkEventButton *ev) {
  if (ev->button != 3)
    return false;

  int x, y;
  textview.window_to_buffer_coords(Gtk::TEXT_WINDOW_TEXT, (int)ev->x, (int)ev->y, x, y);
  //std::cerr << "Loc: " << x << ", " << y << std::endl;
  textview.get_iter_at_location(last_click_pos, x, y);
  //std::cerr << "Ch=" << char(last_click_pos.get_char()) << std::endl;
  return false;
}

void Textarea::cb_context_menu(std::string what) {
  //std::cerr << "Ctx menu: " << what << std::endl;
  if (what == "Open File") {
    Gtk::FileChooserDialog dialog("Open File",
				  Gtk::FILE_CHOOSER_ACTION_OPEN);
    dialog.set_transient_for( dynamic_cast<Gtk::Window &>(*this->get_toplevel()) );
    dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
    dialog.add_button("_Open", Gtk::RESPONSE_OK);
    int result = dialog.run();
    if (result != Gtk::RESPONSE_OK)
      return;
    //std::cerr << "File: " << dialog.get_filename() << std::endl;
    std::ifstream fd(dialog.get_filename());
    if (!fd) {
      std::cerr << "cannot open file " << dialog.get_filename() << std::endl;
      return;
    }
    buffer->set_text("");
    Gtk::TextBuffer::iterator iter = buffer->end();
    std::string line;
    while ( getline(fd, line) ) {
      iter = buffer->insert(iter, line);
      iter = buffer->insert(iter, "\n");
    }
    return;
  }
  gunichar ch = last_click_pos.get_char();
  if (!g_unichar_isalpha(ch) and ch != '-')
    return;

  Gtk::TextIter wbeg = last_click_pos;
  while (wbeg.backward_char()) {
    gunichar c = wbeg.get_char();
    if (!g_unichar_isalpha(c) and c != '-') {
      wbeg.forward_char();
      break;
    }
  }

  Gtk::TextIter wend = last_click_pos;
  while (wend.forward_char()) {
    gunichar c = wend.get_char();
    if (!g_unichar_isalpha(c) and c != '-') {
      break;
    }
  }
  Glib::ustring word = buffer->get_text(wbeg, wend, false);
  //std::cerr << "Word: <" << word << ">\n";
  if (what == "Search")
    my_tab->search_tab(word);
  else if (what == "Edit")
    my_tab->edit_tab(word);
  my_tab->goto_last_tab();
}

void Textarea::do_spellcheck_buffer() {
  Gtk::TextIter curr_pos,
    next_pos = buffer->get_iter_at_offset(0);

  // List misspelled words:
  std::vector<unsigned int> misspelled_string_numbers;
  std::set<std::string> misspelled_unknown;

  while (true) {
    curr_pos=next_pos;
    next_pos.forward_line();
    if (next_pos == curr_pos)
      break;

    Glib::ustring the_line = curr_pos.get_slice(next_pos);
    //std::cerr << "LINE: " << the_line << std::endl;
    std::deque<int> word_index;
    unsigned int possible_sentence_end = 0;
    int line_start_char_pos = curr_pos.get_offset();
    Dict::tokenizer->tokenize_line(the_line, word_index, possible_sentence_end);
    for (unsigned int i=0; i<word_index.size(); i+=2) {
      if (word_index[i+1] < 2)
	continue;
      Glib::ustring the_word = the_line.substr(word_index[i], word_index[i+1]);
      if (the_word[the_word.size()-1] == ':')
	the_word.resize(the_word.size()-1);
      unsigned int sno = Dict::string_db->find(the_word);
      if (!sno and the_word[the_word.size()-1] == '.') {
	the_word.resize(the_word.size()-1);
	sno = Dict::string_db->find(the_word);
      }
      if (the_word.size() < 2)
	continue;
      Gtk::TextIter start_iter =
	buffer->get_iter_at_offset(line_start_char_pos+word_index[i]);
      Gtk::TextIter end_iter =
	buffer->get_iter_at_offset(line_start_char_pos+word_index[i]+the_word.size());
      if (sno) {
	unsigned char spell = Dict::huninfodb->spell_status(sno);
	//std::cerr << " {" << the_word << "}" << (int) spell;
	if (spell & SPELL_DEVEL_OK) {
	  if (spell & SPELL_IS_COMPOUND) {
	    buffer->apply_tag_by_name("spell_yellow", start_iter, end_iter);
	  }
	} else {
	  if (g_unichar_isupper(the_word[0])) {
	    buffer->apply_tag_by_name("spell_green", start_iter, end_iter);
	  } else {
	    buffer->apply_tag_by_name("spell_red", start_iter, end_iter);
	  }
	  misspelled_string_numbers.push_back(sno);
	}
      } else {
	//std::cerr << " [" << the_word << "]";
	buffer->apply_tag_by_name("unknown_word", start_iter, end_iter);
	int sp_res = Dict::huninfodb->check_word(the_word.c_str());
	if (sp_res == 0) {
	  if (g_unichar_isupper(the_word[0])) {
	    buffer->apply_tag_by_name("spell_green", start_iter, end_iter);
	  } else {
	    buffer->apply_tag_by_name("spell_red", start_iter, end_iter);
	    misspelled_unknown.insert(the_word);
	  }
	} else if (sp_res == 2) {
	  buffer->apply_tag_by_name("spell_yellow", start_iter, end_iter);
	}
      }
    }
    word_index.clear();
  }
  //gtk_list_store_clear (dsso_listview_store);
  //GtkTreeIter iter;
  Dict::huninfodb->freqsort(misspelled_string_numbers);
  unsigned int prev=0;
  for (int i=misspelled_string_numbers.size()-1; i>=0; --i) {
    if (misspelled_string_numbers[i] == prev)
      continue;
    prev = misspelled_string_numbers[i];
    my_tab->add_result((*Dict::string_db)[prev], Dict::infodb->get_info(prev).word_count);
  }
  for (std::set<std::string>::iterator i=misspelled_unknown.begin();
       i != misspelled_unknown.end(); ++i) {
    //std::cerr << "Insert <" << *i << ">" << std::endl;
    my_tab->add_result(i->c_str(), 0);
  }
}

void Textarea::cb_spell() {
  //std::cerr << "Text spell!!" << std::endl;
  do_spellcheck_buffer();
}

unsigned int Textarea::cb_search(const Glib::ustring search_str, unsigned int max_hits, unsigned int from_no) {
  //std::cerr << "Text search for " << search_str << std::endl;
  //Gtk::TextBuffer::iterator iter = buffer->get_iter_at_offset(0);
  Gtk::TextBuffer::iterator iter = buffer->end();
  CPointer cpos[max_hits];
  unsigned int no_hits = Dict::corpus->search(search_str, cpos, max_hits, from_no);

  for (unsigned int i=0; i < no_hits; ++i) {
    if (!i or cpos[i] != cpos[i-1]) {
      iter = buffer->insert(iter, Dict::corpus->line_at(cpos[i]));
      iter = buffer->insert(iter, "\n");
      //std::cerr << Dict::corpus->line_at(cpos[i]) << std::endl;
    }
  }

  //int n = buffer->get_line_count();
  //std::cerr << "Text scroll to line " << n << std::endl;
  //Gtk::TextBuffer::iterator start_pos = buffer->get_iter_at_line_offset(n, 0);
  //textview.scroll_to(start_pos, 0.0001);

  return no_hits;
}

void Textarea::cb_textsearch(const Glib::ustring search_str) {
  //std::cerr << "Searching for " << search_str << std::endl;
  Glib::RefPtr< Gtk::TextBuffer::Mark > cpos = buffer->get_insert();
  Gtk::TextIter curr_pos;
  Gtk::TextIter next_pos = buffer->get_iter_at_mark(cpos);

  next_pos.forward_line();
  bool wrapped = false;
  while (true) {
    curr_pos = next_pos;
    bool more_lines = next_pos.forward_line();
    Glib::ustring the_line = curr_pos.get_slice(next_pos);
    Glib::ustring::size_type start_pos = the_line.find(search_str, 0);
    while (start_pos != Glib::ustring::npos) {
      Glib::ustring::size_type end_pos = start_pos+search_str.length();
      /*
      std::cerr << "Line <" << the_line << ">"
		<< "At pos: " << start_pos << " ch=" << the_line[start_pos]
		<< " to ch=" << the_line[end_pos-1]
		<< std::endl;
      */
      bool ok = true;
      if (start_pos > 0) {
	gunichar c = the_line[start_pos-1];
	if (g_unichar_isalpha(c) or c == '-') {
	  ok = false;
	}
      }
      if (end_pos < the_line.length()) {
	gunichar c = the_line[end_pos];
	if (g_unichar_isalpha(c) or c == '-') {
	  ok = false;
	}
      }
      if (ok) {
	curr_pos.forward_chars(start_pos);
	buffer->place_cursor(curr_pos);
	textview.scroll_to(curr_pos, 0.0001);
	//std::cerr << "A hit." << std::endl;
	return;
      } else {
	//std::cerr << "No hit." << std::endl;
      }
      start_pos = the_line.find(search_str, start_pos+1);
    }
    if (!more_lines) {
      if (wrapped)
	return; // No hits
      next_pos = buffer->get_iter_at_offset(0);
      wrapped = true;
    }
  }
}
