#include <iostream>

#include "Tab.h"
#include "dict.h"
#include "MainWindow.h"

Tab::Tab(MainWindow *master, Gtk::Widget *widget, bool multiple_spelection)
  : Gtk::Paned(Gtk::ORIENTATION_HORIZONTAL),
    main_tab_widget(widget),
    result_window(this, multiple_spelection),
    menuBarBox(Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL))),
    _tab_no(++_tab_serial_no),
    main_window(master)
{
  m_ScrolledWindow.set_shadow_type(Gtk::SHADOW_ETCHED_IN);
  m_ScrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
  m_ScrolledWindow.add(result_window);

  add1(m_ScrolledWindow);
  add2(*widget);
  widget->show();
  show_all_children();
}

void Tab::clear_result() {
  result_window.clear();
}

void Tab::goto_last_tab() {
  main_window->goto_last_tab();
}

void Tab::set_status_text(std::string msg) {
  main_window->set_status_text(msg);
}

void Tab::add_result(std::string str, unsigned int score, std::string info) {
  result_window.add_result(str.c_str(), score, info);
}

void Tab::add_result_top(std::string str, unsigned int score, std::string info) {
  result_window.add_result_top(str.c_str(), score, info);
}

void Tab::add_tab(std::string title, Tab *tab, std::string word, std::string info) {
  main_window->get_new_tab(title.c_str(), tab, word);
}

void Tab::edit_tab(std::string word, std::string info) {
  main_window->new_edit_tab(word, info);
}

void Tab::search_tab(std::string word, std::string info) {
  main_window->new_search_tab(word);
}

void Tab::status_show_suggestions(const char *word) {
  std::vector<std::string> v;
  Dict::get_suggestions(word, v);
  std::ostringstream out;
  out << word << ": ";
  if (v.size()) {
    out << "Similar words: " << v[0] << " (" << Dict::word_count(v[0].c_str()) << ")";
    for (unsigned int i=1; i<v.size(); ++i)
      out <<  ", " << v[i] << " (" << Dict::word_count(v[i].c_str()) << ")";
  } else {
    out << "No suggestions.";
  }
  set_status_text(out.str());
}

Tab::~Tab()
{
}

unsigned int Tab::_tab_serial_no = 0;
