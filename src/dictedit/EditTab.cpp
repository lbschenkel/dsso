/*

TODO

* I CLI-programmen, skapa klass SyU !!
  get_obj returnerar en SyU, object_cache
  lagrar SyU.


* cb_search ska lägga in alla Syu-resultat i resultatlistan.
* När man klickar på ett resultat får man upp SyU-edit.
* SyUEdit:
  En grid, vänsterkolumnen ger namn på böjngklasser, mittkol.
  Entry med nuv. bformer, högerkol. button med nuv. tags;
  klickar man på tag-knappen får man klicka i vilka tags man
  vill sätta. Radiobutton för kända, Entry för
  att ange en ny. I grammar kan man ha en lista med wf-tags
  och en med syu-tags?? Vissa med parameter?

  - Loopa igenom gc:ns alla bklasser, fixa grid,
    map ic-->struct m. entry/tag
  - Loopa sedan igenom alla wf, mata in i resp. entry,
    om det finns fler så ha dem nedanför, "alternativa bf.?"
    
*/


#include <iostream>
#include <sstream>

#include "dict.h"

#include "EditTab.h"
#include "EditWidget.h"


EditTab::EditTab(MainWindow *master, std::string word /*, SyU *a_syu */)
  : Tab(master, Gtk::manage(new EditWidget(this))),
    editwidget(dynamic_cast<EditWidget *>(main_tab_widget)),
    add_new_box(Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL))),
    add_new_label("Add new: "),
    other_gc_label(" Other... "),
    paradigm_label(" Paradigm... "),
    grammar_class(Dict::dict_db->gcs()),
    new_gc0(grammar_class[0]),
    new_gc1(grammar_class[1]),
    new_gc2(grammar_class[2]),
    new_gc3(grammar_class[3])
{
  set_position(300);
  add_new_box->pack_start(add_new_label, Gtk::PACK_SHRINK);
  add_new_box->pack_start(new_gc0, Gtk::PACK_SHRINK);
  add_new_box->pack_start(new_gc1, Gtk::PACK_SHRINK);
  add_new_box->pack_start(new_gc2, Gtk::PACK_SHRINK);
  add_new_box->pack_start(new_gc3, Gtk::PACK_SHRINK);
  add_new_label.show();
  new_gc0.signal_clicked().
    connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditTab::cb_new_syu), 0));
  new_gc0.show();
  new_gc1.signal_clicked().
    connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditTab::cb_new_syu), 1));
  new_gc1.show();
  new_gc2.signal_clicked().
    connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditTab::cb_new_syu), 2));
  new_gc2.show();
  new_gc3.signal_clicked().
    connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditTab::cb_new_syu), 3));
  new_gc3.show();

  for (unsigned int i=0; i<grammar_class.size(); ++i) {
    Gtk::MenuItem *gcItem = Gtk::manage(new Gtk::MenuItem(grammar_class[i].c_str()));
    gcItem->signal_activate().
      connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditTab::cb_new_syu), i));
    gcMenu.add(*gcItem);
  }
  gcMenu.show_all();
  
  add_new_box->pack_start(otherGCbox, Gtk::PACK_SHRINK);
  other_gc_label.show();
  otherGCbox.add(other_gc_label);
  otherGCbox.signal_button_press_event().
    connect(sigc::mem_fun(*this, &EditTab::cb_other_gc_box));
  otherGCbox.show();
  
  add_new_box->pack_start(paradigm_box, Gtk::PACK_SHRINK);
  paradigm_label.show();
  paradigm_box.add(paradigm_label);
  paradigm_box.signal_button_press_event().
    connect(sigc::mem_fun(*this, &EditTab::cb_paradigm_box));
  paradigm_box.show();

  menuBarBox->pack_start(*add_new_box, Gtk::PACK_SHRINK);
  if (word.size()) {
    if (!info_string(word))
      cb_search(word);
  }
  
  /*
  if (a_syu) {
    editwidget->open_syu(a_syu, tab_id());
  }
  */
}

bool EditTab::cb_paradigm_box(GdkEventButton* event)
{
  if (curr_word.size() < 4)
    return false;
  if ((event->type == GDK_BUTTON_PRESS) && (event->button == 1 || event->button == 3)) {
    std::vector<Gtk::Widget *> items = paradigmMenu.get_children();
    for (unsigned int i=0; i<items.size(); ++i) {
      paradigmMenu.remove(*items[i]);
    }
    const std::vector<SyU *> &sufs = Dict::dict_db->possible_suffixes(curr_word);
    for (unsigned int i=0; i<sufs.size(); ++i) {
      Gtk::MenuItem *paradigmItem = Gtk::manage(new Gtk::MenuItem(sufs[i]->show_long()));
      paradigmItem->signal_activate().
	connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditTab::cb_paradigm), sufs[i]->id()));
      paradigmMenu.add(*paradigmItem);
    }
    paradigmMenu.show_all();

    if (!paradigmMenu.get_attach_widget()) {
      paradigmMenu.attach_to_widget(*this);
    }
    paradigmMenu.popup(event->button, event->time);
    return true;
  } else {
    return false;
  }
}

bool EditTab::cb_other_gc_box(GdkEventButton* event)
{
  if ((event->type == GDK_BUTTON_PRESS) && (event->button == 1 || event->button == 3)) {
    if (!gcMenu.get_attach_widget()) {
      gcMenu.attach_to_widget(*this);
    }
    gcMenu.popup(event->button, event->time);
    return true;
  } else {
    return false;
  }
}

void EditTab::cb_paradigm(unsigned int syuid) {
  SyU *suffix = Dict::dict_db->get_syu(syuid);
  unsigned int slen = suffix->headword().word().length();
  if (curr_word.size() < slen+2)
    return;
  std::string prefix = curr_word.substr(0, curr_word.size()-slen);
  //std::cerr << "Suffix syu " << syuid << " = " << prefix << "+" << suffix->headword().word() << std::endl;
  SyU *compound = suffix->get_compound(prefix);
  editwidget->open_syu(compound, tab_id());
}

void EditTab::cb_new_syu(unsigned int gc) {
  //std::cerr << "Add syu " << gc << std::endl;
  editwidget->new_syu(curr_word, gc, tab_id());
}

EditTab::~EditTab()
{
}

void EditTab::cb_clear() {
  //std::cerr << "Clear!!" << std::endl;
}

void EditTab::cb_spell() {
  //std::cerr << "Spell!!" << std::endl;
}

void EditTab::cb_search_start(const Glib::ustring search_str) {
  if (!search_str.size())
    return;
  std::string prefix = search_str;
  WCMAP sufs;
  Dict::huninfodb->prefix_analysis(prefix, sufs);
  std::map<unsigned int, unsigned long> syuscore;
  Dict::dict_db->syu_score(sufs, syuscore);
  std::multimap<unsigned long, unsigned int, std::greater<unsigned long> > scores =
    flip_map(syuscore);
  //for (std::map<unsigned int, unsigned long>::const_iterator it=syuscore.begin();
  //   it!=syuscore.end(); ++it) {
  for (std::multimap<unsigned long, unsigned int>::const_iterator it=scores.begin();
       it!=scores.end(); ++it) {
    SyU *syu = Dict::dict_db->get_syu(it->second);
    std::string compound = prefix + syu->headword().word();
    std::string info = prefix + " (prefix) + ";
    info += str_value(syu->id());
    add_result(compound, it->first, info);
  }
}

void EditTab::cb_search(const Glib::ustring search_str) {
  curr_word = search_str;
  set_status_text("Searching for " + curr_word + "...");
  unsigned int sno = Dict::string_number(curr_word.c_str());
  std::string spell = sno ?
    Dict::spellcheck(sno) :
    Dict::spellcheck(curr_word.c_str());
  if (spell == "nok")
    status_show_suggestions(curr_word.c_str());
  if (sno) {
    std::set<unsigned int> res =
      Dict::dict_db->search_wf(sno);
    if (!res.size()) {
      //std::cerr << "No hits." << std::endl;
      add_result_top(search_str, Dict::word_count(sno), spell);
    } else {
      unsigned int id;
      for (std::set<unsigned int>::const_reverse_iterator p = res.rbegin(); p!=res.rend(); ++p) {
	SyU *syu = Dict::dict_db->get_syu(*p);
	unsigned long score = Dict::tot_score(syu);
	//std::string gc = syu->gc();
	//std::cerr << syu->show_long() << std::endl;
	add_result_top(syu->show_long().c_str(), score, std::string("ID ") + str_value(*p));
	/*
	const std::vector<WF> &wflist = syu->wfs();
	for (unsigned int i=0; i < wflist.size(); ++i) {
	  std::string wf =  wflist[i].word();
	  std::cerr << "... " << wf;
	  std::string tags = wflist[i].tagstring();
	  if (tags.size())
	    std::cerr << ", tags = " << tags;
	  std::cerr << std::endl;
	}
	*/
	id = syu->id();
      }
      editwidget->open_syu(id, tab_id());
    }
  } else {
    //std::cerr << "Unknown word." << std::endl;
    add_result_top(search_str.c_str(), 0, spell);
  }
  std::string lbl = "Add " + curr_word + ": ";
  add_new_label.set_text(lbl);
  add_new_box->show();
}

bool EditTab::info_string(Glib::ustring info) {
  Glib::ustring::size_type pos = info.find(" (prefix)");
  if (pos == Glib::ustring::npos)
    pos = info.find(" (non-prefix)");
  if (pos == Glib::ustring::npos)
    pos = info.find(" (valid prefix)");
  Glib::ustring::size_type end = info.find("prefix) + ");
  if (pos == Glib::ustring::npos or end == Glib::ustring::npos)
    return false;
  end += strlen("prefix) + ");
  std::string prefix = info.substr(0, pos);
  std::string suff_id = info.substr(end);
  std::istringstream in(suff_id);
  unsigned int id;
  in >> id;
  //std::cerr << "Pref <" << prefix << "> Suff ID=" << suff_id << "=" << id << std::endl;
  SyU *syu;
  if (id) {
    syu =  editwidget->curr_open_syu();
    if (!syu or syu->id() != id)
      syu = Dict::dict_db->get_syu(id);
  } else {
    syu = editwidget->curr_analyse_syu();
    if (!syu)
      return false;
  }
  SyU *compound = syu->get_compound(prefix);
  compound->set_compound_suffix(id);
  status_show_suggestions(compound->headword().word().c_str());
  editwidget->open_syu(compound, tab_id());
  return true;
}

void EditTab::selected_result(Glib::ustring str, unsigned int score, Glib::ustring info) {
  //std::cerr << "Sel. Syu\n";
  if (info_string(info))
    return;
  if (info.size() > 3 and info.substr(0, 3) == "ID ") {
    unsigned int id = num_value(info.c_str()+strlen("ID "));
    //std::cerr << "SyU ID = " << id << std::endl;
    if (id)
      editwidget->open_syu(id, tab_id());
  }
}

void EditTab::new_tab(std::string word, std::string info) {
  add_tab(word, new EditTab(mw(), word), word);
}
