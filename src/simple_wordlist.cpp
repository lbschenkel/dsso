//#include <algorithm>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <errno.h>


#include "StringInfo.h"
#include "StringDB.h"

#include <iostream>

int main(int argc, char *argv[]) {
  StringDB sdb = StringDB::get_db();
  StringDB::iterator p = sdb.begin();
  while (p != sdb.end()) {
    std::cerr << p.string_number() << ' ' << *p << std::endl;
    ++p;
  }
}
