#include "defs.h"
#include "SyU.h"

Json::Reader DictObject::_reader;
Json::FastWriter DictObject::_writer;

SyU::SyU(const char *str)
  : _owner(0) {
  if (!_reader.parse(str, obj)) {
    // Throw...
  }
  wfs();
  if (obj.isMember("compound")) {
    c_prefix = obj["compound"][0].asUInt();
    c_suffix = obj["compound"][1].asUInt();
  } else {
    c_prefix = c_suffix = 0;
  }
  c_prefix_orig = c_prefix;
  c_suffix_orig = c_suffix;
  _next_wfno = _wfs.size();
}

const std::vector<WF> &SyU::wfs() const {
  if (_wfs.size() == 0) {
    Json::Value wflist = obj["wfs"];
    for (unsigned int i=0; i < wflist.size(); ++i) {
      std::string wf =  wflist[i]["w"].asString();
      std::string tags =  wflist[i]["tags"].asString();
      unsigned int icno = wflist[i]["ic"].asUInt();
      _wfs.push_back(WF(icno, wf, tags));
    }
  }
  return _wfs;
}

SyU *SyU::get_compound(std::string prefix) {

  //std::cerr << "Wfno=" << wfno << ", orig=" << _wfs.size() << std::endl;

  if (prefix.size()>1 and prefix[prefix.size()-1]==prefix[prefix.size()-2]) {
    char simplified_triple = prefix[prefix.size()-1];
    std::string s = headword().word();
    if (s.size() and s[0] == simplified_triple)
      prefix.resize(prefix.size()-1);
  }
  std::vector<WF> cwfs;
  for (unsigned int j=0; j<_wfs.size(); ++j) {
    std::map<unsigned int, WF>::const_iterator it = modified_wfs.find(j);
    if (it == modified_wfs.end()) {
      if (_wfs[j].word().size()) {
	WF cwf(_wfs[j].icno(), prefix+_wfs[j].word(), _wfs[j].tagstring());
	cwfs.push_back(cwf);
      }
    } else {
      if ((it->second).word().size()) {
	const WF &wf(it->second);
	WF cwf(wf.icno(), prefix + wf.word(), wf.tagstring());
	cwfs.push_back(cwf);
      }
    }
  }

  for (std::map<unsigned int, WF>::iterator it = modified_wfs.begin();
       it!=modified_wfs.end(); ++it) {
    if (it->first >= _wfs.size() and (it->second).word().size()) {
      const WF &wf(it->second);
      WF cwf(wf.icno(), prefix + wf.word(), wf.tagstring());
      cwfs.push_back(cwf);
    }
  }

  SyU *other1 = create(gc());
  Json::Value &cobj = other1->raw_obj();
  
  cobj["wfs"].resize(cwfs.size());
  for (unsigned int i = 0; i < cwfs.size(); ++i) {
    cobj["wfs"][i]["ic"] = cwfs[i].icno();
    cobj["wfs"][i]["w"] = cwfs[i].word();
    if ( cwfs[i].tagstring().size() > 0 )
      cobj["wfs"][i]["tags"] = cwfs[i].tagstring();
  }
  std::string cstr = _writer.write(cobj);
  //std::cerr << "COMPOUND=<" << cstr << ">" << std::endl;
  delete other1;
  SyU *other = new SyU(cstr.c_str());
  return other;

}


unsigned int SyU::find_wfno(unsigned int icno) {
  for (unsigned int j=0; j<_wfs.size(); ++j) {
    if (_wfs[j].icno() == icno)
      return j;
  }
  for (std::map<unsigned int, WF>::iterator it = modified_wfs.begin();
       it != modified_wfs.end(); ++it) {
    if ( (it->second).icno() == icno )
      return it->first;
  }
  return npos;
}

bool SyU::default_compound() {
  std::string sfl = get_attr("spell_flags");
  if (sfl.length() >= 3)
    return (sfl[2] == 'y');
  if (_wfs.size() and _wfs[0].word().find(" ") != std::string::npos)
    return false;
  unsigned int minlength;
  if (gc() == "substantiv")
    minlength = 4;
  else if (gc() == "verb" or gc() == "adjektiv")
    minlength = 5;
  else
    return false;
  for (unsigned int j=0; j<_wfs.size(); ++j)
    if (_wfs[j].ulength() < minlength)
      return false;
  return true;
}


void SyU::wordforms(SpellSets &dict, std::set<std::string> &exclude_tags) try {
  bool default_cok = default_compound();
  bool default_nok = false;
  std::string cat = "normal";

  std::set<std::string> tags;
  if (obj.isMember("tags")) {
    std::string s = obj["tags"].asString() + ",";
    std::string::size_type pos = 0, next;
    do {
      next = s.find(",", pos);
      tags.insert(s.substr(pos, next-pos));
      pos = next+1;
    } while (pos < s.length());

    std::vector<std::string> xt(exclude_tags.size());
    std::vector<std::string>::iterator it =
      std::set_intersection(tags.begin(), tags.end(),
			    exclude_tags.begin(), exclude_tags.end(),
			    xt.begin());
    if (it != xt.begin())
      return;
    
    if (tags.count("nocompsug") or tags.count("obscene"))
      cat = "nosuggest";
    
  }

  std::string sfl = get_attr("spell_flags");
  if (sfl.length()) {
    if (sfl[0] == 'n')
      default_nok = true;
    else if (sfl[0] == 'b')
      cat = "nosuggest";
  }

  if (obj.isMember("spell_cp")) {
    std::string s = obj["spell_cp"].asString() + ",";
    std::string::size_type pos = 0, next;
    do {
      next = s.find(",", pos);
      dict.add(cat, SpellSet::PREFIX, s.substr(pos, next-pos));
      pos = next+1;
      //std::cerr << "From " << pos << " to " << next << ", then " << p
    } while (pos < s.length());
  }

  if (obj.isMember("spell_cm")) {
    std::string s = obj["spell_cm"].asString() + ",";
    std::string::size_type pos = 0, next;
    do {
      next = s.find(",", pos);
      dict.add(cat, SpellSet::MIDDLE, s.substr(pos, next-pos));
      pos = next+1;
    } while (pos < s.length());
  }

  if (obj.isMember("bw")) {
    std::string s = obj["bw"].asString() + ",";
    std::string::size_type pos = 0, next;
    do {
      next = s.find(",", pos);
      if (dict.has(cat, SpellSet::MIDDLE, s.substr(pos, next-pos)))
	dict.add(cat, SpellSet::COK, s.substr(pos, next-pos) + "-");
      else
	dict.add(cat, SpellSet::CNOK, s.substr(pos, next-pos) + "-");      
      pos = next+1;
    } while (pos < s.length());
  }

  if (!sfl.length() and gc() == "prefix") {
    for (unsigned int j=0; j<_wfs.size(); ++j) {
      std::string w = _wfs[j].word();
      dict.add(cat, SpellSet::CNOK, w + "-");
    }
    return;
  }

  if (gc() == "verb") {
    if (tags.count("vsms")) {
      cat = "verb";
    } else if (cat == "normal" and !default_cok and
	       !tags.count("nosms")) {
      bool longword = true;
      for (unsigned int j=0; j<_wfs.size(); ++j)
	if (_wfs[j].ulength() < 5) {
	  longword = false;
	  break;
	}      
      if (longword)
	cat = "verb";
    }
  }
  
  if (gc() == "adjektiv") {
    if (tags.count("asms")) {
      cat = "adjektiv";
    } else if (cat == "normal" and !default_cok and
	       !tags.count("nosms")) {
      bool longword = true;
      for (unsigned int j=0; j<_wfs.size(); ++j)
	if (_wfs[j].ulength() < 5) {
	  longword = false;
	  break;
	}      
      if (longword)
	cat = "adjektiv";
    }
  }
  
  for (unsigned int j=0; j<_wfs.size(); ++j) {
    std::string w = _wfs[j].word();
    if (default_nok) {
      if (default_cok or _wfs[j].has_tag("cok"))
	dict.add(cat, SpellSet::SUFFIX, w);
      else
	dict.add(cat, SpellSet::NOK, w);
      continue;
    }
    if (w.find(" ") != std::string::npos) {
      std::string s = w + " ";
      std::string::size_type pos = 0, next;
      do {
	next = s.find(" ", pos);
	dict.add(cat, SpellSet::CNOK, s.substr(pos, next-pos));
	pos = next+1;
      } while (pos < s.length());
      continue;
    }
    if (_wfs[j].has_tag("nospell")) {
      dict.add(cat, SpellSet::NOK, w);
      continue;
    }
    if (default_cok) {
      if (_wfs[j].has_tag("cnok"))
	dict.add(cat, SpellSet::CNOK, w);
      else
	dict.add(cat, SpellSet::COK, w);
    } else {
      if (_wfs[j].has_tag("cok"))
	dict.add(cat, SpellSet::COK, w);
      else
	dict.add(cat, SpellSet::CNOK, w);
    }
  }
  //std::cerr << "SyU::wordforms() OK" << std::endl;
 } catch (std::exception ex) {
  std::cerr << "SyU::wordforms() failed (exception generated) " << ex.what() << std::endl;
 } catch (...) {
  std::cerr << "SyU::wordforms() failed" << std::endl;
 }


void SyU::all_wfs(std::set<std::string> &cok, std::set<std::string> &cnok,
		  std::set<std::string> &nok) {
  bool default_cok = default_compound();
  bool default_nok = false;
  {
    std::string sfl = get_attr("spell_flags");
    if (sfl.length() and sfl[0] == 'n')
      default_nok = true;
  }
  for (unsigned int j=0; j<_wfs.size(); ++j) {
    std::string w = _wfs[j].word();
    if (default_nok or _wfs[j].has_tag("nospell")) {
      nok.insert(w);
      continue;
    }
    if (default_cok) {
      if (_wfs[j].has_tag("cnok"))
	cnok.insert(w);
      else
	cok.insert(w);	
    } else {
      if (_wfs[j].has_tag("cok"))
	cok.insert(w);
      else
	cnok.insert(w);	
    }
  }
}

void SyU::ok_wfs(std::set<std::string> &wfset) {
  bool default_cok = default_compound();
  for (unsigned int j=0; j<_wfs.size(); ++j) {
    if (_wfs[j].has_tag("nospell"))
      continue;
    if (default_cok) {
      if (_wfs[j].has_tag("cnok"))
	wfset.insert(_wfs[j].word());
    } else {
      if (!_wfs[j].has_tag("cok"))
	wfset.insert(_wfs[j].word());
    }
  }
}

void SyU::cok_wfs(std::set<std::string> &wfset) {
  bool default_cok = default_compound();
  std::string nospell;
  for (unsigned int j=0; j<_wfs.size(); ++j) {
    if (_wfs[j].has_tag("nospell"))
      continue;
    if (default_cok) {
      if (!_wfs[j].has_tag("cnok"))
	wfset.insert(_wfs[j].word());
    } else {
      if (_wfs[j].has_tag("cok"))
	wfset.insert(_wfs[j].word());
    }
  }
}

void SyU::get_wfs(WCMAP &wc) const {
  for (unsigned int j=0; j<_wfs.size(); ++j) {
    std::map<unsigned int, WF>::const_iterator it = modified_wfs.find(j);
    if (it == modified_wfs.end())
      wc[_wfs[j].word()] = 0;
    else
      wc[(it->second).word()] = 0;
  }
  WCMAP::iterator it = wc.find("");
  if (it != wc.end())
    wc.erase(it);
}

const WF &SyU::get_wf(unsigned int wfno) {
  //std::cerr << "Wfno=" << wfno << ", orig=" << _wfs.size() << std::endl;
  std::map<unsigned int, WF>::iterator it = modified_wfs.find(wfno);
  if (it == modified_wfs.end()) {
    if (wfno < _wfs.size())
      return _wfs[wfno];
    throw BadIndex("No such wordform");
  }
  return modified_wfs[wfno];
}

void SyU::add_wf(unsigned int wfno, unsigned int ic,
		 std::string w, std::string t) {
  if (wfno != _next_wfno) {
    std::cerr << "Error: " << wfno << " != " << _next_wfno << std::endl;
    exit(1);
  }
  ++_next_wfno;
  unsigned int s = wfs().size();
  //std::cerr << "add_wf " << wfno << ", ic=" << ic << ", s=" << s << std::endl;
  if (wfno < s or modified_wfs.find(wfno) != modified_wfs.end()) {
    throw BadIndex("Wordform number exists");
  }

  modified_wfs.insert(std::pair<unsigned int, WF>(wfno, WF(ic, w, t)));
}

std::string SyU::old_format() {
  static const char *attname[8] = { "id", "gc", "tags", "spell_flags", "nospell",
				    "spell_cp", "spell_cm", "bw" };
  std::string obj = "{\"rev\":1,";
  for (unsigned int i=0; i<sizeof(attname)/sizeof(char *); ++i) {
    std::string val = get_attr(attname[i]);
    if (val.size()) {
      ((((obj += "\"") += attname[i]) += "\":\"") += val) += "\",";
    }
  }
  obj += "\"wfs\":[";
  if (_wfs.size()) {
    ((obj += "\"") += _wfs[0].word()) += "\"";
  }
  for (unsigned int i=1; i<_wfs.size(); ++i) {
    ((obj += ",\"") += _wfs[i].word()) += "\"";
  }
  obj += "]}";
  return obj;
}

void SyU::add_wf_tag(unsigned int wfno, std::string tag) {
  WF wf = get_wf(wfno);
  wf.add_tag(tag);
  set_wf(wfno, wf.word(), wf.tagstring());
  //std::cerr << "Word " << wf.word() << " (no. " << wfno << "), tags=" << wf.tagstring() << "\n";
}

void SyU::set_wf(unsigned int wfno, std::string w, std::string t) {
  
  //std::cerr << "Wfno=" << wfno << ", orig=" << _wfs.size() << std::endl;
  std::map<unsigned int, WF>::iterator it = modified_wfs.find(wfno);

  //std::cerr << "Found: " << (it == modified_wfs.end()) << std::endl;
  // Is it equal to the original value?
  if (wfno < _wfs.size()) {
    if (_wfs[wfno].is_equal(w, t)) {
      if (it != modified_wfs.end())
	modified_wfs.erase(it);
      return;
    }
  } else if (it == modified_wfs.end())
    throw BadIndex("No such wordform");

  if (it == modified_wfs.end())
    modified_wfs[wfno] = WF(_wfs[wfno].icno(), w, t);
  else
    modified_wfs[wfno].set(w, t);
}

std::string SyU::save(unsigned int new_id) try {
  if (!id())
    obj["id"] = new_id;
  if (modified_wfs.size() > 0) {
    std::vector<unsigned int> remove_pos, add_pos;

    for (std::map<unsigned int, WF>::iterator it = modified_wfs.begin();
	 it != modified_wfs.end(); ++it) {
      unsigned int wfno = it->first;
      const WF &wfrec = it->second;
      if (wfno < _wfs.size()) {
	//std::cerr << "Old: <" << obj["wfs"][wfno]["w"] << ">\n";
	if (wfrec.word().size() > 0) {
	  obj["wfs"][wfno]["w"] = wfrec.word();
	  if (wfrec.tagstring().size()) {
	    obj["wfs"][wfno]["tags"] = wfrec.tagstring();
	  } else {
	    obj["wfs"][wfno].removeMember("tags");
	  }
	} else {
	  remove_pos.push_back(wfno);
	  //std::cerr << "Remove " << wfno << "\n";
	}
      } else if (wfrec.word().size() > 0) {
	add_pos.push_back(wfno);
	//std::cerr << "Add\n";
      }
    }

    unsigned int no_wfs = _wfs.size();

    if (remove_pos.size()) {
      unsigned int curr_pos = remove_pos[0];
      unsigned int keep_pos = remove_pos[0]+1;
      unsigned int r = 1;
      while (true) {
	while (keep_pos < _wfs.size() and r<remove_pos.size()
	       and remove_pos[r] == keep_pos) {
	  ++r;
	  ++keep_pos;
	}
	if (keep_pos >= _wfs.size())
	  break;
	obj["wfs"][curr_pos] = obj["wfs"][keep_pos];
	//std::cerr << "PUT " << keep_pos << "--->" << curr_pos << "\n";
	++curr_pos;
	++keep_pos;
      }
      no_wfs = curr_pos;
      obj["wfs"].resize(no_wfs);
    }

    if (add_pos.size()) {
      //std::sort(add_pos.begin(), add_pos.end());
	
      obj["wfs"].resize(no_wfs + add_pos.size());
      for (unsigned int i = 0; i < add_pos.size(); ++i) {
	obj["wfs"][no_wfs]["ic"] = modified_wfs[add_pos[i]].icno();
	obj["wfs"][no_wfs]["w"] = modified_wfs[add_pos[i]].word();
	if ( modified_wfs[add_pos[i]].tagstring().size() > 0)
	  obj["wfs"][no_wfs]["tags"] = modified_wfs[add_pos[i]].tagstring();
	++no_wfs;
      }
    }
    modified_wfs.clear();
  }

  for (std::map<const char *, std::string>::iterator it=modified_attrs.begin();
       it!=modified_attrs.end(); ++it) {
    if ((it->second).size())
      obj[it->first] = it->second;
    else
      obj.removeMember(it->first);
  }

  if (c_prefix != c_prefix_orig or c_suffix != c_suffix_orig) {
    if (c_prefix == 0 and c_suffix == 0) {
      obj.removeMember("compound");
    } else {
      obj["compound"][0] = c_prefix;
      obj["compound"][1] = c_suffix;
    }
    c_prefix_orig = c_prefix;
    c_suffix_orig = c_suffix;
  }

  modified_attrs.clear();
  //std::cerr << "SyU::save() OK" << std::endl;
  return _writer.write(obj);
 } catch (std::exception ex) {
  std::cerr << "SyU::save() failed (exception generated) " << ex.what() << std::endl;
  return "";
 } catch (...) {
  std::cerr << "SyU::save() failed" << std::endl;
  return "";
 }


void SyU::revert() {
  modified_attrs.clear();
  modified_wfs.clear();
  _next_wfno = _wfs.size();
  c_prefix = c_prefix_orig;
  c_suffix = c_suffix_orig;
  _owner = 0;
}

std::string SyU::get_attr(const char *attr) {
  std::map<const char *, std::string>::iterator it = modified_attrs.find(attr);
  if (it != modified_attrs.end())
    return it->second;
  if (obj.isMember(attr))
    return obj[attr].asString();
  return "";
}

void SyU::set_attr(const char *attr, std::string value) {
  if (obj[attr].asString() == value) {
    std::map<const char *, std::string>::iterator it = modified_attrs.find(attr);
    if (it != modified_attrs.end())
      modified_attrs.erase(it);
    return;
  }
  modified_attrs[attr] = value;
}

std::string SyU::show_long() {
  std::string res = gc() + ": ";
  const std::vector<WF> &v = wfs();
  if (v.size()) {
    res += v[0].word();
    for (unsigned int i = 1; i < v.size(); ++i)
      (res += " ") += v[i].word();
  }
  return res;
}

std::string SyU::show_full() {
  std::string res = show_long();
  static const char *attname[5] = { "tags", "spell_flags",
				    "spell_cp", "spell_cm", "bw" };
  for (unsigned int i=0; i<sizeof(attname)/sizeof(char *); ++i) {
    std::string val = get_attr(attname[i]);
    if (val.size()) {
      ((((res += " ") += attname[i]) += "=\"") += val) += "\"";
    }
  }
  return res;
}
