#include <string.h>
#include <errno.h>

#include <iostream>
#include <fstream>
#include <string>

#include "CorpusTypes.h"

#include "Tokenizer.h"
#include "HunInfoDB.h"

int main(int argc, char *argv[]) {

  if (argc < 2) {
    std::cerr << "usage: " << argv[0] << " filename" << std::endl;
    exit(1);
  }
  
  HunInfoDB *huninfodb = new HunInfoDB();
  Tokenizer tokenizer(huninfodb);
  if (!tokenizer.start_file(argv[1], argc>2)) {
    std::cerr << "cannot open " << argv[1] << ": " << strerror(errno) << std::endl;
    exit(1);
  }

  for (std::string line=tokenizer.next_line(); line.size(); line=tokenizer.next_line()) {
    std::cout << line << std::endl;
  }

  exit(0);
}
