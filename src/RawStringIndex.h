#ifndef __RAWSTRINGINDEX_H__
#define __RAWSTRINGINDEX_H__

#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

#include <algorithm>
#include <iostream>
#include <fstream>
#include <set>
#include <map>

#include "RawStringDB.h"

// Måste kanske komma ihåg gamla positionen för objekten för att kunna
// uppdatera alla index!
//
// 1. Journal av borttagna/uppdaterade. Journalen är en lista av poster
//    med pekare till gamla elementet samt objektets ID.
// 2. Pekare till högsta ID vi har och var i journalen vi är.
// 3. 

/*
  // Nej: Loopa igenom hela journalen, kom ihåg ny-->gammal, bara nyaste
  Sedan, för varje Data, kom ihåg vilka id som ska läggas till eller tas bort.
  MAP: Data --> pos förut, antal förut, vektor av nya!!!
  SPARA DETTA I CACHE!!!!
*/

/*
  Konfigfil anger, separerade av blank:

    1. Filnamn till DbIndex
    2. Högsta ID som vi indexerat
    3. Position i journalen som vi kommit till
*/
//typedef unsigned int DataType;

template <class DataType, class Func>
class RawStringIndex {
  typedef struct { DataType data; unsigned int objId; } Irec;
  typedef struct {
    off_t old_index_pos;
    unsigned int old_count;
    std::set<unsigned int> objIds;
  } Crec;
  typedef std::map<DataType, Crec> CacheType;
 public:
 RawStringIndex(RawStringDB &rsdb, std::string config_filename)
   : rawstringdb(rsdb),
    cfg_filename(config_filename),
    map_addr(0),
    index_bytelength(0),
    index_fd(-1)
      {
	std::ifstream cfg(cfg_filename);
	cfg >> idx_filename >> max_id >> journal_pos;
	if (!cfg) {
	  struct stat st;
	  if (stat(cfg_filename.c_str(), &st) == 0) {
	    throw BadObjDB((("cannot read from file " + cfg_filename)+
			    ": ") + strerror(errno));	    
	  }
	  max_id = 0;
	  journal_pos = 0;
	  idx_filename = config_filename;
	  if (idx_filename.substr(idx_filename.size()-4, 4) != ".bin") {
	    idx_filename.resize(idx_filename.size()-4);
	  }
	  idx_filename += ".bin";
	  std::ofstream cfgfd(cfg_filename);
	  cfgfd << idx_filename << ' ' << max_id << ' ' << journal_pos;
	  cfgfd.close();
	  if (!cfgfd)
	    throw BadObjDB((("cannot read from file " + cfg_filename)+
			    ": ") + strerror(errno));
	}
	map_idx_file();
	update();
      }
  ~RawStringIndex() {
    save_cache();
  }
  // Search for all IDs containing the given data.
  // Will return an empty set unless found.
  const std::set<unsigned int> find_ids(const DataType &data) {
    return fetch_crec(data).objIds;
  }

  // Checks if the RawStringDB has been updated. If it has, all changes
  // are applied to the local cache
  void update() {
    /* Skanna igenom hela journalen. För de gamla ID:na, spara första
       förekomsten i en set<id>. Sedan, loopa igenom alla nya
       ID och lägg till dem. Sedan, loopa igenom alla i map:en och
       lägg till ev. senaste samt ta bort gamla.
    */
    unsigned int next_id = rawstringdb.next_id();
    off_t next_journal_pos = rawstringdb.journal_offset();
    if (next_id == max_id and journal_pos == next_journal_pos)
      return;
    // Store all ID:s to removed/updated objects in oldobj:
    std::set<unsigned int> oldobj;
    while (journal_pos < next_journal_pos) {
      unsigned int id = 0;
      off_t offset = 0;
      rawstringdb.get_journal(journal_pos, id, offset);
      //std::cerr << "Journal: " << id << " at pos " << offset << std::endl;
      if (id < max_id and oldobj.find(id) == oldobj.end()) {
	oldobj.insert(id);
	char *str = rawstringdb.get_old_string(offset);
	const std::set<DataType> &remove_refs(get_refs(id, str));
	free(str);
	for (typename std::set<DataType>::const_iterator p = remove_refs.begin();
	     p!=remove_refs.end(); ++p) {
	  typename CacheType::iterator it = cache.find(*p);
	  if (it == cache.end()) {
	    cache[*p] = fetch_crec(*p);
	    cache[*p].objIds.erase(id);
	  } else {
	    (it->second).objIds.erase(id);
	  }
	}
      }
    }

    // All new IDs must also be added to the cache.
    // And max_id must be updated to next_id.
    for (; max_id < next_id; ++max_id) {
      oldobj.insert(max_id);
    }

    for (std::set<unsigned int>::const_iterator it=oldobj.begin();
	 it!=oldobj.end(); ++it) {
      unsigned int id = *it;
      char *str = rawstringdb.get_string(id);
      const std::set<DataType> &add_refs(get_refs(id, str));
      free(str);
      for (typename std::set<DataType>::const_iterator p = add_refs.begin();
	   p!=add_refs.end(); ++p) {
	//std::cerr << "Data " << *p << " in ID " << id << std::endl;
	typename CacheType::iterator it = cache.find(*p);
	if (it == cache.end()) {
	  cache[*p] = fetch_crec(*p);
	  cache[*p].objIds.insert(id);
	} else {
	  (it->second).objIds.insert(id);
	}
      }
    }

    /*
    std::cerr << "Full cache: " << std::endl;
    for (typename CacheType::iterator it = cache.begin(); it != cache.end(); ++it) {
      std::cerr << "Data: " << it->first << ", IDs: ";
      std::set<unsigned int> &objs = (it->second).objIds;
      for (std::set<unsigned int>::const_iterator p=objs.begin(); p!=objs.end(); ++p)
	std::cerr << *p << ' ';
      std::cerr << std::endl;
    }
    */
  }


  /*
    Merge existing index with cache and save to new file. Then close
    current index and overwrite the old index file and the old config file.
    This will be done at exit. It's quite pointless to call this during
    normal execution unless the cache has grown large, i.e. very many records
    have been added, removed or modified.
    If this is called during normal execution, call map_idx_file() afterwards.
  */
  void save_cache() {

    if (cache.size() == 0)
      return;

    if (!map_addr and index_fd < 0)
      map_idx_file();

    std::string tmp_file = idx_filename + ".tmp";

    int tmp_fd = open(tmp_file.c_str(), O_WRONLY|O_CREAT, 0664);
    if (tmp_fd < 0) {
      throw BadObjDB(("Cannot create file " + tmp_file +
		      ": ") + strerror(errno));
    }

    Irec irec;
    off_t old_index_pos = 0;
    for (typename CacheType::const_iterator it = cache.begin(); it != cache.end(); ++it) {

      irec.data = it->first;

      const std::set<unsigned int> &objIds = (it->second).objIds;
      off_t old_pos = (it->second).old_index_pos;
      unsigned int old_count = (it->second).old_count;

      off_t len = old_pos - old_index_pos;
      if (len > 0) {
	if (write(tmp_fd, (const char *)map_addr + old_index_pos, len) != len) {
	  close(tmp_fd);
	  unlink(tmp_file.c_str());
	  throw BadObjDB(("Cannot write file " + tmp_file +
			  ": ") + strerror(errno));
	}
      }

      old_index_pos = old_pos + (off_t) old_count * sizeof(Irec);

      for (std::set<unsigned int>::const_iterator p = objIds.begin();
	   p != objIds.end(); ++p) {
	irec.objId = *p;
	if (write(tmp_fd, (const char *)&irec, sizeof irec) != sizeof irec) {
	  close(tmp_fd);
	  unlink(tmp_file.c_str());
	  throw BadObjDB(("Cannot write file " + tmp_file +
			  ": ") + strerror(errno));
	}
      }
    }

    {
      off_t len = index_bytelength - old_index_pos;
      if (len > 0) {
	if (write(tmp_fd, (const char *)map_addr + old_index_pos, len) != len) {
	  close(tmp_fd);
	  unlink(tmp_file.c_str());
	  throw BadObjDB(("Cannot write file " + tmp_file +
			  ": ") + strerror(errno));
	}
      }
    }
    
    if (close(tmp_fd) < 0) {
      throw BadObjDB(("Cannot save file " + tmp_file +
		      ": ") + strerror(errno));
    }

    std::string tmp_cfg = cfg_filename + ".tmp";
    std::ofstream cfgfd(tmp_cfg);
    cfgfd << idx_filename << ' ' << max_id << ' ' << journal_pos;
    cfgfd.close();
    if (!cfgfd) {
      throw BadObjDB(("Cannot save file " + tmp_cfg +
		      ": ") + strerror(errno));
    }

    unmap_idx_file();

    if (rename(tmp_cfg.c_str(), cfg_filename.c_str()) < 0) {
      throw BadObjDB(("Cannot overwrite file " + cfg_filename +
		      ": ") + strerror(errno));
    }
    if (rename(tmp_file.c_str(), idx_filename.c_str()) < 0) {
      throw BadObjDB(("Cannot overwrite file " + idx_filename +
		      ": ") + strerror(errno));
    }
  }

 private:
  void map_idx_file() {
    if (map_addr) {
      // Already mapped
      return;
    }
    if (index_fd < 0) {
      index_fd = open(idx_filename.c_str(), O_RDWR | O_CREAT, 0664);
      if (index_fd < 0){
	throw BadObjDB((("cannot open file " + idx_filename)+
			": ") + strerror(errno));
      }
    }
    struct stat st;
    if (fstat(index_fd, &st) < 0) {
      throw BadObjDB(("Cannot stat " + idx_filename+
		      ": ") + strerror(errno));
    }
    index_bytelength = st.st_size;
    if (!index_bytelength)
      return;
    map_addr = (Irec *) mmap(NULL, index_bytelength, PROT_READ|PROT_WRITE, MAP_SHARED, index_fd, 0);
    if ((caddr_t) map_addr == (caddr_t) -1) {
      throw BadObjDB(("Cannot mmap " + idx_filename +
		      ": ") + strerror(errno));
    }
  }

  void unmap_idx_file() {
    if (!map_addr) {
      // Not mapped
      return;
    }
    if (munmap(map_addr, index_bytelength) < 0) {
      throw BadObjDB(("Cannot munmap " + idx_filename +
		      ": ") + strerror(errno));
    }
    map_addr = 0;
    // Let index_bytelength keep its value
    
    close(index_fd);
    index_fd = -1;
  }

  // Find the index record for the given data.
  // First check the cache. If it's not there, check the index file.
  // If it's not there, return an empty record, i.e. old_count is 0,
  // objIds is an empty set and old_index_pos points to where in the
  // index the data _should_ be stored.
  Crec fetch_crec(const DataType &data) {
    typename CacheType::iterator it = cache.find(data);
    if (it != cache.end()) {
      return it->second;
    }
    Crec res = _index_rec(data);
    return res;
  }

  Crec _index_rec(const DataType &data) {
    Crec res = { 0, 0 };

    if (index_fd < 0)
      map_idx_file();
    if (!index_bytelength) 
      return res;

    unsigned int max_pos = index_bytelength/sizeof(Irec);
    unsigned int start_pos = 0, end_pos = max_pos;
    if (map_addr[0].data > data) {
      //std::cerr << "{Idx pos " << res.old_index_pos << "}";
      return res;
    } else if (map_addr[end_pos-1].data < data) {
      res.old_index_pos = (off_t) end_pos * sizeof(Irec);
      //std::cerr << "{Idx pos " << res.old_index_pos << "}";
      return res;
    }
    while (true) {
      unsigned int middle = (start_pos + end_pos) / 2;
      //std::cerr << "Search<" << start_pos << "," << middle << "," << end_pos << ">";
      if (map_addr[middle].data > data) {
	end_pos = middle;
      } else if (map_addr[middle].data < data) {
	if (middle == start_pos) {
	  break;
	}
	start_pos = middle;
      } else {
	for (unsigned int i=middle+1; map_addr[i].data == data; ++i) {
	  ++res.old_count;
	  res.objIds.insert(map_addr[i].objId);
	}

	while (true) {
	  ++res.old_count;
	  res.objIds.insert(map_addr[middle].objId);
	  if (middle == 0 or map_addr[middle-1].data != data) {
	    res.old_index_pos = (off_t) middle * sizeof(Irec);
	    //std::cerr << "{Idx pos " << res.old_index_pos << "}";
	    return res;
	  }
	  --middle;
	}
      }
    }
    res.old_index_pos = (off_t) start_pos * sizeof(Irec);
    //std::cerr << "{Idx pos " << res.old_index_pos << "}";
    return res;
  }

  Func get_refs;
  RawStringDB &rawstringdb;
  std::string cfg_filename;
  Irec *map_addr;
  size_t index_bytelength;
  std::string idx_filename;
  CacheType cache;
  int index_fd;
  unsigned int max_id;
  off_t journal_pos;
};

#endif
