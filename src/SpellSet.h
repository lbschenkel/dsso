#ifndef __SPELLSET_H__
#define __SPELLSET_H__

#include <string>
#include <set>
#include <map>

class SpellSet {
 public:
  enum SS_TYPE {
    COK=0, CNOK=1, PREFIX=2, MIDDLE=3, SUFFIX=4, NOK=5
  };
  static const unsigned int No_Types = 6;
  static const char *filename[No_Types];
  
  virtual ~SpellSet() {}
  SpellSet();
  virtual void add(SS_TYPE type, std::string word);
  virtual bool has(SS_TYPE type, std::string word);
  std::set<std::string> &word_set(SS_TYPE type) {
    return wset[type];
  }
  void save();
 private:
  std::set<std::string> wset[No_Types];
};

class SpellSets {
 public:
  virtual ~SpellSets() {}
  SpellSets() {}
  virtual void add(std::string cat, SpellSet::SS_TYPE type, std::string word);
  virtual bool has(std::string cat, SpellSet::SS_TYPE type, std::string word);
  SpellSet *dict(std::string cat);
  void save(std::string dir);
 private:
  std::map<std::string, SpellSet> dicts;
};

#endif
