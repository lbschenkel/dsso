#include <iostream>
#include <string>
#include "RawStringIndex.h"
#include "SyuGetWordforms.h"

#include "SyU.h"

int main(int argc, char *argv[]) {
  const char *dbfile = (argc == 5) ? argv[1] : "dsso_db.txt";
  const char *dbindx = (argc == 5) ? argv[2] : "dsso_db.bin";
  const char *dbjrnl = (argc == 5) ? argv[3] : "dsso_db.journal";
  const char *wfindx = (argc == 5) ? argv[4] : "dssodb_word_index.txt";

  RawStringDB dssodb(dbfile, dbindx, dbjrnl);
  dssodb.rebuild_index();
  RawStringIndex<unsigned int, SyuGetWords> idx(dssodb, wfindx);
}
