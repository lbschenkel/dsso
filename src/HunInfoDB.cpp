#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <iostream>
#include <fstream>
#include <exception>
#include <locale>

#include "StringDB.h"
#include "StringSort.h"
#include "HunInfoDB.h"


A_Dict TheDicts[3] = {
  {0, spell_devel_aff,  spell_devel_dic,
   SPELL_DEVEL_OK, (unsigned char)~SPELL_DEVEL_OK, 0},
  {1, spell_base_aff, spell_base_dic,
   SPELL_BASE_OK, (unsigned char)~SPELL_BASE_OK, 0},
  {2, spell_sv_se_aff, spell_sv_se_dic,
   SPELL_SV_SE_OK, (unsigned char)~SPELL_SV_SE_OK, 0},
};

const char *HunInfoDB::spell_category_name[16] = {
  "OK", "NOK", "IGNORE", "UNDECIDED",
  "IGNORE: Forget", "OK: Important", "OK: Not important", "OK: Ambigious",
  "NOK: Forbidden", "NOK: Ambigious", "NOK: sv_FI", "IGNORE: ok",
  "IGNORE: nok", "UNDECIDED: Perhaps", "UNDECIDED: Ambigious", "UNDECIDED: Rare"
};

const unsigned char HunInfoDB::spell_category_number[16] = {
  SPELL_CAT_OK, SPELL_CAT_NOK, SPELL_CAT_IGNORE, SPELL_CAT_UNDECIDED,
  SPELL_CAT_IGNORE_FORGET, SPELL_CAT_OK_MAJOR, SPELL_CAT_OK_MINOR, SPELL_CAT_OK_AMBIGIOUS,
  SPELL_CAT_NOK_MAJOR, SPELL_CAT_NOK_AMBIGIOUS, SPELL_CAT_NOK_SV_FI, SPELL_CAT_IGNORE_OK,
  SPELL_CAT_IGNORE_NOK, SPELL_CAT_UNDECIDED_PERHAPS, SPELL_CAT_UNDECIDED_AMBIGIOUS,
  SPELL_CAT_UNDECIDED_RARE
};

void HunInfoDB::refresh() {
  hdb.update();
}

HunInfoDB::HunInfoDB()
  : hdb(InfoDB<unsigned char, HunInfo>::get_db("index/huninfodb.bin")),
    infodb(InfoDB<StringInfo, EmptyStringInfo>::get_db("index/stringinfo.bin")),
    sdb(StringDB::get_db()),
    wc_cmp(infodb) {
  setlocale(LC_ALL, "sv_SE.UTF-8");
  strcpy(prefix_check_buf, PREFIX_STR);
  prefix_check_str = prefix_check_buf + strlen(PREFIX_STR);
  if (!TheDicts[0].dict)
    TheDicts[0].dict = new Hunspell(spell_devel_aff, spell_devel_dic);
  if (!TheDicts[1].dict)
    TheDicts[1].dict = new Hunspell(spell_base_aff, spell_base_dic);
  if (!TheDicts[2].dict)
    TheDicts[2].dict = new Hunspell(spell_sv_se_aff, spell_sv_se_dic);
}

void HunInfoDB::dump_spell_categories() {
  int fd[16];
  for (unsigned char i=1; i<16; ++i) {
    fd[i] = open(get_filename_by_spell_cat(i), O_WRONLY|O_CREAT|O_TRUNC, 0664);
    if (fd[i] < 0) {
      std::cerr << "error creating " << get_filename_by_spell_cat(i) << ": "
		<< strerror(errno) << std::endl;
      return;
    }
  }
  StringSort<LexiCmp> *alpha = StringSort<LexiCmp>::get_db("index/lexisort.txt");
  for (StringSort<LexiCmp>::iterator p = alpha->begin(); p != alpha->end(); ++p) {
    unsigned char cat = get_spell_cat_number(*p);
    if (cat) {
      write(fd[cat], sdb[*p], strlen(sdb[*p]));
      write(fd[cat], "\n", strlen("\n"));
    }
  }
  for (unsigned char i=1; i<16; ++i) {
    if (close(fd[i]) < 0) {
      std::cerr << "error closing " << get_filename_by_spell_cat(i) << ": "
		<< strerror(errno) << std::endl;
    }
  }
}

// Find and return suffix score of given cok-word, where score is based on non-prefixes.
// Cache the return value in suffix_cache.
// Score of each non-prefix shall be added to the WCMAP "prefixes".
unsigned int HunInfoDB::find_prefixes(std::string word, WCMAP &suffix_cache,
				      WCMAP &prefixes) {
  {
    WCMAP::const_iterator  sp = suffix_cache.find(word);
    if (sp != suffix_cache.end())
      return sp->second;
  }
      
  StringSort<RLexiCmp> *beta = StringSort<RLexiCmp>::get_db("index/rlexisort.txt");

  int len = word.size();
  unsigned int score;
  for (StringSort<RLexiCmp>::iterator p = beta->find_first(word.c_str());
       p != beta->end() and strcmp(word.c_str(), sdb[*p]+strlen(sdb[*p])-len) == 0;
       ++p) {
    const char *str = sdb[*p];
    //std::cerr << " ... " << str << std::endl;
    int prefix_len = strlen(str)-len;
    if (!prefix_len)
      continue;
    if ( !spell_ok(*p) ) {
      unsigned char cat = spell_cat_main(*p);
      if (cat == SPELL_CAT_IGNORE or cat == SPELL_CAT_NOK)
	continue;
      std::string prefix(str, str+prefix_len);
      unsigned int sc = infodb.get_info(*p).word_count;
      if (prefixes.find(prefix) == prefixes.end()) {
	prefixes[prefix] = sc;
      } else {
	prefixes[prefix] += sc;
      }
      score += sc;
      // Simplifiedtriple??
    }
  }
  suffix_cache[word] = score;
  return score;
}

// "wc" shall be an empty map that will be populated
// with potential suffixes and corresponding score.
// If prefix is a valid compound prefix, it will be popoulated
// with non-suffixes, otherwise with suffixes.
void HunInfoDB::prefix_analysis(std::string prefix, WCMAP &wc) {
  StringSort<LexiCmp> *alpha = StringSort<LexiCmp>::get_db("index/lexisort.txt");
  bool is_prefix = is_a_prefix(prefix);
  unsigned int len = prefix.size();
  char simplifiedtriple = 0;
  if (len > 2 and prefix[len-1] == prefix[len-2])
    simplifiedtriple = prefix[len-1];
  for (StringSort<LexiCmp>::iterator p = alpha->find_first(prefix.c_str()) ;
       p != alpha->end() and strncmp(prefix.c_str(), sdb[*p], len) == 0; ++p) {
    if ( spell_ok(*p) )
      continue;
    unsigned char cat = spell_cat_main(*p);
    if (cat == SPELL_CAT_IGNORE or cat == SPELL_CAT_NOK)
      continue;
    const char *suff = sdb[*p]+len;
    if (!*suff)
      continue;
    if (simplifiedtriple) {
      if (*suff == simplifiedtriple)
	continue; // Like "bussstation" 
      if (is_a_suffix(suff-1) xor is_prefix) {
	if (wc.count(suff-1))
	  wc[suff-1] += infodb.get_info(*p).word_count;
	else
	  wc[suff-1] = infodb.get_info(*p).word_count;
	//std::cerr << sdb[*p] << ' ' << infodb.get_info(*p).word_count << std::endl;
	continue; // Like "busstation"
      }
    }
    if (is_a_suffix(suff) xor is_prefix) {
      if (wc.count(suff))
	wc[suff] += infodb.get_info(*p).word_count;
      else
	wc[suff] = infodb.get_info(*p).word_count;
      //std::cerr << sdb[*p] << ' ' << infodb.get_info(*p).word_count << std::endl;
    }
  }
  Glib::ustring wu;
  {
    Glib::ustring w(prefix);
    gunichar f=w[0], fu=g_unichar_toupper(f);
    if (f != fu)
      wu = fu + w.substr(1);
  }
  if (wu.size() and (is_prefix or !is_a_prefix(wu)))
    prefix_analysis(wu, wc);
}

// The keys of the map "suffixes" shall be a set of suffixes,
// "wc1", "wc2", "wc3" shall be (empty) maps that will be populated
// with potential prefixes and corresponding score.
// wc1 will have valid compound prefixes, wc2 other prefixes.
// For each key of "suffixes",  the value will be set to the score
// to be gained by allowing it as a general compound suffix
// (the score will be 0 if it already is a compound suffix).
void HunInfoDB::suffix_analysis(WCMAP &suffixes, WCMAP &wc1, WCMAP &wc2, WCMAP &wc3) {
  StringSort<RLexiCmp> *beta = StringSort<RLexiCmp>::get_db("index/rlexisort.txt");
  wc1.clear();
  wc2.clear();
  wc3.clear();
  std::set<unsigned int> used;
  
  for (WCMAP::iterator it = suffixes.begin(); it != suffixes.end(); ++it) {
    unsigned int suffix_score = 0;
    const char *word = (it->first).c_str();
    //std::cerr << "Kollar " << word << std::endl;
    unsigned int len = (it->first).length();
    bool is_suffix = is_a_suffix(word);
    char first_char = *word;
    for (StringSort<RLexiCmp>::iterator p = beta->find_first(word);
	 p != beta->end() and strcmp(word, sdb[*p]+strlen(sdb[*p])-len) == 0;
	 ++p) {
      unsigned char cat = spell_cat_main(*p);
      if (cat == SPELL_CAT_IGNORE or cat == SPELL_CAT_NOK)
	continue;
      if (used.count(*p))
	continue;
      used.insert(*p);
      const char *str = sdb[*p];
      //std::cerr << " ... " << str << std::endl;
      int prefix_len = strlen(str)-len;
      if (!prefix_len)
	continue;
      std::string prefix(str, str+prefix_len);
      unsigned int c = infodb.get_info(*p).word_count;
      bool simplified_triple = false;
      if (prefix_len > 2 and str[prefix_len-1] == first_char) {
	if (str[prefix_len-2] == first_char)
	  continue;
	simplified_triple = true;
      }
      if ( !spell_ok(*p) ) {
	bool is_prefix = is_a_prefix(prefix) or (simplified_triple and is_a_prefix(prefix+first_char));
	if (is_prefix) {
	  if (wc1.find(prefix) == wc1.end())
	    wc1[prefix] = c;
	  else
	    wc1[prefix] += c;
	} else {
	  if (wc2.find(prefix) == wc2.end())
	    wc2[prefix] = c;
	  else
	    wc2[prefix] += c;
	}
	if (!is_suffix and is_prefix)
	  suffix_score += c;
      } else if ( is_suffix and spell_compound(*p) ) {
	if (is_a_prefix(prefix) or (simplified_triple and is_a_prefix(prefix+first_char))) {
	  if (wc3.find(prefix) == wc3.end())
	    wc3[prefix] = c;
	  else
	    wc3[prefix] += c;
	}
      }
    }
    it->second = suffix_score;
  }
}

void HunInfoDB::ends_with(const char *word, bool spell_ok, std::vector<unsigned int> &resv) {
  StringSort<RLexiCmp> *beta = StringSort<RLexiCmp>::get_db("index/rlexisort.txt");

  unsigned char res = spell_ok ? SPELL_DEVEL_OK : 0;
  unsigned int len = strlen(word);
  StringSort<RLexiCmp>::iterator p = beta->find_first(word);
  while (p != beta->end() and strcmp(word, sdb[*p]+strlen(sdb[*p])-len) == 0) {
    if ( (spell_status(*p) & SPELL_DEVEL_OK) == res) {
      resv.push_back(*p);
      //std::cerr << sdb[*p] << ' ' << infodb.get_info(*p).word_count << std::endl;
    }
    ++p;
  }
  std::sort(resv.begin(), resv.end(), wc_cmp);
}

/*
// Return value >= 0 if currently not a valid prefix,
// the value is number of currently "red" words that contain this prefix.
// Negative value (<0) if it is a valid prefix, one less than minus the
// number of compound words with this prefix.
*/
long HunInfoDB::signed_prefix_score(std::string word) {
  if (word.size() == 0)
    return 0;
  std::map<std::string, long>::const_iterator it =
    prefix_score_cache.find(word);
  if (it != prefix_score_cache.end())
    return it->second;
  long res;
  Glib::ustring wu;
  {
    Glib::ustring w(word);
    gunichar f=w[0], fu=g_unichar_toupper(f);
    if (f != fu)
      wu = fu + w.substr(1);
  }
  if (is_a_prefix(word)) {
    long score = _valid_prefix_score(word.c_str());
    if (wu.size()) {
      score += _valid_prefix_score(wu.c_str());
    }
    //std::cerr << "Pf=" << word << ", sc=" << score << "\n";
    res = -score - 1;
  } else {
    res = _prefix_score(word.c_str());
    if (wu.size() and !is_a_prefix(wu.c_str()))
      res += _prefix_score(wu.c_str());
  }
  prefix_score_cache[word] = res;
  return res;
}

unsigned long HunInfoDB::_valid_prefix_score(const char *word) {
  StringSort<LexiCmp> *alpha = StringSort<LexiCmp>::get_db("index/lexisort.txt");
  unsigned int len = strlen(word);
  unsigned long score = 0;
  char simplifiedtriple = 0;
  if (len > 2 and word[len-1] == word[len-2])
    simplifiedtriple = word[len-1];
  for (StringSort<LexiCmp>::iterator p = alpha->find_first(word);
       p != alpha->end() and strncmp(word, sdb[*p], len) == 0; ++p) {
    if ( !spell_compound(*p) )
      continue;
    const char *suff = sdb[*p]+len;
    if (!*suff)
      continue;
    if (simplifiedtriple) {
      if (*suff == simplifiedtriple)
	continue; // Like "bussstation" 
      if (is_a_suffix(suff-1)) {
	score += infodb.get_info(*p).word_count;
	//std::cerr << sdb[*p] << ' ' << infodb.get_info(*p).word_count << std::endl;
	continue; // Like "busstation"
      }	  
    }
    if (is_a_suffix(suff)) {
      score += infodb.get_info(*p).word_count;
      //std::cerr << sdb[*p] << ' ' << infodb.get_info(*p).word_count << std::endl;
    }
  }
  return score;
}

unsigned long HunInfoDB::_prefix_score(const char *word) {
  StringSort<LexiCmp> *alpha = StringSort<LexiCmp>::get_db("index/lexisort.txt");
  unsigned int len = strlen(word);
  unsigned long score = 0;
  char simplifiedtriple = 0;
  if (len > 2 and word[len-1] == word[len-2])
    simplifiedtriple = word[len-1];
  for (StringSort<LexiCmp>::iterator p = alpha->find_first(word) ;
       p != alpha->end() and strncmp(word, sdb[*p], len) == 0; ++p) {
    if ( spell_ok(*p) )
      continue;
    unsigned char cat = spell_cat_main(*p);
    if (cat == SPELL_CAT_IGNORE or cat == SPELL_CAT_NOK)
      continue;
    const char *suff = sdb[*p]+len;
    if (!*suff)
      continue;
    if (simplifiedtriple) {
      if (*suff == simplifiedtriple)
	continue; // Like "bussstation" 
      if (is_a_suffix(suff-1)) {
	score += infodb.get_info(*p).word_count;
	//std::cerr << sdb[*p] << ' ' << infodb.get_info(*p).word_count << std::endl;
	continue; // Like "busstation"
      }
    }
    if (is_a_suffix(suff)) {
      score += infodb.get_info(*p).word_count;
      //std::cerr << sdb[*p] << ' ' << infodb.get_info(*p).word_count << std::endl;
    }
  }
  return score;
}

unsigned long HunInfoDB::suffix_score(const char *word) {
  StringSort<RLexiCmp> *beta = StringSort<RLexiCmp>::get_db("index/rlexisort.txt");
  // Is it already a suffix?
  if (is_a_suffix(word))
    return 0;
  unsigned long score = 0;
  unsigned int len = strlen(word);
  char first_char = *word;
  for (StringSort<RLexiCmp>::iterator p = beta->find_first(word);
       p != beta->end(); ++p) {
    unsigned char cat = spell_cat_main(*p);
    if (cat == SPELL_CAT_IGNORE or cat == SPELL_CAT_NOK)
      continue;
    const char *str = sdb[*p];
    //std::cerr << str << ' ' << infodb.get_info(*p).word_count << std::endl;
    int prefix_len = strlen(str)-len;
    if ( strcmp(word, str+prefix_len) )
      break;
    bool simplified_triple = false;
    if (prefix_len > 2 and str[prefix_len-1] == first_char) {
      if (str[prefix_len-2] == first_char)
	continue;
      simplified_triple = true;
    }
    if ( !spell_ok(*p) ) {
      std::string prefix(str, str+prefix_len);
      if (is_a_prefix(prefix) or (simplified_triple and is_a_prefix(prefix+first_char))) {
	score += infodb.get_info(*p).word_count;
	//std::cerr << sdb[*p] << ' ' << infodb.get_info(*p).word_count << std::endl;
	//} else {
	//std::cerr << "No prefix: " << prefix << std::endl;
      }
    }
  }
  return score;
}

// Count number of compound words with the given word as a suffix:
unsigned long HunInfoDB::valid_suffix_score(const char *word, bool explain) {
StringSort<RLexiCmp> *beta = StringSort<RLexiCmp>::get_db("index/rlexisort.txt");
  unsigned long score = 0;
  unsigned int len = strlen(word);
  char first_char = *word;
  std::ostringstream details;
  for (StringSort<RLexiCmp>::iterator p = beta->find_first(word);
       p != beta->end(); ++p) {
    const char *str = sdb[*p];
    //std::cerr << str << ' ' << infodb.get_info(*p).word_count << std::endl;
    int prefix_len = strlen(str)-len;
    if ( strcmp(word, str+prefix_len) )
      break;
    if ( spell_compound(*p) ) {
      std::string prefix(str, str+prefix_len);
      bool simplified_triple = false;
      if (prefix_len > 2 and str[prefix_len-1] == first_char) {
	if (str[prefix_len-2] == first_char)
	  continue;
	simplified_triple = true;
      }
      if (is_a_prefix(prefix) or (simplified_triple and is_a_prefix(prefix+first_char))) {
	unsigned long wc = infodb.get_info(*p).word_count;
	score += wc;
	if (explain)
	  details << '<' << str << ' ' << wc << "> ";
      }
    }
  }
  if (explain)
    std::cerr << word << ' ' << score << ' ' << details.str() << std::endl;
  return score;
}

void HunInfoDB::starts_with(const char *word, bool spell_ok, std::vector<unsigned int> &resv) {
  StringSort<LexiCmp> *alpha = StringSort<LexiCmp>::get_db("index/lexisort.txt");

  unsigned char res = spell_ok ? SPELL_DEVEL_OK : 0;
  unsigned int len = strlen(word);
  StringSort<LexiCmp>::iterator p = alpha->find_first(word);
  while (p != alpha->end() and strncmp(word, sdb[*p], len) == 0) {
    if ( (spell_status(*p) & SPELL_DEVEL_OK) == res) {
      resv.push_back(*p);
      //std::cerr << sdb[*p] << ' ' << infodb.get_info(*p).word_count << std::endl;
    }
    ++p;
  }
  std::sort(resv.begin(), resv.end(), wc_cmp);
}

void HunInfoDB::reload_devel() {
  reload_dict(TheDicts[HUNDICT_DEVEL], sdb.begin(), sdb.end(), true, HUNDICT_BASE);
}

void HunInfoDB::reload_base() {
  reload_dict(TheDicts[HUNDICT_BASE], sdb.begin(), sdb.end(), true, HUNDICT_SV_SE);
}

void HunInfoDB::reload_sv_se() {
  reload_dict(TheDicts[HUNDICT_SV_SE], sdb.begin(), sdb.end(), true, -1);
}

void HunInfoDB::reload_dict(A_Dict &adict, StringDB::iterator from, StringDB::iterator to,
			    bool print_self_diff, int other_diff) {
  std::ofstream self_in, self_out, other_in, other_out;
  if (print_self_diff) {
    self_in.open("tmp/self.in");
    self_out.open("tmp/self.out");
  }
  if (other_diff >= 0) {
    other_in.open("tmp/other.in");
    other_out.open("tmp/other.out");
  }
  if (adict.no == HUNDICT_DEVEL)
    clear_cache();
  if (adict.dict)
    delete adict.dict;
  unsigned char other_mask = (other_diff >= 0) ? TheDicts[other_diff].mask : 0;
  adict.dict = new Hunspell(adict.aff, adict.dic);
  for (; from != to; ++from) {
    if (get_spell_cat(from) == SPELL_CAT_IGNORE_FORGET)
      continue;
    int infobits;
    bool res = adict.dict->spell(*from, &infobits, (char **)0);
    unsigned char oldinf = hdb.get_info(from.string_number());
    unsigned char newinf = (oldinf & adict.unmask);
    bool oldres = (newinf != oldinf);
    if (res) {
      newinf |= adict.mask;
    }
    if (adict.no == HUNDICT_DEVEL) {
      newinf &= ~SPELL_IS_COMPOUND;
      if (infobits & SPELL_COMPOUND)
	newinf |= SPELL_IS_COMPOUND;
    }
    if (newinf != oldinf) {
      hdb.set_info(from.string_number(), newinf);
      if (res != oldres and print_self_diff) {
	//std::cerr << "Word: " << *from << " now " << (res ? "OK" : "NOK") << std::endl;
	if (res)
	  self_out << *from << ' ' <<
	    infodb.get_info(from.string_number()).word_count << std::endl;
	else
	  self_in << *from << ' ' <<
	    infodb.get_info(from.string_number()).word_count << std::endl;
      }
    }
    if (other_mask) {
      bool other_res = oldinf & other_mask;
      if (res != other_res) {
	if (res)
	  other_out << *from << ' ' <<
	    infodb.get_info(from.string_number()).word_count << std::endl;
	else
	  other_in << *from << ' ' <<
	    infodb.get_info(from.string_number()).word_count << std::endl;
      }
    }
  }
}

unsigned char HunInfo::operator()(StringDB::iterator p) {
  unsigned char res = 0;
  int infobits;
  for (int i=0; i<sizeof(TheDicts)/sizeof(A_Dict); ++i) {
    //std::cerr << "i=" << i << ", sno=" << p.string_number() << std::endl;

    if (TheDicts[i].dict->spell(*p, &infobits, (char **)0)) {
      res |= TheDicts[i].mask;
    }
    if (i==HUNDICT_DEVEL && (infobits & SPELL_COMPOUND)) {
      res |= SPELL_IS_COMPOUND;
    }
  }
  /*
    std::cerr << "DBG: *p = <" << *p << "> Res=" << (unsigned) res
    << " Compound: " << (infobits & SPELL_COMPOUND)  << std::endl;
  */
  return res;
}
