#ifndef __FREQCMP_H__
#define __FREQCMP_H__

#include "StringDB.h"
#include "InfoDB.h"
#include "StringInfo.h"
#include "StringSort.h"

#include <iostream>

#include <glibmm/ustring.h>

struct FreqCmp {
  FreqCmp() : sdb(StringDB::get_db()),
	      infodb(InfoDB<StringInfo, EmptyStringInfo>::get_db("index/stringinfo.bin")) {
  }
  bool operator()(unsigned int id1, unsigned int id2) {
    return ( infodb.get_info(id1).word_count > infodb.get_info(id2).word_count );
  }
  bool filter(const char *str) {
    if (!*str)
      return false;
    Glib::ustring word(str);
    for (Glib::ustring::iterator i = word.begin(); i != word.end() ; ++i) {
      if (*i > 1023)
	return false;
    }
    return true;
  }
private:
  StringDB &sdb;
  InfoDB<StringInfo, EmptyStringInfo> &infodb;
};

#endif
