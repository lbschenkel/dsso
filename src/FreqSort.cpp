#include <iostream>

#include "StringDB.h"
#include "InfoDB.h"
#include "StringInfo.h"
#include "StringSort.h"

#include "FreqCmp.h"


int main(int argc, char *argv[]) {
  StringDB &sdb = StringDB::get_db();
  
  StringSort<FreqCmp> *freq = StringSort<FreqCmp>::get_db("index/freqsort.txt");

  freq->refresh();

  InfoDB<StringInfo, EmptyStringInfo> &idb = InfoDB<StringInfo, EmptyStringInfo>::get_db("index/stringinfo.bin");
  StringSort<FreqCmp>::iterator the_end = freq->end();
  for (StringSort<FreqCmp>::iterator it = freq->begin(); it != the_end; ++it) {
    std::cout << sdb[*it] << ' ' << idb.get_info(*it).word_count << std::endl;
  }

  exit(0);
}
