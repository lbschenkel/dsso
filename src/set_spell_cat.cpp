#include <string.h>
#include <iostream>
#include <fstream>

#include "HunInfoDB.h"
#include "StringInfo.h"
#include "StringSort.h"
#include "InfoDB.h"

int main(int argc, char *argv[]) {
  if (argc != 3) {
    std::cerr << "usage: " << argv[0] << " filename category";
    exit(1);
  }
  std::ifstream fd(argv[1]);
  if (!fd) {
    std::cerr << "cannot open " << argv[1] << ": " << strerror(errno) << std::endl;
    exit(1);
  }
  unsigned char i=0;
  while (i < 16) {
    if (!strcmp(argv[2], spell_category_name_sorted[i]))
      break;
    ++i;
  }
  if (i >= 16) {
    std::cerr << "unknown spell category: " << argv[2] << std::endl;
    exit(1);
  }
  unsigned char cat = (i<<4);
  std::string word;
  HunInfoDB *huninfodb = new HunInfoDB();
  StringDB &sdb = StringDB::get_db();
  while ( fd >> word ) {
    unsigned int sno = sdb.find(word);
    std::cout << "Set " << word << "(sno " << sno << ") from <"
	      << huninfodb->get_spell_cat_name(sno) << "> to " << (int) cat << std::endl;
    huninfodb->set_spell_cat(sno, cat);
  }
  delete huninfodb;
}
