#include <string.h>
#include <iostream>
#include "RawStringDB.h"

int main() {
  RawStringDB strobjdb("test_str_db.txt", "test_str_db.bin", "test_str_db.journal");
  char *str;

  strobjdb.set_string(0, "Id 0");

  str = strobjdb.get_string(0);
  std::cout << "Object 0, len=" << strlen(str) << ": " << str << "\n";
  free(str);

  strobjdb.set_string(1, "Id 1");
  strobjdb.set_string(2, "Id 2");
  strobjdb.set_string(0, "Id 0 uppdaterad");

  str = strobjdb.get_string(0);
  std::cout << "Object 0, len=" << strlen(str) << ": " << str << "\n";
  free(str);
  str = strobjdb.get_string(1);
  std::cout << "Object 1, len=" << strlen(str) << ": " << str << "\n";
  free(str);
  str = strobjdb.get_string(2);
  std::cout << "Object 2, len=" << strlen(str) << ": " << str << "\n";
  free(str);

}
