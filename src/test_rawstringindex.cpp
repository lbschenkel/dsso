#include <iostream>
#include "RawStringIndex.h"

struct DigitTest {
  const std::set<unsigned int> &operator()(unsigned int id, const char *str) {
    static std::set<unsigned int> res;
    res.clear();
    for (const char *pos=str; *pos; ++pos) {
      if (*pos >= '0' and *pos <= '9') {
	res.insert(*pos);
      }
    }
    return res;
  }
};

int main() {
  RawStringDB rsdb("test_str_db.txt", "test_str_db.bin", "test_str_db.journal");
  RawStringIndex<unsigned int, DigitTest> idx(rsdb, "digit_index.txt");
  for (unsigned int d='0'-1; d<='9'+1; ++d) {
    const std::set<unsigned int> &res = idx.find_ids(d);
    std::cout << "All IDs with " << d << ": ";
    for (std::set<unsigned int>::const_iterator p = res.begin(); p!=res.end(); ++p) {
      std::cout << *p << ' ';
    }
    std::cout << std::endl;
  }
}
