#ifndef __STRINGDB_H__
#define __STRINGDB_H__

#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include "defs.h"

const size_t MAX_STRING_LENGTH = 200;

/* 
   Database of strings. It will be mapped into memory for fast access.
   To avoid having duplicate mappings, instantiate an object reference
   with the static method

      StringDB::get_db()

   Strings can be added using this method:

      unsigned int add(const char *str);

   Each string will be assigned a number, its "StringNumber". The same string may be
   added more than once. To avoid this, check if the string already exists in the
   database before adding it (e.g. using the method "find" on a StringSort obejct.)
   
   The StringNumber is a positive integer, starting at 1. NOTE! Each new string number
   will be greater than all existing numbers, but there may be "holes", i.e. there may
   be integers that are not valid StringNumbers.

   The size of the string database is returned by the method

      unsigned int size();

   The value returned by size() is larger than all existing string numbers, and it will be
   the string number given to the next string added to the database.

   Given a StringNumber, the corresponding string can be retrieved using operator[] or
      const char *string_at(unsigned int str_no)
  If str_no is too large, an exception will be thrown. If str_no is larger than
  the memory mapped region, a remapping will be performed first.

   All strings can also be accessed using an iterator,

      StringDB::iterator

   An iterator can be retrieved with the methods

      begin(), end(), iterator(unsigned int string_number = 1)

   On an iterator p, the following operations can be used:

      string_number(), =, *, ++, --, ==, !=

Implementation:
   The strings are stored sequentially in the file named by stringdb_filename.
   Each string is terminated by one or more nul bytes until length is a multiple
   of the constant StrLength (typically 8 or 16).
   The "StringNumber" of a string is its position in the below file divided by StrLength.
   (At position 0 in the file, we must store nul bytes.)
*/

const char *const stringdb_filename = "index/stringdb.bin";

class StringDB {
  typedef std::map<std::string, unsigned int> SMap;

 public:
  static StringDB *get_dbp() {
    if (the_db == 0) {
      the_db = new StringDB();
      iterator::sdb = the_db;
    }
    return the_db;
  }
  static StringDB &get_db() {
    return *get_dbp();
  }
  class iterator {
    friend StringDB *StringDB::get_dbp();
  public:
  iterator(unsigned int string_number = 1 /*, bool filter = false*/)
    : curr_pos(string_number)
      //, use_filter(filter)
      {
      }
    // iterator(const iterator&);
    //~iterator();

    iterator& operator=(const iterator &rhs) {
      curr_pos = rhs.curr_pos;
      return *this;
    }

    bool operator==(const iterator &rhs) const {
      return (curr_pos == rhs.curr_pos);
    }
    bool operator!=(const iterator &rhs) const {
      return (curr_pos != rhs.curr_pos);
    }
    bool operator<(const iterator &rhs) const {
      return (curr_pos < rhs.curr_pos);
    }
    bool operator>(const iterator &rhs) const {
      return (curr_pos > rhs.curr_pos);
    }
    bool operator<=(const iterator &rhs) const {
      return (curr_pos <= rhs.curr_pos);
    }
    bool operator>=(const iterator &rhs) const {
      return (curr_pos >= rhs.curr_pos);
    }
    /*
    iterator &next_substring(const char *substr) {
      const char *pos = (*sdb)[curr_pos];
      const char *end = (*sdb)[0] + StrLength * (*sdb).end().string_number();
      //std::cerr << "From " << pos << " to " << end << std::endl;
      unsigned int len = strlen(substr);
      if (!len)
	return *this;
      {
	char first = substr[0];
	char second = substr[1];
	if (len == 1) {
	  while (pos < end)
	    if (*pos++ == first)
	      break;
	} else if (len == 2) {
	  while (pos < end)
	    if (*pos++ == first)
	      if (*pos == second)
		break;
	} else {
	  const char *thirdpos = &substr[2];
	  len -= 2;
	NEXT_3:
	  while (pos < end)
	    if (*pos++ == first)
	      if (*pos == second) {
		for (const char *p=pos+1, *e=p+len, *s=thirdpos; p < e; ++p, ++s)
		  if (*p != *s)
		    goto NEXT_3;
		break;
	      }
	}
      }
      // Move back to start of word:
      while (*pos)
	--pos;
      curr_pos = (pos + 1 - (*sdb)[0]) / StrLength;
      return *this;
    }
    */
    iterator &operator++() {
      curr_pos += (strlen((*sdb)[curr_pos]) / StrLength + 1);
      return *this;
    }
    iterator operator++(int) {
      iterator it(*this);
      curr_pos += (strlen((*sdb)[curr_pos]) / StrLength + 1);
      return it;
    }
    iterator& operator--() {
      do {
	--curr_pos;
      } while ( curr_pos>0 and *((*sdb)[curr_pos]-1) );
      return *this;
    }
    iterator operator--(int) {
      iterator it(*this);
      --(*this);
      return it;
    }
    const char *operator*() const {
      return (*sdb)[curr_pos];
    }
    unsigned int string_number() {
      return curr_pos;
    }
    //pointer operator->() const;
  private:
    static StringDB *sdb;
    unsigned int curr_pos;
    //bool use_filter;
  };
  iterator begin() {
    return iterator();
  }
  iterator end() {
    return iterator(curr_size);
  }
  ~StringDB();
  const char *operator[](unsigned int str_no) const {
    return string_at(str_no);
  }
  const char *string_at(unsigned int str_no) const {
    if (str_no < curr_size)
      return stringdb_addr+str_no*StrLength;
    std::ostringstream msg;
    msg << "String number " << str_no << " does not exist (next is " << curr_size << ")";
    throw BadIndex(msg.str());
  }
  // Add string (no check for duplicates, should be done by client), then return its StringNumber:
  unsigned int add(const char *str);
  // The number of StrLength blocks in the file (probably greater than number of words):
  unsigned int size() {
    return curr_size;
  }
  unsigned int find(std::string str);
  unsigned int find_or_add(std::string str) {
    unsigned int sno = find(str);
    if (sno)
      return sno;
    return add(str.c_str());
  }
 private:
  StringDB();
  static void clean_up() {
    if (the_db) {
      delete the_db;
      the_db = 0;
    }
  }
  static StringDB *the_db;
  int stringdb_fd;

  // Position of memory mapping:
  char *stringdb_addr;

  // Size of DB file:
  size_t stringdb_filesize;

  // Mapped part of DB file:
  size_t stringdb_maplen;

  // Number of StringNumber positions currently
  unsigned int curr_size;

  // Number of StringNumber positions within mapped memory.
  // May be larger than curr_size.
  unsigned int curr_map_size;

  SMap cache;

  void remap_dbfile();
  void get_db_size();
};

#endif
