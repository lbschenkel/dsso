#include "Tokenizer.h"
#include "HunInfoDB.h"

#include <set>
#include <string>

namespace {
  std::set<std::string> top_level_domain;

  const char *tlds[] = {
    // top level domains:
    "se", "com", "nu", "org", "net", "dk", "no", "jp", "fi", "eu", "it",
    "tv", "uk", "pl", "de",
    // file name extensions:
    "exe", "txt", "text", "jpg", "jpeg", "html", "htm", "dll",
    "php", "bin", "cpp", "png", "js", "cpp", "c", "gif"
  };

  bool initialize() {
    for (unsigned int i=0; i<sizeof(tlds)/sizeof(const char *); ++i)
      top_level_domain.insert(tlds[i]);
    return true;
  }

  bool initialized = initialize();
}

Tokenizer::Tokenizer(HunInfoDB *hinfo) :
  huninfodb(hinfo) {
}


// Om slutar med punkt, så kolla om det kanske är en förkortning.
// Om punkten är i mitten,  så kolla om det är en webbadress (stallet.se)
// eller ett missat blanktecken (slut.Och) eller en misslyckad förkortning
// (t.ex, bl.a o.s.v).
unsigned int Tokenizer::singledot(Glib::ustring::const_iterator beg,
				  Glib::ustring::const_iterator end,
				  std::deque<int> &wordindex, unsigned int offset) {
  Glib::ustring::const_iterator theDot;
  unsigned int dotpos = 0, colonpos = 0, dashcount = 0, len = 0;
  bool has_char_before = false;
  bool has_char_after = false;
  for (Glib::ustring::const_iterator i(beg); i != end; ++i, ++len) {
    if (g_unichar_isalpha(*i)) {
      has_char_after = true;
    } else {
      switch(*i) {
      case '.':
	has_char_before = has_char_after;
	has_char_after = false;
	if (len and !has_char_before) {
	  return 0;
	}
	dotpos = len;
	theDot = i;
	break;
      case ':':
	colonpos = len;
	break;
      case '-':
	++dashcount;
	break;
      }
    }
  }
  if (!has_char_after and dotpos+1 != len) {
    // No alphabetic character after the dot, remove the junk
    end = theDot;
    ++end;
    len = dotpos+1;
  }

  if (dotpos+1 == len) {
    // Om ordet är rättstavat, så returnera 3
    // om ordet med punkt är en känd förkortning, så returnera 1
    // annars returnera 2 - kolla även om nästa är en versal....
    int sp_res;
    if (dotpos == 1) {
      switch (*beg) {
      case 'i': case 229: case 246:  // i å ö
	sp_res = 1;
	break;
      default:
	sp_res = 0;
	break;
      }
    } else {
      Glib::ustring word(beg, end);
      sp_res = huninfodb->check_word(word.c_str());
    }
    //std::cerr << "Word1: " << word.c_str() << ", res=" << sp_res << std::endl;
    if (!sp_res) {
      wordindex.push_back(offset);
      wordindex.push_back(len);
      return 2;
    }
    sp_res = huninfodb->check_word(Glib::ustring(beg, theDot).c_str());
    if (!sp_res) {
      wordindex.push_back(offset);
      wordindex.push_back(len);
      return 1;
    }
    wordindex.push_back(offset);
    wordindex.push_back(len-1);
    return 3;
  }

  if (colonpos == len-1) {
    --len;
  }

  if (dotpos > 1 and dotpos < len-2) {
    // Might be a web address (e.g. perl.org) or a missing space (e.g. done.But)
    ++theDot;
    Glib::ustring word(theDot, end);
    if (g_unichar_isupper(*theDot)) {
      // Om första ordet är OK och det andra med gemen är OK, så splittra.
      int sp_res = huninfodb->check_word(word.c_str());
      if (sp_res) {
	wordindex.push_back(offset);
	wordindex.push_back(dotpos-1);
	wordindex.push_back(offset+dotpos+1);
	wordindex.push_back(len-dotpos-1);
	return 0;
      }
      wordindex.push_back(offset);
      wordindex.push_back(len);
      return 0;
    }
    if (top_level_domain.find(word) == top_level_domain.end()) {
      wordindex.push_back(offset);
      wordindex.push_back(len);
    }
    return 0;
  } else if (dotpos == 0) {
    // .Versal --> meningsslut
    // .ord --> splittra, ej meningsslut
    // .ejord --> ej splittra, ej meningsslut
    ++theDot;
    Glib::ustring word(theDot, end);
    int sp_res = huninfodb->check_word(word.c_str());
    if (sp_res) {
      if (g_unichar_isupper(*theDot)) {
	wordindex.push_back(offset+1);
	wordindex.push_back(0);
      }
      wordindex.push_back(offset+1);
      wordindex.push_back(len-1);
    }
    return 0;
  } else {
    wordindex.push_back(offset);
    wordindex.push_back(len);
    return 0;
  }
  return 0;
}

// Två eller fler på slutet -- som vanlig punkt
// En på slutet - förkortning eller skit, ev. meningsslut.
// Annars: skit.
unsigned int Tokenizer::multidots(Glib::ustring::const_iterator beg,
				  Glib::ustring::const_iterator end,
				  std::deque<int> &wordindex, unsigned int offset) {
  unsigned int len = 0;
  std::vector<Glib::ustring> words;
  std::vector<int> dotpos;
  Glib::ustring::const_iterator lastword(beg);
  for (Glib::ustring::const_iterator i(beg); i != end; ++i, ++len) {
    if (*i == '.') {
      if (i != lastword) {
	Glib::ustring w(lastword, i);
	words.push_back(w);
      }
      lastword = i;
      ++lastword;
      dotpos.push_back(len);
      //std::cerr << " [" << len << "] ";
    }
  }
  if (lastword != end) {
    Glib::ustring w(lastword, end);
  }
  if (dotpos.size() == 1)
    return singledot(beg, end, wordindex, offset);
  if (dotpos.size() == 0) // Shouldn't happen
    return 0;

  if (len-dotpos[0] == dotpos.size()) {
    // All dots at the end
    wordindex.push_back(offset);
    wordindex.push_back(dotpos[0]);
    return 3;
  }

  if (dotpos.back() < dotpos.size()) {
    // All dots at the beginning
    wordindex.push_back(offset+dotpos.size());
    wordindex.push_back(len-dotpos.size());
    return 0;
  }
  
  if (dotpos.back()+1 == len) {
    // Last character is a dot
    if (dotpos[0] != 0) {
      // Check if it's a valid abbreviation
      Glib::ustring word(beg, end);
      int sp_res = huninfodb->check_word(word.c_str());
      //std::cerr << "Word3: " << std::string(word) << ", res=" << sp_res << std::endl;
      if (sp_res) {
	wordindex.push_back(offset);
	wordindex.push_back(len);
	return 1;
      }
      // Check if it's a domain name or filename
      if (top_level_domain.find(words.back()) != top_level_domain.end()) {
	return 2;
      }
    }
    // Add the separate words
    dotpos.push_back(len);
    for (unsigned int i=0, lpos=0; i<dotpos.size(); ++i) {
      if (dotpos[i] > lpos) {
	wordindex.push_back(offset+lpos);
	wordindex.push_back(dotpos[i]-lpos);
      }
      lpos = dotpos[i]+1;
    }
    return 2;
  }

  if (dotpos[0] != 0) {
    // Check if it's a domain name or filename
    Glib::ustring w(lastword, end);
    if (top_level_domain.find(w) != top_level_domain.end()) {
      return 0;
    }
  }
  // OK, so just add the separate words:
  dotpos.push_back(len);
  for (unsigned int i=0, lpos=0; i<dotpos.size(); ++i) {
    if (dotpos[i] > lpos) {
      wordindex.push_back(offset+lpos);
      wordindex.push_back(dotpos[i]-lpos);
    }
    lpos = dotpos[i]+1;
  }
  if (*--end == ':')
    --wordindex.back();
  return 0;
}

// In: följd av icke-blanka tecken.
// Alla ordsträngar läggs till i vektorn.
// Ut: sannolikhet för slut på mening.
unsigned int Tokenizer::check_string(Glib::ustring::const_iterator beg,
				     Glib::ustring::const_iterator end,
				     std::deque<int> &wordindex, unsigned int offset) {
  Glib::ustring::const_iterator i(end);
  --i;
  if (*i == '?' or *i == '!' or *i == '.') {
    check_string(beg, i, wordindex, offset);
    return 3;
  }
  if (*i == ':' or *i == ';' or *i == ',') {
    check_string(beg, i, wordindex, offset);
    return 0;
  }
  if (*beg == '(' or *beg == ')' or *beg == '"' or *beg == '\'' or *beg == '_' or *beg == '*') {
    ++beg;
    return check_string(beg, end, wordindex, offset+1);
  }
  if (*i == ')' or *i == '(' or *i == '"' or *i == '\'' or *i == '_' or *i == '*') {
    unsigned int res = check_string(beg, i, wordindex, offset);
    return res;
  }
  unsigned int pos = 0, word_start_pos = 0;
  bool has_alpha = false;
  unsigned int dot_count = 0;

  //if (g_unichar_isalpha(*i)) {
  //} else if (g_unichar_isdigit(*i)) {	  

  Glib::ustring::const_iterator word_start(end);
  for (i=beg; ; ++i, ++pos) {
    if (i == end) {
    } else if (g_unichar_isalpha(*i)) {
      has_alpha = true;
      if (word_start == end) {
	word_start = i;
	word_start_pos = pos;
      }
      continue;
    } else if (g_unichar_isdigit(*i) or *i == '-') {
      if (word_start == end) {
	word_start = i;
	word_start_pos = pos;
      }
      continue;
    } else if (*i == ':') {
      Glib::ustring::const_iterator ii(i);
      if ( (pos-word_start_pos<5 and !dot_count) or (pos-word_start_pos>2 and *--ii=='p' and *--ii=='t')) {
	ii=i;
	if (++ii!=end and *ii=='/' and ++ii!=end and *ii=='/') {
	  // Like http://www.some.thing/...
	  --end;
	  if (*end == '.' or *end == '!' or *end == '?') {
	    return 3;
	  } else {
	    return 0;
	  }
	}
      }
      continue;
    } else if (*i == '.') {
      // Ignore unless already in a word:
      if (word_start != end)
	++dot_count;
      continue;
    }
    if (has_alpha and word_start != end) {
      if (!dot_count) {
	wordindex.push_back(offset+word_start_pos);
	wordindex.push_back(pos-word_start_pos);
	//std::cerr << " {" << word_start_pos << ".." << pos << "} ";
      } else if (dot_count == 1) {
	return singledot(word_start, i, wordindex, offset+word_start_pos);
      } else {
	return multidots(word_start, i, wordindex, offset+word_start_pos);
      }
    }

    if (i == end)
      break;
    has_alpha = false;
    word_start = end;
    dot_count = 0;
  }
  return 0;
}


// Lägg in byte-position och längd för _ord_ ur line. Vid meningsslut, lägg in position
// och längden 0.
// I possible_sentence_end sparas information från föregående rad om sannolikheten om meningsslut:
  // 0=hardly, 1=maybe, 2=probably, 3=certainly
void Tokenizer::tokenize_line(const Glib::ustring &line, std::deque<int> &word_pos, unsigned int &possible_sentence_end) {
  //std::cerr << "Line <" << std::string(line) << ">" << std::endl;
  //std::cerr << "Line <" << std::endl;
  if (!line.validate())
    return;
  int pos = 0;
  bool first_possible_word = true;
  for (Glib::ustring::const_iterator i = line.begin(); ; ) {
    while (i != line.end() and g_unichar_isspace(*i))
      ++i, ++pos;
    if (i == line.end())
      break;
    Glib::ustring::const_iterator start(i);
    int startpos = pos;
    bool has_alpha = false, is_junk = false, separators = false;
    unsigned int colon_count = 0;
    unsigned int dot_count = 0;
    while (i != line.end() and !g_unichar_isspace(*i)) {
      if (is_junk) {
      } else if (g_unichar_isalpha(*i)) {
	has_alpha = true;
      } else if (g_unichar_isdigit(*i)) {	  
      } else {
	switch (*i) {
	case ':':
	  ++colon_count;
	  break;
	case '-':
	  break;
	case '.':
	  ++dot_count;
	  break;
	case '?': case '!': case ',': case '_': case '*':
	case ';': case '"': case '\'': case '(': case ')':
	case '/':
	case 8221: case 8230: // ” …
	  separators = true;
	  break;
	default:
	  is_junk = true;
	  break;
	}
      }
      ++i, ++pos;
    }
    // 0=hardly, 1=possibly, 2=probably
    unsigned int possible_line_start = 0;
    if (g_unichar_isupper(*start)) {
      possible_line_start = 1;
      Glib::ustring lword = Glib::ustring(start, i).lowercase();
      int sp_res = huninfodb->check_word(lword.c_str());
      if (sp_res) {
	// Probably not a name
	possible_line_start = 2;
	if (first_possible_word) {
	  if (!possible_sentence_end)
	    possible_sentence_end = 1;
	} else if (!possible_sentence_end) {
	  int ch = *--start;
	  ++start;
	  if (ch == '\n' or ch == '\r' or ch == '\v' or ch == '\f')
	    possible_sentence_end = 1;
	}
      }
    }

    if (possible_sentence_end+possible_line_start>2) {
      word_pos.push_back(startpos);
      word_pos.push_back(0);
    }

    if (has_alpha)
      first_possible_word = false;

    if (has_alpha and !is_junk) {
      if (separators or colon_count>1)
	possible_sentence_end = check_string(start, i, word_pos, startpos);
      else if (dot_count > 1) {
	possible_sentence_end = multidots(start, i, word_pos, startpos);
      } else if (dot_count) {
	possible_sentence_end = singledot(start, i, word_pos, startpos);
	//std::cerr << "Single returned " << possible_sentence_end << std::endl;
      } else {
	word_pos.push_back(startpos);
	word_pos.push_back(pos-startpos);
	possible_sentence_end = 0;
      }	
    } else {
      // Check for trailing ?!. or (!)
      Glib::ustring::const_iterator last(i);
      --last;
      if (*last == ')' and last != start)
	--last;
      if (*last == '.' or *last == '!' or *last == '?')
	possible_sentence_end = 3;
      else
	possible_sentence_end = 0;
    }
  }
}

bool Tokenizer::start_file(std::string filename, bool pre_tokenized) {

  corpus_fd.open(filename);
  if (!corpus_fd) {
    std::cerr << "cannot open " << filename << ": " << strerror(errno) << std::endl;
    return false;
  }
  curr_lines.clear();
  possible_sentence_end = 0;
  file_is_pre_tokenized = pre_tokenized;
  return true;
}


std::string Tokenizer::next_line() {

  std::string _line;

  if (curr_lines.size()) {
    _line = curr_lines.front();
    curr_lines.pop_front();
    if (!_line.size())
      return next_line();
    return _line;
  }

  if ( !getline(corpus_fd, _line) ) {
    // End of file or error:
    if (curr_line.size()) {
      curr_lines.push_back(curr_line);
      curr_line.clear();
      return next_line();
    } else {
      return "";
    }
  }

  Glib::ustring line(_line);
  std::deque<int> word_index;
  tokenize_line(line, word_index, possible_sentence_end);
  Glib::ustring::const_iterator it=line.begin();
  int junk_word_pos = -1;
  unsigned int pos = 0;
  Glib::ustring::const_iterator junk_it;
  for (unsigned int i=0; i<word_index.size(); i+=2) {
    while (pos<word_index[i]) {
      if (g_unichar_isspace(*it)) {
	if (junk_word_pos >= 0) {
	  curr_line += std::string(Glib::ustring(junk_it, it));
	  curr_line += "\t";
	  junk_word_pos = -1;
	}
      } else if (junk_word_pos < 0) {
	junk_it = it;
	junk_word_pos = pos;
      }
      ++it, ++pos;
    }
    if (junk_word_pos >= 0) {
      curr_line += std::string(Glib::ustring(junk_it, it));
      curr_line += "\t";
      junk_word_pos = -1;
    }
    char sep = ' ';
    int len = word_index[i+1];
    if (len) {
      if (len < 0) {
	len = -len;
	sep = '\t';
      }
      Glib::ustring::const_iterator beg(it);
      for (int j=0; j<word_index[i+1]; ++j)
	++it, ++pos;
      std::string word(Glib::ustring(beg, it));
      curr_line += word;
      curr_line += sep;
    } else if (!file_is_pre_tokenized) {
      curr_lines.push_back(curr_line);
      curr_line.clear();
    }
  }
  while (pos<line.size()) {
    if (g_unichar_isspace(*it)) {
      if (junk_word_pos >= 0) {
	curr_line += std::string(Glib::ustring(junk_it, it));
	curr_line += "\t";
	junk_word_pos = -1;
      }
    } else if (junk_word_pos < 0) {
      junk_it = it;
      junk_word_pos = pos;
    }
    ++it, ++pos;
  }
  if (junk_word_pos >= 0) {
    curr_line += std::string(Glib::ustring(junk_it, it));
    curr_line += "\t";
  }
  if (file_is_pre_tokenized) {
    curr_lines.push_back(curr_line);
    curr_line.clear();
  }
  return next_line();
}
