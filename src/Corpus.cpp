#include "Corpus.h"
#include <iostream>

#include <glibmm/ustring.h>

namespace {

struct Wrecord {
  unsigned int word_no;
  unsigned int count;
  CPointer pos;
};

typedef std::map< std::string, Wrecord> Windex;

const char *const corpus_filename = "corpus.txt";

}

CPointer Corpus::line_search(CPointer pos, std::string word) {
  std::string line = line_at(pos);
  std::string::size_type res = line.find(word);
  if (res == std::string::npos)
    return 0;
  return pos + res;
}

unsigned int Corpus::search2(const std::string &word1, const std::string &word2, CPointer *buf, unsigned int max_hits, unsigned int from_no) {

  unsigned int wp1 = string_db.find(word1);
  if (!wp1)
    return 0;
  unsigned int wp2 = string_db.find(word2);
  if (!wp2)
    return 0;

  StringInfo inf1 = _infodb.get_info(wp1);
  StringInfo inf2 = _infodb.get_info(wp2);

  /*
  if (inf1.word_count > inf2.word_count)
    std::swap(wp1, wp2);

  unsigned int n1 = (wp1->second).count, n2 = (wp2->second).count;

  if ( n1 == 1 ) {
    if (line_search((wp1->second).pos, word2)) {
      *buf = (wp1->second).pos;
      return 1;
    }
    return 0;
  }
  */
  return idx.find_union(inf1.indexpos, inf1.word_count, inf2.indexpos, inf2.word_count, buf, max_hits, from_no);
}

unsigned int Corpus::search(const char *word, CPointer *buf, unsigned int max_hits, unsigned int from_no) {
  unsigned int wp1 = string_db.find(word);
  //std::cerr << "W=" << word << ", StringNumber=" << wp1 << std::endl;
  if (!wp1) {
    //std::cerr << "no hits" << std::endl;
    return 0;
  }
  StringInfo inf1 = _infodb.get_info(wp1);
  //std::cerr << "Pos=" << inf1.indexpos << ", count=" << inf1.word_count << std::endl;
  if ( inf1.word_count == 1 ) {
    *buf = inf1.indexpos;
    return 1;
  }
  if (max_hits > ( inf1.word_count - from_no)) {
    max_hits = inf1.word_count - from_no;
  }
  idx.load_hits( inf1.indexpos+from_no, buf, max_hits );
  return max_hits;
}

Corpus::~Corpus() {
  corpus_fd.close();
}

Corpus::Corpus() :
  string_db(StringDB::get_db()),
  corpus_fd(corpus_filename),
  _infodb(InfoDB<StringInfo, EmptyStringInfo>::get_db("index/stringinfo.bin"))
{
  incremental_update(0);
}

void Corpus::get_corpus_size() {
  corpus_fd.seekg(0, corpus_fd.end);
  corpus_size = corpus_fd.tellg();
  if (!corpus_fd) {
    corpus_fd.clear();
    struct stat st;
    if (stat(corpus_filename, &st) < 0 or !corpus_fd) {
      std::cerr << "cannot open " << corpus_filename << ": " << strerror(errno);
      throw BadCorpus();
    }
    corpus_size = st.st_size;
  }
}

bool Corpus::is_up_to_date() {
  get_corpus_size();
  if (idx.curr_pos() >= corpus_size)
    return true;
  return false;
}

bool Corpus::incremental_update(CPointer max_increment) {
  CPointer curr_pos = idx.curr_pos();

  //std::cerr << "Current index position: " << curr_pos << std::endl;
  //std::cerr << "Corpus size: " << corpus_size << std::endl;

  if  (is_up_to_date())
    return false;

  corpus_fd.seekg(curr_pos, corpus_fd.beg);

  CPointer max_pos = max_increment ? curr_pos + max_increment : corpus_size;
  std::string line;
  while ( getline(corpus_fd, line) ) {
    //std::cerr << "Pos " << curr_pos << " Line: <" << line << ">" << std::endl;
    std::istringstream theLine(line);
    std::string word;
    while ( theLine >> word ) {
      if (!is_word(word)) {
	//std::cerr << word << ": not a word" << std::endl;
	continue;
      }
      unsigned int sno = string_db.find_or_add(word.c_str());
      if (!sno)
	continue;  // Failed, ignore.
      StringInfo old_info = _infodb.get_info(sno);
      if (old_info.word_count == 0) {
	// Only one occurrence, store indexpos directly:
	old_info.indexpos = curr_pos;
	old_info.word_count=1;
	_infodb.set_info(sno, old_info);
      } else if (old_info.word_count == 1) {
	// Allocate memory
	CPointer ipos = idx.alloc(4);
	// Old "indexpos" is in fact the corpus position of the former occurence:
	set_idx(ipos, old_info.indexpos);
	old_info.indexpos = ipos;
	set_idx(ipos+1, curr_pos);
	++old_info.word_count;
	_infodb.set_info(sno, old_info);
      } else if ((old_info.word_count == 2) or (old_info.word_count & (old_info.word_count-1))) {
	// old_info is not a power of 2, there is still index space left
	// since the allocated size is always a power of 2.
	set_idx(old_info.indexpos+old_info.word_count, curr_pos);
	++old_info.word_count;
	_infodb.set_info(sno, old_info);
      } else {
	// old_info is a power of 2, we need to allocate twice as much memory
	// and move the old index.
	unsigned long asize = 2 * (unsigned long) old_info.word_count;
	CPointer oldpos = old_info.indexpos;
	idx.dealloc(old_info.word_count, oldpos);
	old_info.indexpos = idx.alloc(asize);
	idx.copy_idx(oldpos, old_info.word_count, old_info.indexpos);
	set_idx(old_info.indexpos+old_info.word_count, curr_pos);
	++old_info.word_count;
	_infodb.set_info(sno, old_info);
      }
    }
    curr_pos = corpus_fd.tellg();
    if (curr_pos >= max_pos)
      break;
  }
  idx.set_pos(curr_pos);
  corpus_fd.clear();
  return true;
}

void Corpus::do_update_index() {
  
  //  First count all tokens to be added,
  //   then allocate index memory
  //   and finally write to index.

  CPointer curr_pos = idx.curr_pos();

  //std::cerr << "Current index position: " << curr_pos << std::endl;
  //std::cerr << "Corpus size: " << corpus_size << std::endl;

  corpus_fd.seekg(curr_pos, corpus_fd.beg);
  Windex wcount;

  std::string word;
  std::cerr << "Pass 1: counting words..." << std::endl;
  CPointer no_words = 0, no_unique_words = 0;
  CPointer log_size = (corpus_size-curr_pos+99)/100;
  CPointer next_log = curr_pos + log_size;
  int log_percent = 0;
  while ( corpus_fd >> word ) {

    if (!is_word(word)) {
      //std::cerr << word << ": not a word" << std::endl;
      continue;
    }
    
    ++no_words;

    if (wcount.find(word) == wcount.end()) {
      wcount[word].count = 1;
      ++no_unique_words;
    } else {
      ++wcount[word].count;
    }

    if (corpus_fd.tellg() >= next_log){
      std::cerr << "" << ++log_percent << "% ";
      next_log += log_size;
    }
  }

  std::cerr << "Found " << no_words << " strings, " << no_unique_words << " unique.\n" << std::endl;
  std::cerr << "Pass 2: allocating index space..." << std::endl;

  for (Windex::iterator p = wcount.begin(); p != wcount.end(); p++) {

    const std::string &word(p->first);

    // Number of index positions to allocate, shall be at least as large as the word count,
    // but not twice as big (though not less than 4):
    unsigned int asize = 4;

    Wrecord &newwr = p->second;
    newwr.word_no = string_db.find(word.c_str());
    if (newwr.word_no == 0) {
      if (newwr.count > 1) {
	while (asize < newwr.count)
	  asize <<= 1;
	newwr.pos = idx.alloc(asize);
	newwr.word_no = string_db.add(word.c_str());
	_infodb.add(newwr.word_no, StringInfo(newwr.pos, newwr.count));
	//std::cout << " alloc " << asize << " at " << wr.pos << std::endl;
      } else {
	// Don't allocate index storage since there is only one occurrence,
	// will store its index directly
	newwr.pos = 0;
	// When we reach the single occurence, we'll know the pos; then we'll
	// store the word in the string DB. For now, pos = 0 means no alloc.

	//std::cout << " no alloc " << std::endl;
      }      
    } else {
      StringInfo old_info = _infodb.get_info(newwr.word_no);
      if (old_info.word_count == 1) {
	// Only one occurrence of this word before, no memory was allocated.
	old_info.word_count = newwr.count + 1;
	while (asize < old_info.word_count)
	  asize <<= 1;
	newwr.pos = idx.alloc(asize);
	set_idx(newwr.pos, old_info.indexpos);
	old_info.indexpos = newwr.pos;
	++newwr.pos;
      } else if (old_info.word_count == 0) {
	old_info.word_count = newwr.count;
	if (newwr.count > 1) {
	  while (asize < old_info.word_count)
	    asize <<= 1;
	  old_info.indexpos = newwr.pos = idx.alloc(asize);
	}
      } else {
	// Find the old allocation size:
	while (asize < old_info.word_count)
	  asize <<= 1;
	if ( newwr.count + old_info.word_count > asize ) {
	  // Deallocate ....
	  CPointer oldpos = old_info.indexpos;
	  idx.dealloc(asize, old_info.indexpos);
	  while ( newwr.count + old_info.word_count > asize )
	    asize <<= 1;
	  old_info.indexpos = idx.alloc(asize);
	  idx.copy_idx(oldpos, old_info.word_count, old_info.indexpos);
	  newwr.pos = old_info.indexpos + old_info.word_count;
	  old_info.word_count += newwr.count;
	  //std::cout << " realloc " << asize << " at " << wr.pos << std::endl;
	} else  {
	  newwr.pos = old_info.indexpos + old_info.word_count;
	  old_info.word_count += newwr.count;
	}
      }
      if (old_info.word_count > 1) {
	_infodb.set_info(newwr.word_no, old_info);
	newwr.count = old_info.word_count;
      }
      //std::cerr << "SET_INFO w=" << word << ", wno=" << newwr.word_no << ", count=" << old_info.word_count << ", ipos=" << old_info.indexpos << std::endl;
    }

  }

  // Now in Windex, for each word the pos points to next free index position -
  // unless pos is 0, in which case there will be only one occurrence and pos must be stored directly

  std::cerr << "Pass 3: saving index..." << std::endl;
  next_log = curr_pos + log_size;
  log_percent = 0;

  corpus_fd.clear();
  corpus_fd.seekg(curr_pos, corpus_fd.beg);
  std::string line;
  while ( getline(corpus_fd, line) ) {
    //std::cerr << "Pos " << curr_pos << " Line: <" << line << ">" << std::endl;
    std::istringstream theLine(line);
    while ( theLine >> word ) {
      Windex::iterator wp = wcount.find(word);
      if (wp == wcount.end())
	continue;
      Wrecord &newwr = wp->second;
      if (newwr.count == 1) {
	// Only occurence, store corpus position directly:
	if (newwr.word_no) {
	  _infodb.set_info(newwr.word_no, StringInfo(curr_pos, 1));
	} else {
	  unsigned int sno = string_db.add(word.c_str());
	  _infodb.add(sno, StringInfo(curr_pos, 1));
	}
      } else {
	set_idx(newwr.pos, curr_pos);
	++newwr.pos;
      }
      //std::cerr << '[' << word << --newwr.count << "] ";
    }
    //std::cerr << std::endl;
    curr_pos = corpus_fd.tellg();
    if (curr_pos >= next_log){
      std::cerr << "" << ++log_percent << "% ";
      next_log += log_size;
    }
  }

  std::cerr << "\nDone!" << std::endl;
  corpus_fd.clear();
}
