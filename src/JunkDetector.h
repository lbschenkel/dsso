#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <set>

#include <hunspell/hunspell.hxx>
class HunInfoDB;

typedef std::set<std::string> WordSet;

class JunkDetector {
public:
  JunkDetector(std::string name, const char *dict_filename, const char *aff_name = 0, const char *dic_name = 0);
  
  // Returns probability of the word belonging to this category:
  virtual float word_score(const std::string &word);
  virtual float check(const std::string &word, float weight = 1.0);
  std::string name() {
    return detector_name;
  }
  float score() {
    return sentence_score;
  }
  void reset() {
    sentence_score = 0.0;
  }
  // Returns false if the given line probably does not belong to this category:
  virtual bool detect(const WordSet &ok_words, const WordSet &bad_words,
		      const WordSet &bad_names, const std::string &line);
protected:
  virtual void read_dict(const char *filename);
  float sentence_score;
private:
  std::string detector_name;
  std::map<std::string, float> dict;
  Hunspell *hundict;
};

class NoDotsDetector : public JunkDetector {
public:
  NoDotsDetector(std::string name, const char *dict_filename) : JunkDetector(name, dict_filename) {
  }
  virtual bool detect(const WordSet &ok_words, const WordSet &bad_words,
		      const WordSet &bad_names, const std::string &line);
 private:
};


class Detector {
public:
  Detector(HunInfoDB *hidb);
  void add_detector(JunkDetector *jd) {
    detector.push_back(jd);
  }
  void add_detector(std::string name, const char *dict_filename, const char *aff_name, const char *dic_name);
  bool check_line(std::string line);
private:
  HunInfoDB *huninfodb;
  std::vector<JunkDetector *> detector;
};
