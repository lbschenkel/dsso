#include <string.h>
#include <fstream>
#include "JunkDetector.h"
#include "HunInfoDB.h"
#include <glibmm/ustring.h>

void JunkDetector::read_dict(const char *filename) {
  std::ifstream fd(filename);
  if (!fd) {
    std::cerr << "cannot open file " << filename << ": " << strerror(errno) << std::endl;
  }
  std::string word;
  while (fd >> word) {
    dict[word] = 1.0;
  }
}

JunkDetector::JunkDetector(std::string name, const char *dict_filename, const char *aff_name, const char *dic_name) :
    sentence_score(0.0),
    detector_name(name)
{
  if (dict_filename)
    read_dict(dict_filename);
  if (aff_name and dic_name) {
    hundict = new Hunspell(aff_name, dic_name);
  } else {
    hundict = 0;
  }
}

// Returns probability of the word belonging to this category:
float JunkDetector::word_score(const std::string &word) {
  if (dict.find(word) != dict.end())
    return 1.0;
  if (hundict and hundict->spell(word.c_str()))
    return 0.9;
  return 0.0;
}

float JunkDetector::check(const std::string &word, float weight) {
  float s = word_score(word);
  sentence_score += s*weight;
  return s;
}

bool JunkDetector::detect(const WordSet &ok_words, const WordSet &bad_words,
			  const WordSet &bad_names, const std::string &line) {

  reset();

  for (WordSet::const_iterator it=bad_words.begin(); it!=bad_words.end(); ++it)
    check(*it);

  if (score() < bad_words.size()*0.5)
    return false;

  for (WordSet::const_iterator it=ok_words.begin(); it!=ok_words.end(); ++it)
    check(*it);

  for (WordSet::const_iterator it=bad_names.begin(); it!=bad_names.end(); ++it)
    check(*it, 0.5);

  return (score() > ok_words.size());
}

bool NoDotsDetector::detect(const WordSet &ok_words, const WordSet &bad_words,
			    const WordSet &bad_names, const std::string &line) {
  if (line.find_first_of("åäö") != std::string::npos)
    return false;

  for (WordSet::const_iterator it=bad_words.begin(); it!=bad_words.end(); ++it)
    check(*it);

  return (score() > bad_words.size()/2);
}

Detector::Detector(HunInfoDB *hidb)
  : huninfodb(hidb)
{  
  add_detector(new NoDotsDetector("NoDots", "resources/nodots.txt"));
  add_detector("Danska", (const char *)0, "resources/da_DK.aff", "resources/da_DK.dic");
  add_detector("Norska", (const char *)0, "resources/nn_NO.aff", "resources/nn_NO.dic");
  add_detector("Bokmål", (const char *)0, "resources/nb_NO.aff", "resources/nb_NO.dic");
  add_detector("Engelska", (const char *)0, "resources/en_US.aff", "resources/en_US.dic");
  add_detector("Tyska", (const char *)0, "resources/de_DE.aff", "resources/de_DE.dic");  
  add_detector("Latin", (const char *)0, "resources/la.aff", "resources/la.dic");  
}

void Detector::add_detector(std::string name, const char *dict_filename, const char *aff_name, const char *dic_name) {
  add_detector(new JunkDetector(name, dict_filename, aff_name, dic_name));
}

bool Detector::check_line(std::string line) {
  std::set<std::string> bad_words;
  std::set<std::string> bad_names;
  std::set<std::string> ok_words;
  size_t old_pos = 0, pos;
  while (true) {
    pos = line.find_first_of(" \t", old_pos);
    if (pos == std::string::npos)
      break;
    if (line[pos] == ' ') {
      std::string word = line.substr(old_pos, pos-old_pos);
      if (huninfodb->check_word(word.c_str())) {
	ok_words.insert(word);      
      } else {
	Glib::ustring w(word);
	if (g_unichar_isupper(*w.begin()))
	  bad_names.insert(word);
	else
	  bad_words.insert(word);
      }
    }
    old_pos = pos+1;
  }
  if (!ok_words.size() and !bad_words.size())
    return false; // No words at all
  if (bad_words.size() <= 1)
    return true; // At most one misspelled word
  if (bad_words.size()+bad_names.size()/2 > (ok_words.size()+2)/3)
    return false; // 25% misspelled
  // Less than 25% misspelled, keep it unless we find a
  // detector matching it

  for (int i=0; i<detector.size(); ++i)
    if (detector[i]->detect(ok_words, bad_words, bad_names,  line))
      return false;
  return true;
}
