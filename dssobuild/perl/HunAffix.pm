package HunAffix;

use utf8;
use strict;
use warnings;
use SpellSet;

# Attributes:
#  SFX => Hash; key is flag character,
#         value list of triplets:
#         chars to strip, chars to add, condition
#         first element is Y or N, the combinepermitflag.
#  CSFX => As above, but for flags generating non-words
#          that may appear first in a compound
#  CSFXFLAGS => Hash; keys are the second-level suffix flags.
#               Note: this is for reference only.
#               Two-fold suffix handling is not implemented yet.

# Value: 0 for single valued parameter,
# >0 for  multi-line definitions, the value
# shall be the number of parameters in the first line.
my %KEYWORD = (
    KEY => \&parse_string,
    TRY => \&parse_string,
    SET => \&parse_string,
    COMPLEXPREFIXES => \&parse_number,
    COMPOUNDFLAG => \&parse_flag,
    COMPOUNDBEGIN  => \&parse_flag,
    COMPOUNDMIDDLE => \&parse_flag,
    COMPOUNDEND  => \&parse_flag,
    COMPOUNDWORDMAX  => \&parse_number,
    COMPOUNDROOT => \&parse_flag,
    COMPOUNDPERMITFLAG  => \&parse_flag,
    COMPOUNDFORBIDFLAG => \&parse_flag,
    CHECKCOMPOUNDDUP => \&boolean_keyword,
    CHECKCOMPOUNDREP => \&boolean_keyword,
    CHECKCOMPOUNDTRIPLE => \&boolean_keyword,
    SIMPLIFIEDTRIPLE => \&boolean_keyword,
    CHECKCOMPOUNDCASE => \&boolean_keyword,
    ONLYMAXDIFF => \&boolean_keyword,
    MAXDIFF  => \&parse_number,
    NOSUGGEST => \&parse_flag,
    FORBIDDENWORD => \&parse_flag,
    LEMMA_PRESENT => \&parse_flag,
    CIRCUMFIX => \&parse_flag,
    FULLSTRIP => \&boolean_keyword,
    MAXCPDSUGS => \&parse_number,
    ONLYINCOMPOUND  => \&parse_flag,
    PSEUDOROOT => \&parse_flag,
    NEEDAFFIX => \&parse_flag,
    COMPOUNDMIN  => \&parse_number,
    COMPOUNDSYLLABLE => \&not_implemented,
    SYLLABLENUM => \&parse_string,
    CHECKNUM => \&boolean_keyword,
    WORDCHARS  => \&parse_string,
    IGNORE => \&parse_string,
    REP => \&not_implemented,
    PHONE => \&not_implemented,
    CHECKCOMPOUNDPATTERN => \&not_implemented,
    COMPOUNDRULE => \&not_implemented,
    MAP => \&not_implemented,
    FORCEUCASE => \&parse_flag,
    BREAK => \&not_implemented,
    LANG => \&parse_string,
    VERSION => \&parse_string,
    MAXNGRAMSUGS => \&parse_number,
    NOSPLITSUGS => \&boolean_keyword,
    SUGSWITHDOTS => \&boolean_keyword,
    KEEPCASE => \&parse_flag,
    SUBSTANDARD => \&parse_flag,
    CHECKSHARPS => \&boolean_keyword,
    SFX => \&parse_sfx,
    PFX => \&not_implemented,
);

sub boolean_keyword {
    my ($self, $keyword) = @_;
    if (@_ != 2) {
        die("wrong number of parameters");
    }
    $self->{$keyword} = 1;
    return 0;
}

sub parse_sfx {
    my $self = shift;
    my $keyword = shift;
    if (@_ == 3) {
        my ($flag, $combinepermit, $lines) = @_;
        die("SFX @_: syntax error")
            unless $combinepermit  =~ /^[YNyn]$/ && $lines =~/^\d+$/;
        $self->{SFX}{$flag} = [ uc($combinepermit) ];
        return $lines;
    } elsif (@_ == 4) {
        my ($flag, $remove, $add, $cond) = @_;
        if ($remove eq "0") {
            $remove = "";
        }
        if (substr($add,0,1) eq "0") {
            substr($add,0,1) = "";
        }
	if (index($add, "/") >= 0) {
	    my ($realadd, $cflags) = split(/\//, $add);
	    undef($self->{CSFXFLAGS}{$cflags});
	    if (exists($self->{CSFX}{$flag})) {
		push(@{$self->{CSFX}{$flag}}, $remove, $realadd, $cond);
	    } elsif (exists($self->{CMSFX}{$flag})) {
		push(@{$self->{CMSFX}{$flag}}, $remove, $realadd, $cond)
		    unless $realadd =~ /-/;
	    } else {
		if (exists($self->{SFX}{$flag}) &&
		    @{$self->{SFX}{$flag}} == 1) {
		    if (index($cflags, $self->{COMPOUNDBEGIN}) >= 0) {
			$self->{CSFX}{$flag} = $self->{SFX}{$flag};
			push(@{$self->{CSFX}{$flag}}, $remove, $realadd, $cond);
		    } elsif (index($cflags, $self->{COMPOUNDMIDDLE}) >= 0) {
			$self->{CMSFX}{$flag} = $self->{SFX}{$flag};
			push(@{$self->{CMSFX}{$flag}}, $remove, $realadd, $cond);
		    }
		    delete($self->{SFX}{$flag});
		} else {
		    die("subflags not implemented");
		}
	    }
	    # TODO: Handle subflags
	} else {
	    if (exists($self->{SFX}{$flag})) {
		push(@{$self->{SFX}{$flag}}, $remove, $add, $cond);
	    } else {
		die("SFX @_: unexpected") unless
		    (exists($self->{CSFX}{$flag}) or exists($self->{CMSFX}{$flag}));
	    }
	}
    } else {
        die("SFX @_: wrong number of parameters");
    }
}

sub dump {
    my ($self) = @_;
    print("Compound flags: ", keys(%{$self->{CSFXFLAGS}}), "\n");
    my $h1 = $self->{CSFX};
    foreach my $flag (keys %$h1) {
	my $l = join(".", @{$h1->{$flag}});
	print("Cflag $flag: $l\n");
    }
    my $h2 = $self->{CMSFX};
    foreach my $flag (keys %$h2) {
	my $l = join(".", @{$h2->{$flag}});
	print("CMflag $flag: $l\n");
    }
}

sub no_merge_flags {
    my ($self) = @_;
    if (exists($self->{NO_MERGE_FLAGS})) {
	return $self->{NO_MERGE_FLAGS};
    } else {
	return "";
    }
}

sub check_is_flag {
}

sub parse_flag {
    my ($self, $keyword, $flag) = @_;
    if (@_ != 3) {
        die("wrong number of parameters");
    }
    check_is_flag($flag);
    $self->{$keyword} = $flag;
    return 0;
}

sub parse_number {
    my ($self, $keyword, $number) = @_;
    if (@_ != 3) {
        die("wrong number of parameters");
    }
    $self->{$keyword} = $number;
    return 0;
}


sub parse_string {
    my ($self, $keyword, $string) = @_;
    if (@_ != 3) {
        die("wrong number of parameters");
    }
    $self->{$keyword} = $string;
    return 0;
}

sub not_implemented {
    pop;
}

# TODO: skriv merge av en dict till en annan,
# med parameter kan man ange ifall den ena eller
# ANDRA HAR NÅGON INKOMPATIBEL flagga

sub new {
    my ($pkg, $affixfilename) = @_;
    defined($affixfilename) or
        die("no affixfile");
    open(my $affixes, "<:encoding(utf-8)", $affixfilename)
        or die("$affixfilename: cannot open: $!");
    my $self = { };
    {
        my $no_lines = 0;
        my $old_command;
	my $pragma_ignore = 0;
        while (my $line = <$affixes>) {
	    if ($pragma_ignore) {
		if ($line =~ /^\s*#\s*pragma\s+ignore-end/) {
		    $pragma_ignore = 0;
		}
		next;
	    }
	    if ($line =~ /^\s*#\s*pragma\s+ignore-start/) {
		$pragma_ignore = 1;
		next;
	    }
	    if ($line =~ /^\s*#\s*pragma\s+no-merge\s+(\S+)/) {
		$self->{NO_MERGE_FLAGS} = ("" . $1);
		next;
	    }
            next if $line =~ /^\s*#/;
            chomp($line);
            my @cmdline = split(" ", $line)
                or next;
            my $command = shift(@cmdline);
            die("$command: unknown keyword")
                unless exists($KEYWORD{$command});
            if ($no_lines > 0) {
                --$no_lines;
                die("$line: expected $old_command")
                    unless $command  eq $old_command;
                &{$KEYWORD{$command}}($self, $command, @cmdline);
            } elsif (ref($KEYWORD{$command})) {
                $old_command = $command;
                $no_lines = &{$KEYWORD{$command}}($self, $command, @cmdline);
            }
        }
        die("$affixfilename: unexpected end of file")
            if $no_lines;
    }
    bless($self, $pkg);
    return $self;
}

# Add a flag to every word of dictfile.
# Second parameter may be either an explicit flag,
# or a keyword used to define a flag in the affixfile.
sub addflag {
    my ($self, $roothash, $flag) = @_;
    return unless keys(%$roothash);
    if (exists($KEYWORD{$flag})) {
        die("$flag: not defined") unless exists($self->{$flag});
        $flag = $self->{$flag}
    } else {
        check_is_flag($flag);
    }

    foreach my $word (keys(%$roothash)) {
        if (defined($roothash->{$word})) {
            $roothash->{$word} .= $flag;
        } else {
            $roothash->{$word} = $flag;
        }
    }
}

sub compile {
    my ($self) = @_;
    # Map tail to hash of flags to list of pairs: no. chars to cut, and new tail
    my @rulemap;
    my @watch_chars;
    my @weight;
    {
	my $suffix_hash = $self->{SFX};
	# Pass 1: Find out what letters at the end
	#         that we have to check
	while (my ($flag, $listref) = each(%$suffix_hash)) {
	    my @rules = @$listref;
	    shift(@rules);
	    while (my ($remove, $add, $cond) = splice(@rules, 0, 3)) {
		next if $cond eq ".";
		my @conds = reverse($cond =~ m/\[.*?\]|./g);
		for (my $i=0; $i < @conds; ++$i) {
		    if ($conds[$i] =~ m/\[\^(.*)\]/) {
			$watch_chars[$i] .= $1;
		    } elsif ($conds[$i] =~ m/\[(.*)\]/) {
			$watch_chars[$i] .= $1;
		    } else {
			$watch_chars[$i] .= $conds[$i];
		    }
		}
	    }
	}

	foreach my $string (@watch_chars) {
	    my %chars;
	    undef($chars{$_}) foreach split("", $string);
	    # Must put the dot first; we assume as much in sub tailno.
	    $string = "." . join("", keys(%chars));
	}
	{
	    my $w=1;
	    foreach (my $i=$#watch_chars; $i >= 0; --$i) {
		$weight[$i] = $w;
		$w *= length($watch_chars[$i]);
	    }
	}

	while (my ($flag, $listref) = each(%$suffix_hash)) {
	    my @rules = @$listref;
	    shift(@rules);
	    my $w;
	    while (my ($remove, $add, $cond) = splice(@rules, 0, 3)) {
		# For the flag to work on a word, the word must have at
		# least as many characters as indicated by the condition.
		# The word must also be longer than $remove unless the
		# option FULLSTRIP is set.

		my @conds = ();
		if ($cond ne ".") {
		    @conds = reverse(my @l = $cond =~ /\[.*?\]|./g);
		}

		my $minlength = @conds;
		if (length($remove) == $minlength and !exists($self->{FULLSTRIP})) {
		    ++$minlength;
		}

		my $index = -1;
		foreach my $c (@conds) {
		    ++$index;
		    if (substr($c, 0, 1) eq "[") {
			if (substr($c, 1, 1) eq "^") {
			    my $all = $watch_chars[$index];
			    foreach my $ltr (split("", substr($c, 2, -1))) {
				substr($all, index($all, $ltr), 1) = "";
			    }
			    $c = $all;
			} else {
			    $c = substr($c, 1, -1);
			}
		    }
		    $c = [ map { index($watch_chars[$index], $_) * $weight[$index] } split("", $c) ];
		}
		my @comb;
		push(@comb, 0) foreach (@conds);
		my @dmbcomb;
		push(@dmbcomb, scalar(@$_)) foreach (@conds);
#		unless (defined($watch_chars[scalar(@comb))) {
#		    print STDERR "BUUUG! Kombi=<@comb>  WC=<@watch_chars> \n";
#		    
#		    exit(1);
#		}
		my $remainder;
		if (@comb) {
		    $remainder = $weight[$#comb];
		} else {
		    $remainder = $weight[0]*length($watch_chars[0]);
		}
	      COMB:
		while (1) {
		    my $no = 0;
		    $no += $conds[$_][$comb[$_]] foreach (0..$#conds);
		    #print STDERR ("Cond=<$cond> Kombi=<@comb> NO=$no Conds=<@dmbcomb>\n");
		    foreach my $tailno ($no..$no+$remainder-1) {
			push(@{$rulemap[$tailno]{$flag}},
			     -length($remove), $add, $minlength);
		    }
		    last COMB unless @comb;
		    foreach my $i (0..$#comb) {
			++$comb[$i];
			last if $comb[$i] < @{$conds[$i]};
			last COMB if $i == $#comb;
			$comb[$i]=0;
		    }
		}
	    }
	}
    }
    unshift(@watch_chars, "");
    $self->{SFX_RULEMAP} = \@rulemap;
    $self->{SFX_WATCH_CHARS} = \@watch_chars;
    $self->{SFX_WEIGHT} = \@weight;
}

sub tailno {
    my ($self, $word) = @_;
    my $wchars = $self->{SFX_WATCH_CHARS};
    my $weight = $self->{SFX_WEIGHT};
    my $no = 0;
    my $pos = $#{$wchars};
    $pos = length($word) if length($word)<$pos;
    foreach my $p (1..$pos) {
	my $i = index($wchars->[$p], substr($word, -$p, 1));
	if ($i > 0) {
	    $no += $weight->[$p-1]*$i;
	}
#	print STDERR ("C=<",substr($word, -$p, 1),"> w=",$weight->[$p-1]*$i,"\n");
    }
    return $no;
}

# Add as many flags as possible to every word.
# This will be useful in "munch".
sub maximum_flags {
    my ($self, $wordhash) = @_;
    unless (exists($self->{SFX_RULEMAP})) {
	foreach my $word (keys(%$wordhash)) {
	    $wordhash->{$word} = $self->findflags($wordhash, $word);
	}
	return;
    }
    # Map tail to hash of flags to list of pairs: no chars to cut, and new tail
    my $rulemap = $self->{SFX_RULEMAP};

    foreach my $word (keys(%$wordhash)) {
	my $tailno = $self->tailno($word);
      FLAG:
	while (my ($flag, $listref) = each(%{$rulemap->[$tailno]})) {
	    for (my $i=0; $i<@$listref; $i+=3) {
		next FLAG if length($word) < $listref->[$i+2];

		substr(my $w = $word, $listref->[$i] || length($word)) =
		    $listref->[$i+1];
		next FLAG unless exists($wordhash->{$w});
	    }
	    $wordhash->{$word} .= $flag;
	}
    }
}

sub count_flags {
    my ($self, $dict) = @_;
    my %count;
    foreach my $word (keys(%$dict)) {
	my $flags = $dict->{$word};
	next unless defined($flags);
	foreach my $flag ( split("", $flags) ) {
	    ++$count{$flag};
	}
    }
    return \%count;
}

# Given a dict and a word, find all suffix flags
# that may be applied to the word.
sub findflags {
    my ($self, $roothash, $word) = @_;
    my $flags;
    my $suffix_hash = $self->{SFX};
  SUFFIX:
    while (my ($flag, $listref) = each(%$suffix_hash)) {
        my @rules = @$listref;
        shift(@rules);
        my $w;
        while (my ($remove, $add, $cond) = splice(@rules, 0, 3)) {
            next unless $word =~ m/$cond$/;
	    next if length($remove) == length($word); # According to hunspell!!
            my $pos = length($remove) ? -length($remove) : length($word);
            substr($w = $word, $pos) = $add;
            next SUFFIX unless exists($roothash->{$w});
        }
        $flags .= $flag if defined($w);
    }
    return $flags;
}

sub reduce_flags_comp {
    my ($self, $dict, $optional) = @_;

    my $rulemap = $self->{SFX_RULEMAP};
    my %wordformset;

    if (ref($optional)) {
	foreach my $w (keys(%$optional)) {
	    $wordformset{$w} = 1;
	}
    }

    # Pass 1: count all generated wordforms 
    my $suffix_hash = $self->{SFX};
    foreach my $word (keys(%$dict)) {
	++$wordformset{$word};
	my $flags = $dict->{$word};
	next unless defined($flags);
	next if (exists($self->{ONLYINCOMPOUND}) and $flags =~ m/$self->{ONLYINCOMPOUND}/);
	my $tailno = $self->tailno($word);
	foreach my $flag ( split("", $flags) ) {
	    next unless exists($suffix_hash->{$flag});

	    my $listref = $rulemap->[$tailno]{$flag};
	    for (my $i=0; $i<@$listref; $i+=3) {
		unless (length($word) < $listref->[$i+2]) {
		    substr(my $w = $word, $listref->[$i] || length($word)) =
			$listref->[$i+1];
		    ++$wordformset{$w};
		}
	    }
	}
    }

    # Pass 2: Remove redundant flags.
    # Will first remove flags that contribute one wordform, then those
    # that contribute two and so on.
    my @remember;
    foreach my $word (keys(%$dict)) {
	my $flags = $dict->{$word};
	next unless defined($flags);
	next if (exists($self->{ONLYINCOMPOUND}) and $flags =~ m/$self->{ONLYINCOMPOUND}/);
	my $tailno = $self->tailno($word);
      FLAG:
	foreach my $flag ( split("", $flags) ) {
	    next unless exists($suffix_hash->{$flag});

	    my %generated_wfs;

	    {
		my $listref = $rulemap->[$tailno]{$flag};
		for (my $i=0; $i<@$listref; $i+=3) {
		    unless (length($word) < $listref->[$i+2]) {
			substr(my $w = $word, $listref->[$i] || length($word)) =
			    $listref->[$i+1];
			next FLAG if $wordformset{$w} == 1;
			undef($generated_wfs{$w});
		    }
		}
	    }

	    # OK, we survived the above loop. This flag is redundant.
	    # If it only contributes one wordform, remove it at once:
	    if (keys(%generated_wfs) == 1 ) {
		if ($flag eq $dict->{$word}) {
		    undef($dict->{$word});
		} else {
		    $dict->{$word} =~ s/$flag//;
		}
		# Remove the wordforms counted by this flag.
		foreach my $w (keys(%generated_wfs)) {
		    --$wordformset{$w};
		}
	    } else {
		# Remember it, check later if it can be removed:
		$remember[keys(%generated_wfs)]{$flag . $word} = \%generated_wfs;
	    }
	}
    }
    while (@remember) {
	my $ref = shift(@remember) or next;
      CHECK_FLAG:
	while (my ($string, $generated) = each(%$ref)) {
	    foreach my $w (keys(%$generated)) {
		next CHECK_FLAG unless($wordformset{$w} > 1);
	    }
	    # OK, let's remove this flag:
	    my $flag = substr($string, 0, 1);
	    my $word = substr($string, 1);
	    foreach my $w (keys(%$generated)) {
		--$wordformset{$w};
	    }
	    if ($flag eq $dict->{$word}) {
		undef($dict->{$word});
	    } else {
		$dict->{$word} =~ s/$flag//;
	    }
	}
    }
}

# Remove superfluous suffix flags
sub reduce_flags {
    my ($self, $dict, $optional) = @_;
    if (exists($self->{SFX_RULEMAP})) {
	return $self->reduce_flags_comp($dict, $optional);
    }
    my %wordformset;
    # Pass 1: count all generated wordforms 
    my $suffix_hash = $self->{SFX};
    foreach my $word (keys(%$dict)) {
	++$wordformset{$word};
	my $flags = $dict->{$word};
	next unless defined($flags);
	next if (exists($self->{ONLYINCOMPOUND}) and $flags =~ m/$self->{ONLYINCOMPOUND}/);
	foreach my $flag ( split("", $flags) ) {
	    next unless exists($suffix_hash->{$flag});
	    my @rules = @{$suffix_hash->{$flag}};
	    shift(@rules);
	    while (my ($remove, $add, $cond) = splice(@rules, 0, 3)) {
		next unless $word =~ m/$cond$/;
		my $pos = length($remove) ? -length($remove) : length($word);
		substr(my $w = $word, $pos) = $add;
		++$wordformset{$w};
	    }
	}
    }
    # Pass 2: Remove redundant flags (trivial, non-optimal algorithm)
    foreach my $word (keys(%$dict)) {
	my $flags = $dict->{$word};
	next unless defined($flags);
	next if (exists($self->{ONLYINCOMPOUND}) and $flags =~ m/$self->{ONLYINCOMPOUND}/);
      FLAG:
	foreach my $flag ( split("", $flags) ) {
	    next unless exists($suffix_hash->{$flag});
	    my @rules = @{$suffix_hash->{$flag}};
	    shift(@rules);
	    while (my ($remove, $add, $cond) = splice(@rules, 0, 3)) {
		next unless $word =~ m/$cond$/;
		my $pos = length($remove) ? -length($remove) : length($word);
		substr(my $w = $word, $pos) = $add;
		next FLAG if $wordformset{$w} == 1;
	    }
	    # OK, we survived the above loop. This flag is redundant.
	    if ($flag eq $dict->{$word}) {
		undef($dict->{$word});
	    } else {
		$dict->{$word} =~ s/$flag//;
	    }
	    # Remove the wordforms counted by this flag.
	    @rules = @{$suffix_hash->{$flag}};
	    shift(@rules);
	    while (my ($remove, $add, $cond) = splice(@rules, 0, 3)) {
		next unless $word =~ m/$cond$/;
		my $pos = length($remove) ? -length($remove) : length($word);
		substr(my $w = $word, $pos) = $add;
		--$wordformset{$w};
	    }
	}
    }
}

sub expand_c {
    my ($self, $word, $flags) = @_;
    my %wordformset = ( );
    my $suffix_hash = $self->{CSFX};
    foreach my $flag ( split("", $flags) ) {
	undef($wordformset{$word}), next unless exists($suffix_hash->{$flag});
	my @rules = @{$suffix_hash->{$flag}};
	#shift(@rules);
	while (my ($remove, $add, $cond) = splice(@rules, 0, 3)) {
	    #print("@rules: $remove, $add, $cond\n");
	    next unless $word =~ m/$cond$/;
	    my $pos = length($remove) ? -length($remove) : length($word);
	    substr(my $w = $word, $pos) = $add;
	    undef($wordformset{$w});
	}
    }
    return keys(%wordformset);
}

sub expand {
    my ($self, $word, $flags) = @_;
    return ($word) unless defined($flags);
    my %wordformset = ( $word => undef );
    if (exists($self->{SFX_RULEMAP})) {

	my $tailno = $self->tailno($word);
	my $rulemap = $self->{SFX_RULEMAP};
	foreach my $flag ( split("", $flags) ) {
	    my $listref = $rulemap->[$tailno]{$flag};
	    for (my $i=0; $i<@$listref; $i+=3) {
		unless (length($word) < $listref->[$i+2]) {
		    substr(my $w = $word, $listref->[$i] || length($word)) =
			$listref->[$i+1];
		    undef($wordformset{$w});
		}
	    }
	}
    } else {
	 
	my $suffix_hash = $self->{SFX};
	foreach my $flag ( split("", $flags) ) {
	    next unless exists($suffix_hash->{$flag});
	    my @rules = @{$suffix_hash->{$flag}};
	    shift(@rules);
	    while (my ($remove, $add, $cond) = splice(@rules, 0, 3)) {
		next unless $word =~ m/$cond$/;
		my $pos = length($remove) ? -length($remove) : length($word);
		substr(my $w = $word, $pos) = $add;
		undef($wordformset{$w});
	    }
	}

    }
    return keys(%wordformset);
}

sub expand_dict {
    my ($self, $dict) = @_;
    my %wordformset;
    my $suffix_hash = $self->{SFX};
    while (my ($word, $flags) = each(%$dict)) {
        undef($wordformset{$word});
        next unless defined($flags);
	foreach my $flag ( split("", $flags) ) {
	    next unless exists($suffix_hash->{$flag});
	    my @rules = @{$suffix_hash->{$flag}};
	    shift(@rules);
	    while (my ($remove, $add, $cond) = splice(@rules, 0, 3)) {
		next unless $word =~ m/$cond$/;
		my $pos = length($remove) ? -length($remove) : length($word);
		substr(my $w = $word, $pos) = $add;
		undef($wordformset{$w});
	    }
	}
    }
    \%wordformset;
}


sub dict_stats {
    my ($self, $dict) = @_;
    my $suffix_hash = $self->{SFX};
    my %stats;
    while (my ($word, $flags) = each(%$dict)) {
	next unless defined($flags);
	foreach my $flag ( split("", $flags) ) {
            next unless exists($suffix_hash->{$flag});
            my @rules = @{$suffix_hash->{$flag}};
            shift(@rules);
            while (my ($remove, $add, $cond) = splice(@rules, 0, 3)) {
                next unless $word =~ m/$cond$/;
                ++$stats{$flag . (@rules/3)};
            }
        }
    }
    foreach my $flag (sort(keys(%$suffix_hash))) {
        my @rules = @{$suffix_hash->{$flag}};
        shift(@rules);
        while (my ($remove, $add, $cond) = splice(@rules, 0, 3)) {
            printf("%4d Flag%3s -$remove+$add $cond\n",
                   $stats{$flag . (@rules/3)}, $flag);
        }
    }
}

# Given a dict and a prefix word, find a dictionary word and
# compounding suffix that result in that word.
sub findcsuffix {
    my ($self, $wordhash, $word, $type) = @_;
    my $ftype = ($type ne "MIDDLE") ? "CSFX" : "CMSFX"; 
    defined($word) or
        die("word required");
    my $suffix_hash = $self->{$ftype};
    my @results;
    while (my ($flag, $listref) = each(%$suffix_hash)) {
        my @rules = @$listref;
        shift(@rules);
       while (my ($removed, $added, $cond) = splice(@rules, 0, 3)) {
	   #print STDERR "Testar $word: -$added,$removed ($cond)";
            my $pos = length($added) ? -length($added) : length($word);
            if (substr($word,$pos) eq $added) {
		my $w = $word;
                substr($w, $pos) = $removed;
		#print STDERR "  --> ger $w\n";
                if (exists($wordhash->{$w}) and $w =~ m/$cond$/) {
                    push(@results, $w, $flag);
                }
#            } else {
#		print("Funkar ej <", substr($word,$pos), "> ej <",$added,">\n");
	    }
        }
    }
    #keys(%$suffix_hash);
    return @results;
}


# Create and return a compressed HunDict object
# Parameters:
#   $orig_dict      - hashref, keys are the dictionary words
#                     (could be a HunDict object.)
#   $optional_words - hashref, keys are words that are
#                     optional, i.e. they are valid words
#                     but we do not have to keep them since
#                     they will also be stored somewhere else
#                     (e.g. through compound rules.)
sub munch {
    my ($affixfile, $orig_dict, $optional_words) = @_;

    # The keys of $wordhash are all the words we have to store.
    # Whenever a word is "done", it will be removed from $wordhash.
    my $wordhash = $affixfile->expand_dict($orig_dict);

    $optional_words = {} unless ref($optional_words);

    # This will be the result:
    my $dict = new HunDict();

    # Pass 0: For each word, find all valid suffix flags and all
    #         possible roots. Store in %flags and %roots.

    # %flags - dictionary word is key, string of flags is value.
    my %flags = %$wordhash;
    print STDERR "Pos 1: Find flags ",time, "\n";
    $affixfile->maximum_flags(\%flags);
    my @l = $affixfile->expand("and", $flags{and});
    print STDERR "Pos 2: Find roots ",time, "\n";

    # %roots - dictionary word is key; value is a ref to a hash,
    #          the keys of which are all possible roots of the word.
    my %roots;

    foreach my $word (keys(%$wordhash)) {
#       The below call is a simpler, but much slower, alternative
#       to the maximum_flags call above:
#	$flags{$word} = $affixfile->findflags($wordhash, $word);

	my @l = $affixfile->expand($word, $flags{$word});
	# Remember all possible roots of this word:
	foreach my $wf (@l) {
	    undef($roots{$wf}{$word})
		unless $wf eq $word;
	}
    }

    # Now check which words are optional:
    foreach my $w (keys(%$optional_words)) {
	delete($wordhash->{$w});
    }

    print STDERR "Pos 3: Build ",time, "\n";

    # Pass 2: The "low hanging fruit".
    #   First, if a wordform cannot
    #   be generated from another word+flag,
    #   we must keep it.
    #   Second, if there is only one other
    #   word+flag that can generate it and
    #   the word itself cannot be suffixed,
    #   keep the other word.
    {
	my %secondary;
	foreach my $wf (keys(%$wordhash)) {
	    if (!exists( $roots{$wf} )) {
		$dict->{$wf} = $flags{$wf};
		my @l = $affixfile->expand($wf, $flags{$wf});
		foreach my $w (@l) {
		    delete($wordhash->{$w});
		}
	    } elsif (scalar(keys(%{$roots{$wf}})) == 1 and
		     exists($wordhash->{$wf}) and
		     !defined($flags{$wf})) {
		my ($w) = keys(%{$roots{$wf}});
		$secondary{$w}=$wf;
	    }
	}

	foreach my $wf (keys(%secondary)) {
	    next if exists($dict->{$wf});
	    #next unless exists($wordhash->{$secondary{$wf}});
	    my @l = $affixfile->expand($wf, $flags{$wf});
	    my $no_hits = 0;
	    foreach my $w (@l) {
		if (exists($wordhash->{$w})) {
		    delete($wordhash->{$w});
		    ++$no_hits;
		}
	    }
	    # Check that $wf really
	    # contributes more than $secondary{$wf}...
	    if ($no_hits > 1) {
		$dict->{$wf} = $flags{$wf};
	    } else {
		undef($dict->{$secondary{$wf}});
	    }
	}
    }


    # Pass 3: Given an inflection, if one of the
    # possible roots gives the same (or more) inflections as
    # all the other roots together, then keep the former.
  WORD:
    foreach my $word (keys(%$wordhash)) {
        next unless exists($wordhash->{$word});
        my %inflections;
        my $best_root;
        my $best_count = 0;
        foreach my $root ($word, keys(%{$roots{$word}})) {
            my @l = $affixfile->expand($root, $flags{$root});

            my $count = 0;
            foreach my $wf (@l) {
                # Ignore inflections that already exist
                if (exists($wordhash->{$wf})) {
                    ++$count;
                    undef($inflections{$wf});
                }
            }
            if (keys(%inflections) > $best_count or
		(keys(%inflections) == $best_count) &&
		length($root)<length($best_root)) {
                # OK, found new inflections;
                # this root had better be best.
                $best_count = $count;
                $best_root = $root;
            }
        }
	if (keys(%inflections) == $best_count) {
	    $dict->{$best_root} = $flags{$best_root};
	    foreach my $w (keys(%inflections)) {
		delete($wordhash->{$w});
	    }
	}
    }

    # OK, now there's no uniqe best way to choose,
    # so let's just take the first best.

    while ( my ($word) = %$wordhash ) {
	# Cannot use the "each" operator above since we're
	# truncating %wordhash in the loop
	my @r = ($word, keys(%{$roots{$word}}));
	my $best_root;
	my $max_no_words = 0;
	foreach my $root (@r) {
	    my $no_words = 0;
	    foreach my $wf ($affixfile->expand($root, $flags{$root})) {
		++$no_words if exists($wordhash->{$wf});
	    }
	    if ($no_words > $max_no_words) {
		$best_root = $root;
		$max_no_words = $no_words;
	    }
	}
	$dict->{$best_root} = $flags{$best_root};
	foreach my $w ($affixfile->expand($best_root, $flags{$best_root})) {
	    delete($wordhash->{$w});
	}
    }
    print STDERR "Pos 4: Done ",time, "\n";

    $affixfile->reduce_flags($dict, $optional_words);
    return $dict;
}

#   $prefixes - hashref, keys are strings that
#               may occur as first part of a compound.
#               Each prefix may or may not be a valid word
#               listed in $wordhash.
#               Will be marked with ONLYINCOMPOUND and/or
#               COMPOUNDBEGIN flags where appropriate
#               unless $type is the string "MIDDLE".
#               Will be marked with ONLYINCOMPOUND and/or
#               COMPOUNDMIDDLE flags where appropriate
#               if $type is the string "MIDDLE".
sub insert_prefixes {
    my ($affixfile, $dict, $prefixes, $type) = @_;

    my $flag = ($type ne "MIDDLE") ? $affixfile->{COMPOUNDBEGIN} :
	$affixfile->{COMPOUNDMIDDLE};
	
    foreach my $prefix (keys(%$prefixes)) {
	if (exists($dict->{$prefix})) {
	    $dict->{$prefix} .= $flag;
	    next;
	}
	# Check if one of the already selected root words can
	# generate the prefix:
	my @l = $affixfile->findcsuffix($dict, $prefix, $type);
	if (@l) {
	    # OK, just take the first root
	   my ($root, $f) = @l;
	   $dict->{$root} .= $f;
	   # Compoundmiddle flags don't mix
	   # well with the COMPOUNDBEGIN flag:
	   if ($type eq "MIDDLE" and index($dict->{$root}, $affixfile->{COMPOUNDBEGIN}) >= 0) {
	       $dict->{$root} =~ s/$affixfile->{COMPOUNDBEGIN}/s/;
	       #print STDERR "DEBUG: $root = $dict->{$root}\n";
	   }
	} else {
	    #warn("Prefix only:: <$prefix>\n");
	    $dict->{$prefix} = $flag . $affixfile->{ONLYINCOMPOUND};	    
	}
    }
}

# The given directory shall contain five files. Each file is a
# list of words that (1) may be last in a compound, (2) may not
# be last in a compound, (3) is a compound prefix, (4) is a
# compound middle word, (5) is a compound suffix.
# The words in (3), (4), and (5) are not necessarily allowed as
# stand-alone words.
# Will return a HunDict.
sub build_dict {
    my ($affixfile, $dir) = @_;
    my @files = SpellSet->filenames($dir);
    my $compound_ok = new HunDict(shift(@files), $affixfile->{ONLYINCOMPOUND}, $affixfile->no_merge_flags());
    my $compound_nok = new HunDict(shift(@files), $affixfile->{ONLYINCOMPOUND}, $affixfile->no_merge_flags());
    my $prefix_words = new HunDict(shift(@files), $affixfile->{ONLYINCOMPOUND}, $affixfile->no_merge_flags());
    my $middle_words = new HunDict(shift(@files), $affixfile->{ONLYINCOMPOUND}, $affixfile->no_merge_flags());
    my $suffix_words = new HunDict(shift(@files), $affixfile->{ONLYINCOMPOUND}, $affixfile->no_merge_flags());

    my $prefix_only = new HunDict();
    my $middle_only = new HunDict();

    # Make sure we have disjoint sets of words:

    foreach my $word (keys %$compound_ok) {
#   delete($compound_nok->{$word});
	delete($suffix_words->{$word});
    }

    my ($cok_optional, $cnok_optional) = ({}, {});

    # This is really annoying: words that may be
    # used as a prefix but not a suffix cannot
    # be stored using a flag on a root word that
    # also contains the "COMPOUNDEND" flag, even
    # though the prefix flag is marked as not
    # combining :-(
    foreach my $word (keys %$prefix_words) {
	if (!exists($compound_ok->{$word})) {
	    undef($prefix_only->{$word});
	    delete($prefix_words->{$word});
	}
    }

    foreach my $word (keys %$middle_words) {
    if (!exists($compound_ok->{$word})) {
	undef($middle_only->{$word});
	delete($middle_words->{$word});
    }
}

    foreach my $word (keys %$compound_nok) {
	if (exists($suffix_words->{$word})) {
	    delete($compound_nok->{$word});
	    delete($suffix_words->{$word});
	    undef($compound_ok->{$word})
	}
    }

    my $dict = $affixfile->munch($compound_ok, $cok_optional);

    # All words in $compound_ok are optional when munching $compound_nok below.
    foreach my $word (keys(%$cnok_optional)) {
	undef($compound_ok->{$word});
    }
    $cnok_optional = $compound_ok;
    undef($compound_ok);

    foreach my $word (keys(%$dict)) {
	$dict->{$word} .= $affixfile->{COMPOUNDEND};
	# We cannot use the same root for words that
	# are not allowed to use the "COMPOUNDEND" flag:
	delete($compound_nok->{$word});
    }
    $affixfile->insert_prefixes($dict, $prefix_words, "BEGIN");
    undef($prefix_words);
    $affixfile->insert_prefixes($dict, $middle_words, "MIDDLE");
    undef($middle_words);
    my $dict2 = $affixfile->munch($compound_nok, $cnok_optional);
    $affixfile->insert_prefixes($dict2, $prefix_only, "BEGIN");
    undef($prefix_only);
    $affixfile->insert_prefixes($dict2, $middle_only, "MIDDLE");
    undef($middle_only);
    if (%$dict) {
	$dict->merge($dict2, $affixfile->{ONLYINCOMPOUND}, $affixfile->no_merge_flags());
    } else {
	$dict = $dict2;
    }

    if (%$suffix_words) {
	$dict2 = $affixfile->munch($suffix_words);
	$affixfile->addflag($dict2, "COMPOUNDEND");
	$affixfile->addflag($dict2, "COMPOUNDPERMITFLAG");
	$affixfile->addflag($dict2, "ONLYINCOMPOUND");
	$dict->merge($dict2, $affixfile->{ONLYINCOMPOUND}, $affixfile->no_merge_flags());
    }

    return $dict;
}

1;
