#!/usr/bin/perl
use strict;
use warnings;
use diagnostics;
use locale;
use HunDict;
use HunAffix;

binmode(STDOUT, ":encoding(UTF-8)");
binmode(STDERR, ":encoding(UTF-8)");

die("requires 6 parameters") unless @ARGV == 6;
my ($aff_filename, $build_dir, $nosug_dir,
    $forbidden_dir, $extra_filename, $res_filename) = @ARGV;

my $normal_dir = $build_dir . "/normal";
my $affixfile = new HunAffix($aff_filename);

# Compilation is slow (if the affixfile is complex),
# avoid it for small dictionaries:
$affixfile->compile();

my $main_dict = $affixfile->build_dict($normal_dir);

{
    my $nosug_dict = $affixfile->build_dict($nosug_dir);
    $affixfile->addflag($nosug_dict, "NOSUGGEST");
    $main_dict->merge($nosug_dict, $affixfile->{ONLYINCOMPOUND},
		      $affixfile->no_merge_flags());
}

{
#    my $forbiddenwords = new HunDict($forbidden_filename,
#				     $affixfile->{ONLYINCOMPOUND},
#				     $affixfile->no_merge_flags());
#    my $fdict = $affixfile->munch($forbiddenwords);
    my $fdict = $affixfile->build_dict($forbidden_dir);
    $affixfile->addflag($fdict, "FORBIDDENWORD");
    $main_dict->merge($fdict, $affixfile->{ONLYINCOMPOUND},
		      $affixfile->no_merge_flags());
}

{
    my $vdict = $affixfile->build_dict("$build_dir/verb");
    $affixfile->addflag($vdict, "j");
    $main_dict->merge($vdict, $affixfile->{ONLYINCOMPOUND},
		      $affixfile->no_merge_flags());
}

{
    my $adict = $affixfile->build_dict("$build_dir/adjektiv");
    $affixfile->addflag($adict, "k");
    $main_dict->merge($adict, $affixfile->{ONLYINCOMPOUND},
		      $affixfile->no_merge_flags());
}

{
    my $extra_dict = new HunDict($extra_filename,
				 $affixfile->{ONLYINCOMPOUND},
				 $affixfile->no_merge_flags());

    $main_dict->merge($extra_dict, $affixfile->{ONLYINCOMPOUND},
		      $affixfile->no_merge_flags());
}

$main_dict->print($res_filename);
