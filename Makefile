all: wordlists
	$(MAKE) DICTDIR=$(shell pwd) -C dssobuild

sv_FI:
	$(MAKE) DICTDIR=$(shell pwd) -C dssobuild $@

oodict:
	$(MAKE) -C oodict

ffdict:
	$(MAKE) -C ffdict

wordlists: index/stringdb.bin
	$(MAKE) -C src gen_dicts
	./src/gen_dicts

index/stringdb.bin:
	$(MAKE) -C src rebuild_dssodb
	mkdir -p index
	./src/rebuild_dssodb

.PHONY: all wordlists oodict ffdict
