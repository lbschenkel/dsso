# dsso

 Swedish spell-check dictionary.

 To build the Swedish Hunspell dictionary, run GNU make in
 the project's top-level directory. The dictionary will be
 saved as sv_SE.aff and sv_SE.dic in the "build" directory.

 The latest official build can be found in the resources directory.

# Mozilla and LibreOffice extensions

 To build extensions for Mozilla Firefox and LibreOffice,
 first build the dictionary for locales sv_SE and sv_FI:

    make
    make sv_FI

 and then run

    make oodict
    make ffdict

# Dependencies

* Hunspell version 1.7

* GNU Make

* Unix command line tools

* Perl

* Gtk+ and gtkmm version 3

# Modifying the dictionary

 To modify the Swedish Hunspell dictionary, you should edit the source
 file dsso_db.txt and/ord the simple word lists in the dssobuild/words
 directory. The recommended method to edit dsso_db.txt is to build
 and use the gcorpus app in the src directory.
